<?php
class H {
	
    public static function getServerType(){
        $aCnf = Registry::get('aConfig');
        $strServerType = array_search($_SERVER['HTTP_HOST'], $aCnf['aDomains']);
        if (strpos($strServerType, 'localhost')) $strServerType = 'localhost';
        return $strServerType;
    }	

    public static function ConnectToDB() {
        $strConfName = self::getServerType();
        require_once DR.'conf/db/'.$strConfName.'.inc';

        $oDB = new db($aConf['db']);
        $oDB->connect();

        Registry::set('db', $oDB);
    }
	
	
    public static function incModels() {
        $strModelsPath = DR.'lib/models';

        if (is_dir($strModelsPath)) {
            $strFiles = scandir($strModelsPath);
            $aModels = array();

            foreach ($strFiles as $strFile) {
                if (preg_match('/\.php/', $strFile)) {
                        $aModels[] = $strFile;
                }
            }		

            foreach ($aModels as $aModel)	 {
                include_once($strModelsPath.'/'.$aModel);
            }
        }
    }
	
    public static function hcurl($strHost,$strData=false,$bHeaders=false)
    {
        $oCurl = curl_init();
        if($oCurl) {
            if($strData) {
                curl_setopt($oCurl,CURLOPT_SSL_VERIFYPEER,false);
                curl_setopt($oCurl,CURLOPT_SSL_VERIFYHOST,false);
                curl_setopt($oCurl,CURLOPT_URL,$strHost);
                curl_setopt($oCurl,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($oCurl,CURLOPT_POST,false);
                curl_setopt($oCurl,CURLOPT_POSTFIELDS,$strData);
                curl_setopt($oCurl,CURLOPT_HEADER,$bHeaders);
                $oResult = curl_exec($oCurl);
                curl_close($oCurl);
				
                return $oResult;
            } else {
                curl_setopt($oCurl, CURLOPT_URL, $strHost);
                curl_setopt($oCurl, CURLOPT_RETURNTRANSFER,true);
                $oResult = curl_exec($oCurl);
                curl_close($oCurl);
				
                return $oResult;
            }
            return false;
        }
    }
	
    public static function printBool($bVar){ // вывод булевой переменной в виде строки
        print ($bVar) ? 'true' : 'false';
    }

    public static function printDebug($mixVar){ //вывод переменных для дебага
        print '<PRE>';
        print (is_array($mixVar) || is_object($mixVar)) ? print_r($mixVar,true):$mixVar;
        print '</PRE>';
    }
	
    public static function createDateRange($nStart,$nEnd,$strFormat="Y-m-d"){ // создание массива с диапазоном дат
        $aDates = array();
        for($i=$nStart; $i<$nEnd; $i+=86400){
            $aDates[] = date($strFormat,$i);
        }
        return $aDates;
    }

    public static function searchArrray($aData,$mixNeedle,$strField){
        $i=0;
        do 
            if($aData[$i][$strField] == $mixNeedle) return $aData[$i];
        while(++$i < count($aData));
        return FALSE;
    }
	
    public static function reArrayFiles(&$file_post) { //пересобрание массива с множеством файлов
        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

		for ($i=0; $i<$file_count; $i++) {
			foreach ($file_keys as $key) {
				$file_ary[$i][$key] = $file_post[$key][$i];
			}
		}
		return $file_ary;
    }
	
    public static function translit($s,$bRmSpace=TRUE) {
        $s = (string) $s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        if ($bRmSpace) $s = str_replace(" ", "_", $s); // заменяем пробелы 
        return $s; // возвращаем результат
    }
	
	public static function getAbsolutePath($strDir){
		return $_SERVER['DOCUMENT_ROOT'].$strDir;
	}
	
	/************* Старье. Проверить необходимость ********************/
	
	public static function calulate_date_from_sec($sec) {
		$siy = 60*60*24*30*12; 		// Секунд в году
		$sim = 60*60*24*30; 		// Секунд в месяце
		$sid = 60*60*24;			// Секунд в дне
		$sih = 60*60;				// Секунд в часу
		$sii = 60;					// Секунд в минуте
		
		$cy = floor($sec/$siy); // Прошло лет
		$cm = floor(($sec-$siy*$cy)/$sim);
		$cd = floor(($sec-$cy*$siy-$cm*$sim)/$sid);
		$ch = floor(($sec-$cy*$siy-$cm*$sim-$cd*$sid)/$sih);
		$ci = floor(($sec-$cy*$siy-$cm*$sim-$cd*$sid-$ch*$sih)/$sii);
		$cs = floor($sec-$cy*$siy-$cm*$sim-$cd*$sid-$ch*$sih-$ci*$sii);
		
		$result = array();
		
		if ($cy>0) $result['y'] = array(
			'count' => $cy,
			'sign' => self::get_sign($cy, 'y')
		);
		if ($cm>0) $result['m'] = array(
			'count' => $cm,
			'sign' => self::get_sign($cm, 'm')
		);
		if ($cd>0) $result['d'] = array(
			'count' => $cd,
			'sign' => self::get_sign($cd, 'd')
		);
		if ($ch>0) $result['h'] = array(
			'count' => $ch,
			'sign' => self::get_sign($ch, 'h')
		);
		if ($ci>0) $result['i'] = array(
			'count' => $ci,
			'sign' => self::get_sign($ci, 'i')
		);
		if ($cs>0) $result['s'] = array(
			'count' => $cs,
			'sign' => self::get_sign($cs, 's')
		);
		
		return $result;
	}
	
	
	/** [T] Подписи к разным еденицам
	 * 
	 * Коды едениц
	 * y - Года
	 * m - Месяцы
	 * d - Дни
	 * h - Часы
	 * i - Минуты
	 * s - Секунды
	 *
	 * @param int $n Число
	 * @param str $code Код еденицы
	 * @return str название еденицы в нужной форме */
	static public function get_sign($n, $code) {
		$n = (int)$n;
		if ((11>$n)||($n>19)) {
			$n = (string)$n;
			switch (substr($n,-1,1)) {
				case 1: $result = array(
					'y' => 'Год', 
					'm' => 'Месяц', 
					'd' => 'День',
					'h' => 'Час',
					'i' => 'Минута',
					's' => 'Секунда',
				); break;
				case 2: case 3:  case 4: $result = array(
					'y' => 'Года', 
					'm' => 'Месяца', 
					'd' => 'Дня',
					'h' => 'Часа',
					'i' => 'Минута',
					's' => 'Секунды',
				); break;
				default: $result = array(
					'y' => 'Лет', 
					'm' => 'Месяцев', 
					'd' => 'Дней',
					'h' => 'Часов',
					'i' => 'Минут',
					's' => 'Секунд',
				);
			}
		} else {
			$result = array(
				'y' => 'Лет', 
				'm' => 'Месяцев', 
				'd' => 'Дней',
				'h' => 'Часов',
				'i' => 'Минут',
				's' => 'Секунд',
			);
		}
		
		return $result[$code];
	}
	
	public static function isCode($strCode) {
		return preg_match("/^[a-zA-Z0-9_-]+$/", $strCode) > 0;
	}
	
	
	public static function tpl($template, $val_fields = array()) {
		preg_match_all("/{([^}\s]*)}/i",$template,$res);
		
		foreach ($res[0] as $n=>$v)	{
			$template = str_replace($v, $val_fields[$res[1][$n]], $template); 	
		}
		
		return $template; 
	}
	
	/** w3captcha - php-скрипт для генерации изображений CAPTCHA
	 * версия: 1.1 от 08.02.2008
	 * разработчики: http://w3box.ru
	 * тип лицензии: freeware
	 * w3box.ru © 2008 */
	public static function capthca() {
		
		session_start();
		
		$count=5;	/* количество символов */
		$width=100; /* ширина картинки */
		$height=48; /* высота картинки */
		$font_size_min=32; /* минимальная высота символа */
		$font_size_max=32; /* максимальная высота символа */
		$font_file=DR."/public/fonts/Comic_Sans_MS.ttf"; /* путь к файлу относительно w3captcha.php */
		$char_angle_min=-10; /* максимальный наклон символа влево */
		$char_angle_max=10;	/* максимальный наклон символа вправо */
		$char_angle_shadow=5;	/* размер тени */
		$char_align=40;	/* выравнивание символа по-вертикали */
		$start=5;	/* позиция первого символа по-горизонтали */
		$interval=16;	/* интервал между началами символов */
		$chars="0123456789"; /* набор символов */
//		$chars = join('', array_merge(range('a','z'), range('A','Z'), range('1','9')));	

		$noise=10; /* уровень шума */
		
		$image=imagecreatetruecolor($width, $height);
		
		$background_color=imagecolorallocate($image, 255, 255, 255); /* rbg-цвет фона */
		$font_color=imagecolorallocate($image, 32, 64, 96); /* rbg-цвет тени */
		
		imagefill($image, 0, 0, $background_color);
		
		$str="";
		
		$num_chars=strlen($chars);
		for ($i=0; $i<$count; $i++)
		{
			$char=$chars[rand(0, $num_chars-1)];
			$font_size=rand($font_size_min, $font_size_max);
			$char_angle=rand($char_angle_min, $char_angle_max);
			imagettftext($image, $font_size, $char_angle, $start, $char_align, $font_color, $font_file, $char);
			imagettftext($image, $font_size, $char_angle+$char_angle_shadow*(rand(0, 1)*2-1), $start, $char_align, $background_color, $font_file, $char);
			$start+=$interval;
			$str.=$char;
		}
		
		if ($noise)
		{
			for ($i=0; $i<$width; $i++)
			{
				for ($j=0; $j<$height; $j++)
				{
					$rgb=imagecolorat($image, $i, $j);
					$r=($rgb>>16) & 0xFF;
					$g=($rgb>>8) & 0xFF;
					$b=$rgb & 0xFF;
					$k=rand(-$noise, $noise);
					$rn=$r+255*$k/100;
					$gn=$g+255*$k/100;		
					$bn=$b+255*$k/100;
					if ($rn<0) $rn=0;
					if ($gn<0) $gn=0;
					if ($bn<0) $bn=0;
					if ($rn>255) $rn=255;
					if ($gn>255) $gn=255;
					if ($bn>255) $bn=255;
					$color=imagecolorallocate($image, $rn, $gn, $bn);
					imagesetpixel($image, $i, $j , $color);					
				}
			}
		}
		
		$_SESSION["captcha"]=$str;
		
		if (function_exists("imagepng"))
		{
			header("Content-type: image/png");
			imagepng($image);
		}
		elseif (function_exists("imagegif"))
		{
			header("Content-type: image/gif");
			imagegif($image);
		}
		elseif (function_exists("imagejpeg"))
		{
			header("Content-type: image/jpeg");
			imagejpeg($image);
		}
		
		imagedestroy($image);

	}
	

	public static function mail($strFrom,$strToEmail,$strToName,$strSubject,$strBody,$isHtml=true)
	{
		$strFromName = options::get(1);
		$isHtml = (bool)$isHtml;
		$oMail = new PHPMailer;
		$oMail->IsMail();
		$oMail->AddReplyTo('info@potolki.ru');
		$oMail->From = $strFrom;
		$oMail->FromName = $strFromName;
		$oMail->AddAddress($strToEmail,$strToName);
//		$oMail->AddBCC('info@verabystrova.com');
		$oMail->CharSet = 'utf-8';
		$oMail->Subject = $strSubject;
		$oMail->Body = $strBody;
		if($isHtml)
		{
			$oMail->MsgHTML($strBody);
		}
		$oMail->IsHTML($isHtml);
		if(!$oMail->Send())
		{
			exit('Извините, невозможно отправить письмо для '.$strToEmail.'. Пожалуйста попробуйте позже... error = '.$oMail->ErrorInfo);
		}
	}
}