<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of DB
 *
 * @author Faust
 */

/**
*           СВОЙСТВА
*      +-------------------------------------------------------------------------------------+
*      |  private $debugOn;   Логический тип.                                                |
*      |  Если true то в глобальные переменные записывается время выполнения запроса         |
*      +-------------------------------------------------------------------------------------+
*      |  private $_nTime;
*      |  Дополнительная переменная используется для вычисления времени выполнения запроса
*      +-------------------------------------------------------------------------------------+  
* 
* 
*         МЕТОДЫ 
*      +-------------------------------------------------------------------------------------+
*      |  private function mysql_make_qw()
*      |  Функция помогает в создании запроса и экранировании.
*      +-------------------------------------------------------------------------------------+
*      |  public function _getOne($strSql)                                                   |
*      |  Возвращает одну переменную из таблицы(отношения)).                                 |
*      +-------------------------------------------------------------------------------------+
*      |  public function _getRow($strSql)       
*      |  Возвращает одну запись(кортеж) из таблицы).
*      +-------------------------------------------------------------------------------------+
*      |  public function _getAll($strSql,$bUseCache=false)
*      |  Возвращает все записи удовлетворяющие запросу. Если $bUseCache = true, то будет
*      |  произведено кэширование результата запроса
*      +-------------------------------------------------------------------------------------+  
*      |  public function _executeSql($strSql)
*      |  Выполняет запрос, ничего не возвращает
*      +-------------------------------------------------------------------------------------+ 
*      |  public function _insertId($strTable)
*      |  В качестве входных данных выступает имя таблицы, результат максимальный id плюс 1.
*      +-------------------------------------------------------------------------------------+
*      |  public function _getUpdateSql($strTable,$aData,$nId=0)
*      |  Функция генерирует запрос к базе данных, входные данные: 
*      |  - Имя отношения(таблицы)), 
*      |  - Ассоциативный массив, где ключ должен совпадать с атрибутом отношения, 
*      |  - ID кортежа, если такой id существует, то кортеж будет обновлен, иначе добавлен кортеж 
*      +-------------------------------------------------------------------------------------+
* 	   |  private function __execute($strSql)
*      |  Функция возращает результат sql запрса
*      +-------------------------------------------------------------------------------------+
*      |  private function  addDebugTime()
*      |  Записывает в глобальный массив время выполнения sql запроса
*      +-------------------------------------------------------------------------------------+
*      |  private function _getSqlCache($strSql) 
*      |  Возвращает результат запроса из Кэша
*      +-------------------------------------------------------------------------------------+
*      |  private function _setSqlCache($strSql,$mContent)
*      |  Записывает в кэш результат SQL запроса
*      +-------------------------------------------------------------------------------------+
**/   

     
class DBApi {
    private $debugOn;
    private $_nTime;
    private $strSql;
    public $oDb;

    public function __construct($strDbName='',$debugOn=false) {
            $strConfName = H::getServerType();
            require DR.'conf/db/'.$strConfName.'.inc';
            $conf = $aConf['db'];
        $this->debugOn=$debugOn;
            $strDbName = ($strDbName) ? $strDbName : $conf['name'];;

        $this->oDb = new mysqli($conf['host'], $conf['user'], $conf['pass']);
        if(!$this->oDb){
            print sprintf('%s | %s | %s',$conf['host'], $conf['user'], $conf['pass']);
            exit('Cannot connect to mySQL... Check the settings');
        }
        else{
            if(!mysqli_select_db($this->oDb,$strDbName)){
                exit('Cannot select Data');
            }
        }
        mysqli_set_charset($this->oDb,'utf8');    
    } 

    public function getOne($table, $field, $where) {
        $sql = sprintf("SELECT %s FROM %s WHERE %s  ", $field, $this->prefix.$table, $where);
        $res = mysqli_query($sql);
        $rez = mysql_fetch_array($res);

        return $rez[0];

        return false;
    }
		
    public function getById($strTable,$nId,$aFields=array()){ //косая, оставил временно, для совместимости со старыми проектами
        $strFields = ($aFields) ? implode(',', $aFields) : '*';
        $strSql = sprintf('SELECT %s FROM %s WHERE id=%d',$strFields,$strTable,$nId);
        return $this->_getOne($strSql);
    }

    public function getOneById($strTable,$nId,$strField){
        $strSql = sprintf('SELECT %s FROM %s WHERE id=%d LIMIT 1',$strField,$strTable,$nId);
        return $this->_getOne($strSql);
    }
    public function getRowById($strTable,$nId,$aFields=array()){
        $strFields = ($aFields) ? implode(',', $aFields) : '*';
        $strSql = sprintf('SELECT %s FROM %s WHERE id=%d',$strFields,$strTable,$nId);
        return $this->_getRow($strSql);
    }

    public function getAll($strTable, $aFields=array(), $mixWhere=array(), $mixOrder=array(), $nLimit = 9999) {
        $strFields = ($aFields) ? implode(',', $aFields) : '*';
		$strWhere = '';
        if(is_array($mixWhere) && count($mixWhere)){
            $strWhere = $this->prepareArray($mixWhere);				
        }else if (is_string($mixWhere)){
            $strWhere = $mixWhere;
        }
        //if (!$strWhere && !$bAll) return FALSE;
        $strWhere = ($strWhere) ? $strWhere : '1';
		$strOrder = '';
        if (is_array($mixOrder) && count($mixOrder)){
            //дописать
        }elseif(is_string ($mixOrder)){
            $strOrder = $mixOrder;
        }
        $strOrder = ($strOrder) ? $strOrder : '1';
        $strSql = sprintf('SELECT %s FROM %s WHERE %s ORDER BY %s LIMIT %d',$strFields,$strTable,$strWhere,$strOrder,$nLimit);
        
        return $this->_getAll($strSql);
    }

    public function mysql_make_qw()
    {
        $args = func_get_args();
        $tmpl =& $args[0];
        $tmpl = str_replace("%", "%%", $tmpl);
        $tmpl = str_replace("?", "%s", $tmpl);
        foreach ($args as $i=>$v) {
            if (!$i) continue;
            if (is_int($v)) continue;
            $args[$i] = "'".mysql_escape_string($v)."'";
        return call_user_func_array("sprintf", $args);
        } 
    }

    public function _getOne($strSql){
        $oResult = $this->__execute($strSql);
            $uResult = false;
        if(mysqli_num_rows($oResult)>0){
            $aTmp = mysqli_fetch_row($oResult);
            $uResult = $aTmp[0];
        }
        mysqli_free_result($oResult);
        if ($this->debugOn) {
            $this->addDebugTime();
        }
        return $uResult;
    }
       
    public function _getRow($strSql){
        $oResult = $this->__execute($strSql);
        $aResult = false;
        if(mysqli_num_rows($oResult)>0){
            $aResult = mysqli_fetch_assoc($oResult);
        }

        mysqli_free_result($oResult);
        if ($this->debugOn) {
            $this->addDebugTime();
        }
        return $aResult;
    }

    public function getRow($table, $where) {
        $sql = sprintf("SELECT * FROM %s WHERE %s", $table, $where);
        $res = mysqli_query($this->oDb, $sql);

        return mysqli_fetch_assoc($res);

        return false;
    }
		
    /** Простой запрос к базе (возвращае все значения)
     *
     * @param string $sql
     * @return array - массив полей записи */
    public function _getAll($sql,$bAssoc=TRUE) {
        $oRes = mysqli_query($this->oDb, $sql);
        $aResult = array();
        if (!$oRes)	return $aResult;		
        if ($bAssoc){
            while($aTmp = mysqli_fetch_assoc($oRes))	$aResult[] = $aTmp;
        }else{
            while($aTmp = mysqli_fetch_row($oRes))	$aResult[] = $aTmp[0];
        }
        mysqli_free_result($oRes);
        return $aResult;
    }
        
    public function _executeSql($strSql){
        return $this->__execute($strSql);
    }

    public function _insertId($strTable){
        $strSql = sprintf('select MAX(id)+1 from `%s`',$strTable);
        $nInsertId = intval($this->_getOne($strSql));
        return ($nInsertId > 0) ? $nInsertId : 1;
    }
		
    public function getInsertId() {
        return mysqli_insert_id($this->oDb);
    }
    
    public function _getUpdateSql($strTable,$aData,$nId=0){
        if ($this->debugOn) {
            $this->_nTime = microtime(true);
        }
        $strSql = (($nId) ? ' update ':' insert into ').$strTable.' set ';
        foreach($aData as $strKey => $uValue){
            $strSql .= $strKey.' = "'.mysqli_real_escape_string($this->oDb,stripslashes($uValue)).'", ';
        }
        $nInsertId = $this->_insertId($strTable);

        $strSql = substr($strSql,0,-2);
        $strSql .= ($nId) ? ' where id = "'.$nId.'" ' : (!isset($aData['id']) ? ', id = "'.$nInsertId.'"' : '');

        $nId = ($nId) ? $nId : $nInsertId;

        if ($this->debugOn) {
            $this->addDebugTime();
        }
        return array($strSql,$nId);
    }
		
		
    public function getUpdateSql($strTable,$aData,$nId=0){
        if ($this->debugOn) {
            $this->_nTime = microtime(true);
        }
        $strSql = (($nId) ? ' update ':' insert into ').$strTable.' set ';
        foreach($aData as $strKey => $uValue){
            $strSql .= $strKey.' = "'.mysqli_real_escape_string($this->oDb,stripslashes($uValue)).'", ';
        }
        $nInsertId = $this->_insertId($strTable);

        $strSql = substr($strSql,0,-2);
        //$strSql .= ($nId) ? ' where id = "'.$nId.'" ' : (!isset($aData['id']) ? ', id = "'.$nInsertId.'"' : '');
        $strSql .= ($nId) ? sprintf(' where id = %d',$nId) : '';

        $nId = ($nId) ? $nId : $nInsertId;

        if ($this->debugOn) {
            $this->addDebugTime();
        }
        return $strSql;
    }

    public function insertTo($strTable,$aData){
		$strSql = $this->getUpdateSql($strTable, $aData);
		return ($this->_executeSql($strSql)) ? $this->getInsertId() : 0;
    }

    public function updateById($strTable,$aData,$nId){
        $strSql = $this->getUpdateSql($strTable, $aData,$nId);
        return ($this->_executeSql($strSql)) ? $this->getInsertId() : 0;
    }
    
    public function deleteById($strTable,$nId){
        $strSql = sprintf('DELETE FROM %s WHERE id=%d',$strTable,$nId);
        return $this->_executeSql($strSql);
    }
		
    private function __execute($strSql){
        if ($this->debugOn) {
            $this->_nTime = microtime(true);
        }
        $oResult = mysqli_query($this->oDb, $strSql);
        if(!$oResult){
            exit("Sql-query: " . $strSql . "\n\nError string: " . mysqli_error($this->oDb) . " on line " . __LINE__ . ", " . __FILE__ . ", " . __METHOD__);
        }
        return $oResult;
    }
    
    private function addDebugTime()
    {
        $mTime = microtime(true)-$this->_nTime;
            $GLOBALS['aDebugSqlTimes'][] = array($mTime,$strSql,__METHOD__);
        $GLOBALS['nDebugSqlTime'] += $mTime;
    }
        
    private function _getSqlCache($strSql)
    {
        $strSqlMd5 = md5($strSql);
        $strFilePath = SQL_CACHEDIR.$strSqlMd5;
        $nCacheTimeout = time()-SQL_CACHETIMEOUT;

        if(is_dir(SQL_CACHEDIR) && is_writable($strCacheDir)) {
            if(file_exists($strFilePath)) {
                $nFileTime = filemtime($strFilePath);
                if($nFileTime < $nCacheTimeout) {
                    @unlink($strFilePath);
                }
                else {
                    $oFp = @fopen($strFilePath,'rb');
                    flock($oFp, LOCK_SH);
                    $strCache = fread($oFp, filesize($strFilePath));
                    flock($oFp, LOCK_UN);
                    fclose($oFp);
                    return unserialize($strCache);
                }
            }
        }
        return false;
    }
        
    private function _setSqlCache($strSql,$mContent)
    {
        if($mContent != "" || sizeof($mContent) > 0){
            $strSqlMd5 = md5($strSql);
            $strFilePath = SQL_CACHEDIR.$strSqlMd5;
            if(is_dir(SQL_CACHEDIR) && is_writable(SQL_CACHEDIR)){
                if(file_exists($strFilePath)){
                    @unlink($strFilePath);
                }
                $oFp = @fopen($strFilePath,'wb');
                flock($oFp, LOCK_SH);
                fwrite($oFp,serialize($mContent));
                flock($oFp, LOCK_UN);
                fclose($oFp);
                @chmod($file_path, 0666);
                return true;
            }
        }
        return false;
    }

    private function prepareArray($aData=array()){
		if (!is_array($aData) || !count($aData)) return FALSE;
		$strData = '';
		$aTmp = array();
		foreach($aData as $strKey => $uValue){
			$aTmp[] = sprintf('`%s`="%s"',$strKey, mysqli_real_escape_string($this->oDb,stripslashes($uValue)));
		}
		$strData = implode(' AND ', $aTmp);
		return $strData;
	}
}
?>
