<?php

define('IS_DEBUG',TRUE);
error_reporting(E_ALL);
ini_set('display_errors', 1);



define('DR', $_SERVER['DOCUMENT_ROOT'].'/');

include DR.'/conf/main.php';;
include DR.'/lib/registry.php';
include DR.'/lib/helper.php';

include DR.'/lib/DB.Class.php';

include DR.'/lib/mail/class.phpmailer.php';
include DR.'/lib/Images.php';

include DR.'/controllers/Default.php';
include DR.'/controllers/Index.php';
include DR.'/controllers/Admin.php';
include DR.'/controllers/Ajax.php';

include DR.'/lib/smarty/Smarty.class.php';

Registry::set('aConfig', $aConfig);

include DR.'/lib/db.php';
include DR.'/lib/user.php';
H::ConnectToDB();

H::incModels();

$aUrl = parse_url($_SERVER['REQUEST_URI']);
$aAddress = explode('/', $aUrl['path']);
Registry::set('address', $aAddress);

$strPage = $aAddress[1];
if (!isset($aAddress[2])) $aAddress[2] = '';

// Определяем контроллер
if ($strPage == 'ajax') { 
	$oController = new Ajax_Controller(array(
		'smarty_template_dir' => DR.'/tpl/',
		'smarty_compile_dir' => DR.'/tpl/smarty_sys/ctpl/',
		'smarty_cache_dir' => DR.'/tpl/smarty_sys/cache/',
		'path' => '/ajax'
	));		
	$strPage = $aAddress[2];
} 
elseif ($strPage == 'admin') { 
	$oController = new Admin_Controller(array(
		'smarty_template_dir' => DR.'/tpl/admin/',
		'smarty_compile_dir' => DR.'/tpl/smarty_sys/ctpl/',
		'smarty_cache_dir' => DR.'/tpl/smarty_sys/cache/',
		'path' => '/admin',
		'page' => $aAddress[2]
	));		
	$strPage = $aAddress[2];
} 
else { 
	$oController = new Index_Controller(array(
		'smarty_template_dir' => DR.'/tpl/index/',
		'smarty_compile_dir' => DR.'/tpl/smarty_sys/ctpl/',
		'smarty_cache_dir' => DR.'/tpl/smarty_sys/cache/',
		'path' => '/',
		'page' => $strPage
	));	
}

$strAction = (isset($strPage) && $strPage!='') ? $strPage : 'index';

// Запускаем экшн	
$oController->$strAction();
unset($oController);

?>