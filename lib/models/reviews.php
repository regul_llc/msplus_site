<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of projects
 *
 * @author Faust
 */
class reviews extends mainApi {
	
	const IMG_DIR_TPL = '/data/images/reviews/%d/trimmed/%d_%s.jpg';
    const IMG_ORIGDIR_TPL = '/data/images/reviews/%d/originals/%s';
	private static $aSizesNames = array('mini','middle');
	
	private static $MapReviews = array(
			'author'=>'author',
			'text'=>'text',
			'age'=>'age',
			'title'=>'title',
			'type'=>'cat',
			'status'=>'status'
	);

	
	public static function add($aData, $bFromAdmin=false){
		$aData = self::getDataByMap(self::$MapReviews, $aData);
		$aData['date_add'] = time();
		$aData['status'] = 0;
		return self::dbApi()->insertTo(self::Treviews, $aData);
	}
	
	public static function update($aData, $nId){
		$aData = self::getDataByMap(self::$MapReviews, $aData);
		return self::dbApi()->updateById(self::Treviews, $aData, $nId);
	}
	
	public static function delete($nId){
		$aImages = images::getImagesIds($nId, 'review');
        foreach ($aImages as $value) {
            self::deleteImage($value);
        }
        self::dbApi()->deleteById(self::Treviews,$nId);
		return TRUE;
	}
	

	public static function getAll($nCatId=0,$bPublic = TRUE,$nLimit = 0){
		$aFilter = array();
		if ($nCatId) $aFilter['type'] = $nCatId;
		if ($bPublic) $aFilter['status'] = 1;
		$nLimit = ($nLimit) ? intval($nLimit) : 9999;
		$strOrder = "priority DESC, date_add DESC";
        return self::dbApi()->getAll(self::Treviews,array(),$aFilter,$strOrder,$nLimit);
	}
	
	public static function getOne($nId){
        return self::dbApi()->getRowById(self::Treviews,$nId);
	}
	
	public static function setImgToBD($nReviewId,$strImgName){
        return self::dbApi()->insertTo(
            self::Timages,array('item_id'=>$nReviewId,'origname'=>$strImgName, 'item_type'=>'review', 'title'=>'', 'date_add'=>time()));
        }
	
	public static function getImagesForAdmin($nId){
        $aRows = self::dbApi()->_getAll('SELECT * FROM images WHERE item_type="review" AND item_id='.$nId);
		$aResult = imagesHelper::getImagesArray($aRows);
        return $aResult;
        }
	
	public static function deleteImage($nImgId){
        $aImg = self::dbApi()->getRowById(self::Timages,$nImgId);
        @unlink(sprintf(H::getAbsolutePath(self::IMG_ORIGDIR_TPL), $aImg['item_id'], $aImg['origname']));
        foreach (self::$aSizesNames as $value) {
            unlink(sprintf(H::getAbsolutePath(self::IMG_DIR_TPL), $aImg['item_id'], $nImgId,$value));
        }	
        self::dbApi()->deleteById(self::Timages,$nImgId);
        //self::checkMainPhoto($aImg['item_id']);
        return TRUE;
    }	
}
