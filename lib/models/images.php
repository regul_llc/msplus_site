<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of images
 *
 * @author Faust
 */
class images extends mainApi{
	
	public static function UploadImage($aFile,$nId,$strItemType){
		return imagesHelper::UploadImage($aFile, $nId, $strItemType);
	}
	
	public static function getImagesForAdmin($strType,$nId){
		$aResult = array();
		switch ($strType){
			case 'project':
				$aResult = projects::getProjectImagesForAdmin($nId);
				break;
			case 'type':
				$aResult = projects::getTypeImagesForAdmin($nId);
				break;
			case 'review':
				$aResult = reviews::getImagesForAdmin($nId);
				break;
			default :
				$aResult = array();
				break;
		}
		return $aResult;
	}
	
	public static function getImagesByType($strType,$nId){
		return self::dbApi()->getAll(self::Timages, array(), array('item_type'=>$strType,'item_id'=>$nId));
	}

	public static function setMainPhoto($nImgId){
		$aImgInfo = self::dbApi()->getRowById(self::Timages,$nImgId);
		$strItemType = $aImgInfo['item_type'];
		$nItemId = $aImgInfo['item_id'];
		$strTable = self::getItemTableByType($strItemType);
        $strSql = sprintf('UPDATE %s SET is_main=0 WHERE item_type="%s" AND item_id=%d', self::Timages,$strItemType,$nItemId);
        self::dbApi()->_executeSql($strSql);
        self::dbApi()->updateById(self::Timages, array('is_main'=>1), $nImgId);		
		self::dbApi()->updateById($strTable, array('cover'=>$nImgId), $nItemId);
        return TRUE;
    }
	
	private static function getItemTableByType($strType){
		switch ($strType){
			case 'project'	: return self::Tprojects;
			case 'type'		: return self::Tprcats;
			case 'review'	: return self::Treviews;
			default			: return FALSE;
		}
	}
	
	public static function getImagesIds($nId,$strType){
		$strSql = sprintf('SELECT id FROM images WHERE item_type="%s" AND item_id=%d',$strType,$nId);
        return self::dbApi()->_getAll($strSql, FALSE);
    }
	
}
