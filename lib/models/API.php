<?php
/**
* @property oDb $DB
* @property $DB DB
* @property AnotherObject $anotherObject
* @property class DB
*/
class mainApi
{
	const Tprojects = 'works';
	const Tprcats   = 'works_categories';
	const Timages   = 'images';
        const Tarticles = 'articles';
	const Trequests = 'requests';
	const Torders   = 'orders';
	const Treviews  = 'reviews';   


	const IMG_DIR_TPL = '/data/images/projects/%d/trimmed/%d_%s.jpg';
	const IMG_ORIGDIR_TPL = '/data/images/projects/%d/originals/%s';
	
	protected static $oDb = null;
	
	
	public function __construct($DB) {
		/* @var $DB DB  */ 
		self::$oDb = ($DB) ? $DB : Registry::get('db');
	}
	
	public static function dbApi(){
		return self::$oDb;
	}
	
	protected static function getDataByMap($aMap,$aData){
		$aResult = array();
		foreach ($aMap as $key=>$value){
			if (isset($aData[$value])) $aResult[$key] = $aData[$value];
		}
		return $aResult;
	}
	
	public static function getUpdateData($strData){
		$aTmp = explode(';', $strData);
		$aResult = array();
		foreach ($aTmp as $key => $value) {
			$aTmpVal = explode('=', $value);
			$aResult[$aTmpVal[0]] = $aTmpVal[1];
		}
		return $aResult;
	}
	
}

?>
