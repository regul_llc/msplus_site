<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class articles extends mainApi {
	
    const IMG_DIR_TPL = '/data/images/articles/%d/trimmed/%d_%s.jpg';
    const IMG_ORIGDIR_TPL = '/data/images/articles/%d/originals/%s';
	private static $aSizesNames = array('mini','middle');
        
	private static $MapArticlesWeb = array(
                        'title'	=>'title',
			'text'  =>'text',
                        'status'=>'status'
			
	);
        
        public static function getArticles(){
            return self::dbApi()->_getAll('SELECT * FROM articles WHERE status=1');
        }
    
        public static function getArticle($id) {
            return self::dbApi()->_getRow('SELECT * FROM articles WHERE id='.$id);
        }
        
        public static function getArticleByUrl($url) {
            return self::dbApi()->_getRow('SELECT * FROM articles WHERE url LIKE "'.$url.'"');
        }
        public static function add($aData){
		
		//$aData['date_add'] = time();
		$aData['status'] = 0;
		return self::dbApi()->insertTo(self::Tarticles, $aData);
	}
        
        public static function update($aData, $nId){				
		$aData = self::getDataByMap(self::$MapArticlesWeb, $aData); 
		return self::dbApi()->updateById(self::Tarticles, $aData, $nId);
	}
        
        public static function delete($nId){
		$aImages = images::getImagesId($nId, 'articles');
        foreach ($aImages as $value) {
            self::deleteImage($value);
        }
        self::dbApi()->deleteById(self::Tarticles,$nId);
		return TRUE;
	}
        
        public static function getAll($bPublic = TRUE,$nLimit = 0){
		$aFilter = array();
		if ($bPublic) $aFilter['status'] = 1;
		$nLimit = ($nLimit) ? intval($nLimit) : 9999;
		//$strOrder = 'date_add DESC';
		$strOrder = '1';
        return self::dbApi()->getAll(self::Tarticles,array(),$aFilter,$strOrder,$nLimit);
	}
        
        public static function getOne($nId){
        return self::dbApi()->getRowById(self::Tarticles,$nId);
	}
        
        public static function getUrlById($nId){
        return self::dbApi()->getOneById(self::Tarticles,$nId,'url');
	}
        
        public static function setImgToBD($nArticleId,$strImgName){
        return self::dbApi()->insertTo(
            self::Timages,array('item_id'=>$nArticleId,'origname'=>$strImgName, 'item_type'=>'articles', 'title'=>'', 'date_add'=>time()));
        }
        
        public static function getImagesForAdmin($nId){
        $aRows = self::dbApi()->_getAll('SELECT * FROM images WHERE item_type="articles" AND item_id='.$nId);
		$aResult = imagesHelper::getImagesArray($aRows);
        return $aResult;
        }
        
        public static function deleteImage($nImgId){
        $aImg = self::dbApi()->getRowById(self::Timages,$nImgId);
        @unlink(sprintf(H::getAbsolutePath(self::IMG_ORIGDIR_TPL), $aImg['item_id'], $aImg['origname']));
        foreach (self::$aSizesNames as $value) {
            unlink(sprintf(H::getAbsolutePath(self::IMG_DIR_TPL), $aImg['item_id'], $nImgId,$value)); 
            }
        self::dbApi()->deleteById(self::Timages,$nImgId);
        //self::checkMainPhoto($aImg['item_id']);
        return TRUE;
            
        }
        
        
	/*public static function setArticles($aData){
		$aData = self::getDataByMap(self::$MapRequestWeb, $aData);
		return self::dbApi()->insertTo(self::Tarticles, $aData);
	}
	
	public static function getArticles(){
		return self::dbApi()->_getAll(sprintf('SELECT * FROM %s ORDER BY id DESC',  self::Tarticles));
	}*/
	
	
	       
		
//	private static function getDataByMap($aMap,$aData){
//		$aResult = array();
//		foreach ($aMap as $key=>$value){
//			if (!empty($aData[$value])) $aResult[$key] = $aData[$value];
//		}
//		return $aResult;
//	}
}
