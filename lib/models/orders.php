<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of projects
 *
 * @author Faust
 */
class orders extends mainApi {
 	
	
	public static function setNew($aData){
		$aData['date_add'] = time();
		$aData['status'] = 0;
		return self::dbApi()->insertTo(self::Torders, $aData);
	}
	
	public static function getAll(){
		return self::dbApi()->_getAll(sprintf('SELECT * FROM %s ORDER BY date_add DESC',  self::Torders));
	}
}