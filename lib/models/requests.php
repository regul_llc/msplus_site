<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of projects
 *
 * @author Faust
 */
class requests extends mainApi {
	
	private static $MapRequestWeb = array(
			'source'		=>'request-source',
			'cname'			=>'name',
			'cphone'		=>'phone',
			'message'		=>'request-message',
			'fullrequest'           =>'fullrequest',
			'ip'			=>'ip',
			'is_bot'		=>'isbot',
			'is_deleted'            =>'is_deleted',
			'status'		=>'status'
	);


	public static function setRequest($aData){
		$aData = self::getDataByMap(self::$MapRequestWeb, $aData);
		$aData['date_add'] = time();
		$aData['status'] = 0;
		return self::dbApi()->insertTo(self::Trequests, $aData);
	}
	
	public static function getRequests(){
		return self::dbApi()->_getAll(sprintf('SELECT * FROM %s WHERE is_deleted=0 ORDER BY date_add DESC',  self::Trequests));
	}
	
	public static function update($aData, $nId){				
		$aData = self::getDataByMap(self::$MapRequestWeb, $aData);
		return self::dbApi()->updateById(self::Trequests, $aData, $nId);
	}
	
	public static function delete($nId){
		self::update(array('is_deleted'=>1), $nId);
		return TRUE;
	}
	
//	private static function getDataByMap($aMap,$aData){
//		$aResult = array();
//		foreach ($aMap as $key=>$value){
//			if (!empty($aData[$value])) $aResult[$key] = $aData[$value];
//		}
//		return $aResult;
//	}
}
