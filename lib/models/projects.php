<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of projects
 *
 * @author Faust
 */
class projects extends mainApi {

    const IMG_DIR_TPL = '/data/images/projects/%d/trimmed/%d_%s.jpg';
    const TYPE_IMG_DIR_TPL = '/data/images/types/%d/trimmed/%d_%s.jpg';
    const IMG_ORIGDIR_TPL = '/data/images/projects/%d/originals/%s';
	const TYPE_IMG_ORIGDIR_TPL = '/data/images/projects/%d/originals/%s';
    private static $aSizesNames = array('mini','small','middle','big');

    public static function getAllCats($bActive=TRUE){
        if ($bActive) {
            $requestAddition = ' WHERE status=1';
        } else {
            $requestAddition = '';
        }
        return self::dbApi()->_getAll('SELECT id,title FROM works_categories'.$requestAddition);
    }
    
    public static function getFullCats($bActive=TRUE){
        if ($bActive) {
            $requestAddition = ' WHERE status=1';
        } else {
            $requestAddition = '';
        }
        return self::dbApi()->_getAll('SELECT * FROM works_categories'.$requestAddition);
    }
    
    public static function getCatFullInfo($nCatId) {
        return self::dbApi()->_getRow('SELECT * FROM works_categories WHERE id='.$nCatId);
    }
    
    public static function getCatInfo($nCatId){
        return self::dbApi()->_getRow('SELECT * FROM works_categories WHERE id='.$nCatId);
    }
    
    
    
    public static function getCatByUrl($sCatUrl){
        $cat = self::dbApi()->_getRow('SELECT * FROM works_categories WHERE url="'.$sCatUrl.'"');
        $imgsIds = self::dbApi()->_getAll('SELECT * FROM images WHERE  item_type = "type" AND item_id = '.$cat['id']);
        $cat['pics'] = array();
        foreach ($imgsIds as $imgId) {
            $cat['pics'][] = array(
                'url' => sprintf(self::TYPE_IMG_DIR_TPL,$cat['id'],$imgId['id'],'big')
            );
        }
        return $cat;
    }
    
    public static function getProjects($nCatId=0, $nLimit=6, $imgSize='mini' ){
        $strQuery = ($nCatId) ? 'cat_id='.$nCatId : '1';
        $projects = self::dbApi()->_getAll(sprintf('SELECT * FROM works WHERE %s ORDER BY id DESC LIMIT %s',$strQuery, $nLimit));
        foreach ($projects as &$project) {
            $picId = self::dbApi()->_getRow('SELECT id FROM images WHERE item_type="project" AND item_id='.$project['id'].' AND is_main = 1');
            $project['picUrl'] = sprintf(self::IMG_DIR_TPL, $project['id'],$picId['id'],$imgSize);
        }
        return $projects;
    }
    
    public static function getProject($nProjectId){
        return self::dbApi()->_getRow(sprintf('SELECT * FROM works WHERE id=%d ORDER BY id DESC',$nProjectId));
    }
    
    public static function addProject($strProjectName=""){		
        return self::dbApi()->insertTo(self::Tprojects,array('title'=>$strProjectName));
    }
    
    public static function updateProject($nProjectId,$aData){ 		
        return self::dbApi()->updateById(self::Tprojects,$aData,$nProjectId);
    }
    
    public static function typeSave($nCatId,$aData){
        $data = array(
            'title' => $aData['title'],
            'status' => $aData['status'],
            'text' => $aData['description']
        );
        return self::dbApi()->updateById(self::Tprcats,$data,$nCatId);
    }
    
    public static function projectSave($nProjectId, $aData) {
        $data = array(
            'title' => $aData['title'],
            'cat_id' => $aData['cat_id'],
            'description' =>$aData['description'],
            'status' => $aData['status'],
            'price' => $aData['price'],
            'square' => $aData['square']
        );
        return self::dbApi()->updateById(self::Tprojects,$data,$nProjectId);
    }
    
    public static function deleteProject($nProjectId){ 		
        $aImages = self::getProjectImages($nProjectId);
        foreach ($aImages as $value) {
            self::deleteProjectImage($value);
        }
        self::dbApi()->deleteById(self::Tprojects,$nProjectId);
        return true;
    }

    public static function setImgToBD($nProjectId,$strImgName){
        return self::dbApi()->insertTo(
            self::Timages,array('item_id'=>$nProjectId,'origname'=>$strImgName, 'item_type'=>'project', 'title'=>'', 'date_add'=>time()));
    }
    
    public static function setTypeImgToBD($nProjectId,$strImgName){
        return self::dbApi()->insertTo(
            self::Timages,array('item_id'=>$nProjectId,'origname'=>$strImgName, 'item_type'=>'type', 'title'=>'', 'date_add'=>time()));
    }
    
    public static function checkMainPhoto($nProjectId) {
        $nCoverId = self::dbApi()->getOneById(self::Tprojects,$nProjectId,'cover');
        $nImgId = self::getMainImg($nProjectId);
        if ($nImgId && $nCoverId && ($nCoverId == $nImgId)) return $nImgId;
        if (!$nImgId){
            $nImgId = self::dbApi()->_getOne(sprintf('SELECT id FROM %s WHERE item_type="project" AND item_id=%d LIMIT 1', 
                self::Timages,  $nProjectId));		
            if (!$nImgId){
                self::dbApi()->updateById(self::Tprojects, array('status'=>0, 'cover'=>0), $nProjectId);			
                $nCoverId = 0;
            }else{
                self::setMainPhoto($nImgId, $nProjectId);
                $nCoverId = $nImgId;
            }			
        } elseif (!$nCoverId) {
            self::setMainPhoto($nImgId, $nProjectId);
            $nCoverId = $nImgId;
        }
        return $nCoverId;
    }

    public static function getMainImg($nProjectId){
        $nImgId = self::dbApi()->_getOne(sprintf('SELECT id FROM %s WHERE is_main=1 AND item_type="project" AND item_id=%d LIMIT 1', 
            self::Timages,  $nProjectId));
        return ($nImgId) ? $nImgId : 0;
    }

    public static function setMainPhoto($nImgId,$nProjectId){
        $strSql = sprintf('UPDATE %s SET is_main=0 WHERE item_type="project" AND item_id=%d',  self::Timages,$nProjectId);
        self::dbApi()->_executeSql($strSql);
        self::dbApi()->updateById(self::Timages, array('is_main'=>1), $nImgId);
        self::dbApi()->updateById(self::Tprojects, array('cover'=>$nImgId), $nProjectId);
        return TRUE;
    }

    public static function getProjectImagesForAdmin($nProjectId){
        $aRows = self::dbApi()->_getAll('SELECT * FROM images WHERE item_type="project" AND item_id='.$nProjectId);
		$aResult = imagesHelper::getImagesArray($aRows);
        return $aResult;
    }
    
    public static function getTypeImagesForAdmin($nProjectId){
        $aRows = self::dbApi()->_getAll('SELECT * FROM images WHERE item_type="type" AND item_id='.$nProjectId);
        $aResult = imagesHelper::getImagesArray($aRows);
        return $aResult;
    }
    
    public static function deleteProjectImage($nImgId){
        $aImg = self::dbApi()->getRowById(self::Timages,$nImgId);
        unlink(sprintf(H::getAbsolutePath(self::IMG_ORIGDIR_TPL), $aImg['item_id'], $aImg['origname']));
        foreach (self::$aSizesNames as $value) {
            unlink(sprintf(H::getAbsolutePath(self::IMG_DIR_TPL), $aImg['item_id'], $nImgId,$value));
        }	
        self::dbApi()->deleteById(self::Timages,$nImgId);
        self::checkMainPhoto($aImg['item_id']);
        return TRUE;
    }
	public static function deleteTypeImage($nImgId){
        $aImg = self::dbApi()->getRowById(self::Timages,$nImgId);
        unlink(sprintf(H::getAbsolutePath(self::TYPE_IMG_ORIGDIR_TPL), $aImg['item_id'], $aImg['origname']));
        foreach (self::$aSizesNames as $value) {
            unlink(sprintf(H::getAbsolutePath(self::TYPE_IMG_DIR_TPL), $aImg['item_id'], $nImgId,$value));
        }	
        self::dbApi()->deleteById(self::Timages,$nImgId);
        //self::checkMainPhoto($aImg['item_id']);
        return TRUE;
    }
	

    /*--- WEB SECTION ---*/
    public static function getCatsForWeb($nLimit=4, $picSize='mini'){
        $strSql = sprintf('SELECT c.*, COUNT(p.id) AS "pcount", p.cover, p.id AS  "cover_pid"
            FROM %s c
            LEFT JOIN %s p ON p.cat_id=c.id AND p.status=1 AND p.cover>0
            WHERE c.status=1 
            GROUP BY c.id ORDER BY c.id ASC',  self::Tprcats, self::Tprojects);
        $cats = self::dbApi()->_getAll($strSql);
        $result = array();
        
        foreach ($cats as $cat) {
            
            $picId = self::dbApi()->_getRow('SELECT id FROM images WHERE item_type="type" AND item_id='.$cat['id'].' AND is_main = 1');
            $cat['picUrl'] = sprintf(self::TYPE_IMG_DIR_TPL, $cat['id'],$picId['id'],$picSize);
            $result[] = $cat;
        }
        return $result;
    }
    
    public static function getProjectImages($nProjectId){
        return self::dbApi()->_getAll('SELECT id FROM images WHERE item_type="project" AND item_id='.$nProjectId, FALSE);
    }

}
