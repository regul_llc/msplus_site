<?php
include_once DR.'lib/WideImage/WideImage.php';
ini_set("memory_limit", "1024M");

class imagesHelper {
	protected static $aProjectsImgSizes = array(
		array('x'=>152, 'y'=>127, 'postfix'=>'small', 'zoom'=>'crop'),
		array('x'=>318, 'y'=>188, 'postfix'=>'mini', 'zoom'=>'crop'),
		array('x'=>356, 'y'=>231, 'postfix'=>'middle', 'zoom'=>'crop'),
		array('x'=>478, 'y'=>284, 'postfix'=>'big', 'zoom'=>'crop'),
		array('x'=>1200, 'y'=>1000, 'postfix'=>'large', 'zoom'=>'scale')
	);
        
    protected static $aTypesImgSizes = array(
		array('x'=>212, 'y'=>149, 'postfix'=>'mini', 'zoom'=>'crop'),
		array('x'=>320, 'y'=>210, 'postfix'=>'middle', 'zoom'=>'crop'),
		array('x'=>485, 'y'=>364, 'postfix'=>'big', 'zoom'=>'crop'),
		array('x'=>1200, 'y'=>1000, 'postfix'=>'large', 'zoom'=>'scale')
	);
	
	protected static $aReviewsImgSizes = array(
		array('x'=>100, 'y'=>100, 'postfix'=>'mini', 'zoom'=>'crop'),
        array('x'=>200, 'y'=>200, 'postfix'=>'middle', 'zoom'=>'crop')
	);
	
	private $image = null;


	const UPLOAD_DIR = '/data';
	const UPLOADD_PROJECTS_DIR = '/data/images/projects';
	const UPLOAD_TYPES_DIR = '/data/images/types';
	const UPLOAD_REVIEWS_DIR = '/data/images/reviews';
	const UPLOAD_TRASH_DIR = '/data/images/trash';
	
	const ORIGINALS_DIR = '/originals/';
	const TRIMMED_DIR = '/trimmed/';
	
        
        
	public function __construct() {
				
	}
	
	public static function UploadImage($aFile,$nId,$strItemType){		
		list($strOrigDir, $strTrimDir) = self::getImageDirs($strItemType,$nId);		
		$strFileName = self::getUniqFileName($aFile['name'], $strOrigDir);
		$strFilePath = $strOrigDir.$strFileName;
		if(is_uploaded_file($aFile['tmp_name']) && move_uploaded_file($aFile['tmp_name'], $strFilePath)){
			$nImgId = self::setImgToBd($nId, $strFileName, $strItemType);
			self::createImageResizes($strItemType,$nImgId, $strTrimDir, $strFilePath);
			$strBigImgPath = sprintf('%s/%d%s%d_big.jpg', self::getRootDirByType($strItemType), $nId, self::TRIMMED_DIR, $nImgId);
			$strSmallImgPath = sprintf('%s/%d%s%d_mini.jpg', self::getRootDirByType($strItemType), $nId, self::TRIMMED_DIR, $nImgId);
			$strDelUrl = sprintf('/ajax/deletePhoto?type=%s&id=%d',$strItemType,$nImgId);
			return array(
				'id'			=>$nImgId,
				'name'			=>$strFileName,
				'url'			=>$strBigImgPath,
				'thumbnail_url'	=>$strSmallImgPath,
				'delete_url'	=>$strDelUrl,  
				"delete_type"	=>"GET");
		}else{
			return FALSE;
		}		
	}
	
	public function deleteImage($strType,$nImageId){
		
	}
	
	public static function getImagesArray($aImages){
		$aResult = array();
        foreach ($aImages as $value){
			$strItemType = $value['item_type'];
			$nImgId = $value['id'];
			$nId = $value['item_id'];
			$strBigImgPath = sprintf('%s/%d%s%d_big.jpg', self::getRootDirByType($strItemType), $nId, self::TRIMMED_DIR, $nImgId);
			$strSmallImgPath = sprintf('%s/%d%s%d_mini.jpg', self::getRootDirByType($strItemType), $nId, self::TRIMMED_DIR, $nImgId);
			$strDelUrl = sprintf('/ajax/deletePhoto?type=%s&id=%d',$strItemType,$nImgId);
            $aResult[] = array(
                'id'			=>$nImgId,
				'name'			=>$value['origname'],
				'url'			=>$strBigImgPath,
				'thumbnail_url'	=>$strSmallImgPath,
				'delete_url'	=>$strDelUrl,  
                'is_main'	=> $value['is_main'],
                "delete_type"	=> "GET"
            );
        }
		return $aResult;
	}

	private static function createImageResizes($strImgType,$nImgId,$strTrimDir,$strFilePath){
		$aImgParams = @getimagesize($strFilePath);
		if (!$aImgParams) return ' File not is image: '.$strFilePath;
		$oImage = self::loadOriginalImage($strFilePath);
		list($img_width, $img_height) = $aImgParams;
		$aSizes = self::getSizes($strImgType);
		foreach ($aSizes as $aSize){
			$oResizedImage = self::zoomImage($oImage, $aSize);
			$strNewFile = sprintf('%s%d_%s.jpg', $strTrimDir,$nImgId,$aSize['postfix']);
			$oResizedImage->saveToFile($strNewFile);			
		}
		return TRUE;
	}
	
	private static function zoomImage($oImage, $aSizeParams, $aPosition=array('left'=>'center','top'=>'center'), $aRgbCanvas=array(0,0,0)){
		// $strZoomType - тип масштабирования. 
		$strZoomType = (!empty($aSizeParams['zoom'])) ? $aSizeParams['zoom'] : 'default';
		$aSize = array('x'=>$aSizeParams['x'], 'y'=>$aSizeParams['y']);
		$oResultImage = NULL;
		switch ($strZoomType){
			case 'crop': //обрезка
				$oResultImage = $oImage->resize($aSize['x'], $aSize['y'], 'outside', 'any')->crop($aPosition['left'],$aPosition['top'],$aSize['x'],$aSize['y']);
				break;
			case 'canvas': // уменьшение и заливка зветом остатков
				$oCanvasColor = $oImage->allocateColor($aRgbCanvas[0],$aRgbCanvas[1],$aRgbCanvas[2]);
				$oResultImage = $oImage->resize($aSize['x'], $aSize['y'])->resizeCanvas($aSize['x'],$aSize['y'], $aPosition['left'],$aPosition['top'], $oCanvasColor);
				break;
			case 'scale': //пропорциональное уменьшение
				$oResultImage = $oImage->resize($aSize['x'], $aSize['y'], 'outside', 'any');
				break;
			default :
				$oResultImage = $oImage->resize($aSize['x'], $aSize['y'], 'outside', 'any')->crop($aPosition['left'],$aPosition['top'],$aSize['x'],$aSize['y']);
				break;
		}
		return $oResultImage;		
	}	

	private static function getSizes($strImageType){
		switch ($strImageType){
			case 'project'	: return self::$aProjectsImgSizes;
			case 'type'		: return self::$aTypesImgSizes;
			case 'review'	: return self::$aReviewsImgSizes;
			default			: return false;
		}
	}

	private static function setImgToBd($nItemId,$strName,$strType){
		switch ($strType){
			case 'project':
				$nImgId = projects::setImgToBD($nItemId, $strName);
				break;
			case 'type':
				$nImgId = projects::setTypeImgToBD($nItemId, $strName);
				break;
			case 'review':
				$nImgId = reviews::setImgToBD($nItemId, $strName);
				break;
			default :
				$nImgId = 0;
				break;
		}
		return $nImgId;
	}
	
	

	private static function getImageDirs($strType, $nId){
		$strRootDir = self::getRootDirByType($strType);		
		$strImageDir = self::getAbsolutePath($strRootDir);
		if (!file_exists($strImageDir)) { mkdir($strImageDir); }
		if (!file_exists($strImageDir.'/'.$nId) || !is_dir($strImageDir.'/'.$nId)) {
			mkdir ($strImageDir.'/'.$nId);
			mkdir ($strImageDir.'/'.$nId.self::ORIGINALS_DIR);
			mkdir ($strImageDir.'/'.$nId.self::TRIMMED_DIR);
		}
		return array($strImageDir.'/'.$nId.self::ORIGINALS_DIR, $strImageDir.'/'.$nId.self::TRIMMED_DIR);
	}
	
	private static function getRootDirByType($strType){
		switch ($strType){
			case 'project'	: return self::UPLOADD_PROJECTS_DIR;
			case 'type'		: return self::UPLOAD_TYPES_DIR;
			case 'review'	: return self::UPLOAD_REVIEWS_DIR;
			default			: return self::UPLOAD_TRASH_DIR;
		}
	}
        
	private static function loadOriginalImage($strFilePath){
		if (!file_exists($strFilePath))	return FALSE;
		return WideImage::load($strFilePath);
	}
	
	private static function getAbsolutePath($strDir){
		return $_SERVER['DOCUMENT_ROOT'].$strDir;
	}
	
	private static function getUniqFileName($strFileName,$strFilePath){
		$aFname = pathinfo($strFileName);
		$strFileName = H::translit($aFname['filename']).'.'.$aFname['extension'];
		$strFilePath = sprintf('%s/%s', $strFilePath,$strFileName);
		if (file_exists($strFilePath)){		
			$aFname = pathinfo($strFileName);
			$strFileName = sprintf('%s_%s.%s',$aFname['filename'],time(),$aFname['extension']);
		}
		return $strFileName;
	}
}
