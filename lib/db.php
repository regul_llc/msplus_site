<?php
/** 
 * Класс для работы с базой данных
 * 
 * @author Dmitry Ivlev
 * @version 1.5  
 * 
 * Начало работы: Mon Jul 09 18:17:36 GMT 2012
 * Окончание работы: Mon Jul 09 18:17:36 GMT 2012
 */
class db {	
	
	/** Типы данных */
	const DATA_TYPE_INT = 'int';
	const DATA_TYPE_FLOAT = 'float';
	const DATA_TYPE_VARCHAR = 'varchar';
	const DATA_TYPE_TEXT = 'text';
	const DATA_TYPE_ENUM = 'enum';
	
	/** Префикс таблиц сайта **/
	protected $prefix = '';
	protected $host = '';
	protected $user = '';
	protected $pass = '';
	protected $name = '';
	protected $charset = 'utf8';
	protected $link = null;
	
	
	/** Конструктор
	 *
	 * @param array $conf - данные для доступа */
	public function __construct($conf) {
		$this->prefix = $conf['prefix'];
		$this->host = $conf['host'];
		$this->user = $conf['user'];
		$this->pass = $conf['pass'];
		$this->name = $conf['name'];
		$this->charset = $conf['charset'];
	}
	
	
	/** Соединение с DB **/
	public function connect() {
		$this->link = @mysql_connect($this->host, $this->user, $this->pass);
		
		if(!$this->link) echo "НЕТ СОЕДИНЕНИЯ!!!";		
		if(!mysql_select_db($this->name, $this->link)) echo "НЕ ВЫБРАНА БАЗА!!!";		
		
		mysql_set_charset($this->charset);
	}
	
	
	/** Проверка на существование таблицы
	 *
	 * @param string $table - Имя таблицы
	 * @return boolean - Существует или нет */
	public function isTable($table) {
 		return mysql_num_rows(mysql_query("SHOW TABLES LIKE '".$this->prefix.mysql_real_escape_string($table)."'"));
	}
	
	
	public function isField($table, $field) {
		$res = mysql_query(self::p("SELECT ?n FROM ?n WHERE 0", $field, $this->prefix.$table));
		return (($res) ? true : false);
	}
	

	/**
	 * Содает новую таблицу, с заданным именем
	 * с одним полем: ID (AUTO_INCREMENT, PRIMARY KEY)
	 *
	 * @param unknown_type $strName
	 * @return unknown
	 */
	public function createTable($strName) {
		if ($strName != '') {
			return mysql_query(self::p("CREATE TABLE IF NOT EXISTS ?n (`id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;", $strName));
		} else {
			return false;
		}
	}
	
	
	public function fieldSet($strTable, $strField, $strType, $strDefault='') {
		if ($this->isTable($strTable)) {
			if ($strDefault=='CURRENT_TIMESTAMP') $strDefault = '';
			$aRealColums = $this->getRealFieldsName($strTable);

			$strSql = self::p("ALTER TABLE ?n", $strTable);
			$strSql .= (in_array($strField, $aRealColums)) ? self::p(' CHANGE ?n ', $strField) : ' ADD';
			$strSql .= self::p(' ?n ', $strField).$strType;
			$strSql .= ($strDefault=='') ? ' NULL' : " DEFAULT '".$strDefault."'";
//			echo $strSql.'<br>';
			return mysql_query($strSql);
		}
		
		return false;
	}
	
	/** Возвращает следующие свойства поля:
	 * [Field]
       [Type]
       [Null] 
       [Key]
       [Default]
       [Extra] 
	 *
	 * @param str $strTable
	 * @param str $strField
	 * @return array|false */
	public function getFieldInfo($strTable, $strField) {
		$aFields = $this->_getAll(self::p("SHOW COLUMNS FROM ?n", $this->prefix.$strTable));
		foreach ($aFields as $aField) {
			if ($aField['Field']==$strField) {
				return $aField;
			}
		}
		
		return false;
	}
	
	
	public function fieldSetName($strTable, $strField, $strNewField) {
		if ($this->isTable($this->prefix.$strTable)) {
			$aFieldInfo = $this->getFieldInfo($strTable, $strField);

			mysql_query(self::p("ALTER TABLE ?n CHANGE ?n ?n ?s", $this->prefix.$strTable, $strField, $strNewField, $aFieldInfo['Type']));
		}
	}
	
	
	public function delField($strTable, $strField) {
		if ($this->isTable($this->prefix.$strTable) && $this->isField($this->prefix.$strTable, $strField)) {
			$res = mysql_query(self::p("ALTER TABLE ?n DROP ?n", $this->prefix.$strTable, $strField));
			return (($res) ? true : false);
		}
		
		return false;
	}
	
	
	/** Проверяет поля на существование в таблице
	 *
	 * @param string $table - Имя таблицы
	 * @param array $fields - Массив имен полей для проверки существования в указанной
	 * @return array - Массив имен полей из входного массива существующие в проверяемой таблице */
	public function verifyFields($table, $fields) {
		if ($this->isTable($table)) {
			$real_colums = $this->_getAll(self::p("SHOW COLUMNS FROM ?n", $this->prefix.$table));

			$result_fields = array();
			foreach ($real_colums as $real_colum) {
				if (isset($fields[$real_colum['Field']])) {
					$clear_field = $this->clearDataByType($fields[$real_colum['Field']], $real_colum['Type']);
					if ($clear_field!==false) {
						$result_fields[$real_colum['Field']] = $clear_field;
					}
				}
			}

			return $result_fields;
		} else return array();
	}
	
	
	/** Чистит содержимое полей согласно типу
	 * Необходимо перед вставкой или обновлением данных
	 *
	 * @param mix $data - Значение поля
	 * @param string $type - Тип поля
	 * @return mix - очищенное значение */
	public function clearDataByType($data, $type=self::DATA_TYPE_TEXT) {
		$types = array();
		$_type = explode('(', $type);
		$_type = $_type[0];
		
		switch ($_type) {
			case self::DATA_TYPE_INT:
				return (int)$data;
			break;
				
			case self::DATA_TYPE_FLOAT:
				return (float)$data;
			break;
				
			case self::DATA_TYPE_VARCHAR:
				return mysql_real_escape_string($data);
			break;
				
			case self::DATA_TYPE_ENUM:
				$real_vars = substr($type, 5, (strlen($type)-6));
				$real_vars = explode(',', $real_vars);
				$var_for_return = '';
				foreach ($real_vars as $real_var) {
					if (substr($real_var, 1, $real_var-1) == $data) $var_for_return = $data;
				}
				return $var_for_return;
			break;
				
			case self::DATA_TYPE_TEXT: 
				return mysql_real_escape_string(trim($data)); 
			break;
			
			default: return mysql_real_escape_string(trim($data));
		}
		return false;
	}
	
	
	/** Чистит содержимое полей согласно типу
	 * Необходимо перед вставкой или обновлением данных
	 *
	 * @param mix $data - Значение поля
	 * @param string $type - Тип поля
	 * @return mix - очищенное значение */
	static public function clear($data, $type=self::DATA_TYPE_TEXT) {
		$types = array();
		$_type = explode('(', $type);
		$_type = $_type[0];
		
		switch ($_type) {
			case self::DATA_TYPE_INT:
				return (int)$data;
			break;
				
			case self::DATA_TYPE_FLOAT:
				return (float)$data;
			break;
				
			case self::DATA_TYPE_VARCHAR:
				return mysql_real_escape_string($data);
			break;
				
			case self::DATA_TYPE_ENUM:
				$real_vars = substr($type, 5, (strlen($type)-6));
				$real_vars = explode(',', $real_vars);
				$var_for_return = '';
				foreach ($real_vars as $real_var) {
					if (substr($real_var, 1, $real_var-1) == $data) $var_for_return = $data;
				}
				return $var_for_return;
			break;
				
			case self::DATA_TYPE_TEXT: 
				return mysql_real_escape_string(trim($data)); 
			break;
			
			default: return mysql_real_escape_string(trim($data));
		}
		return false;
	}
	
	
	/**
	 * Возвращает названия всех полей указанной таблицы
	 *
	 * @param string $table - Имя таблицы
	 * @return array - массив названий полей указанной таьлицы */
	public function getRealFieldsName($table) {
		$colums = $this->_getAll(self::p("SHOW COLUMNS FROM ?n", $this->prefix.$table));
		$real_fields_name = array();
		foreach ($colums as $colum) {
			$real_fields_name[] = $colum['Field'];
		}
		return $real_fields_name;
	}
	
	
	/** Добавляет запись в таблицу
	 *
	 * @param string $table - Имя таблицы
	 * @param array $fields - массив значений для добавления
	 * @return int - ID новой записи */
	public function add($table, $fields) {
		if ($this->isTable($table)) {
			$verify_data = $this->verifyFields($table, $fields);
			
			if (count($verify_data) > 0) {
				$real_colums = $this->getRealFieldsName($table);

				$values = array();
				foreach ($real_colums as $real_column) {
					$values[] = (isset($verify_data[$real_column]) && $verify_data[$real_column]) ? "'".$verify_data[$real_column]."'" : (($real_column=='id') ? 'NULL' : "''");
				}
				
				$fields = '`'.join('`, `', $real_colums).'`';
				$values = join(' ,', $values);
				
				$sql = "INSERT INTO `".$this->prefix.$table."` (".$fields.") VALUES (".$values.");";
				mysql_query($sql);
				return mysql_insert_id();
			} 
		} 
		return false;
	}
	
	
	/**
	 * @todo Добавить функцию массового добавления
	 *
	 */
	public function addAll($table, $data) {
		
	}
	
	
	/** Обновление данных
	 *
	 * @param string $table - Имя таблицы
	 * @param array $fields - Массив значений
	 * @param string $where - условие
	 * @return mix - результат обновления */
	public function update($table, $fields, $where) {
		if ($this->isTable($table)) {
			$verify_data = $this->verifyFields($table, $fields);
			
			if (count($verify_data)) {
				$set = array();
				foreach ($verify_data as $name=>$value) {
					$set[] = "`".$name."` = '".$value."'";
				}
				$set = join(', ', $set);
				$where = ($where!='') ? 'WHERE '.$where : '';
				$sql = "UPDATE `".$this->prefix.$table."` SET ".$set." ".$where;

				return mysql_query($sql);
			}
		}
	}
	
	
	/** Удаление
	 *
	 * @param string $table - Имя таблицы
	 * @param string $where - Условие */
	public function delete($table, $where) {
		if ($this->isTable($table)) {
			$where = ($where!='') ? ' WHERE '.$where : '';
			$sql = self::p("DELETE FROM ?n", $this->prefix.$table).$where;
			return mysql_query($sql);
		}
	}
	
	
	/** Простой запрос к базе (возвращает первое значение)
	 *
	 * @param string $sql
	 * @return array - массив полей записи */
	public function _get($sql) {
		$res = mysql_query($sql);
		return mysql_fetch_assoc($res);
	}
	
	
	public function get($table, $where = '') {
		if ($this->isTable($table)) {
			$where = ($where!='') ? 'WHERE '.$where : '';
			$sql = "SELECT * FROM `".$this->prefix.$table."` ".$where;
			$res = mysql_query($sql);
			return mysql_fetch_assoc($res);
		}
	}
	
	public function getRowById($table, $id) {
		if ($this->isTable($table)) {
			$sql = self::p("SELECT * FROM ?n WHERE `id` = ?i  ", $this->prefix.$table, $id);
			$res = mysql_query($sql);
			return mysql_fetch_assoc($res);
		}
		
		return false;
	}
	
	public function getRow($table, $where) {
		if ($this->isTable($table)) {
			$sql = self::p("SELECT * FROM ?n WHERE ".$where, $this->prefix.$table);
			$res = mysql_query($sql);

			return mysql_fetch_assoc($res);
		}
		
		return false;
	}
	
	public function getOne($table, $field, $where) {
		if ($this->isTable($table)) {
			$sql = self::p("SELECT ?p FROM ?n WHERE ?s  ", $field, $this->prefix.$table, $where);
			$res = mysql_query($sql);
			$rez = mysql_fetch_array($res);
			
			return $rez[0];
		}
		
		return false;
	}

	
	public function getAll($table, $where = '') {
		if ($this->isTable($table)) {
			$where = ($where!='') ? 'WHERE '.$where : '';
			$sql = "SELECT * FROM `".$this->prefix.$table."` ".$where;
			$res = mysql_query($sql);
			
			$result = array();
			while ($rez = mysql_fetch_assoc($res)) $result[] = $rez;			
			return $result;
		}
	}
	
	
	/** Простой запрос к базе (возвращае все значения)
	 *
	 * @param string $sql
	 * @return array - массив полей записи */
	public function _getAll($sql) {
		$res = mysql_query($sql);
		$result = array();
		while ($rez = mysql_fetch_assoc($res)) $result[] = $rez;
		return $result;
	}
	
	
	public function getPrefix() {
		return $this->prefix;
	}
	
	
	/**
	 * Аналог sprtinf с добавлением типов
	 * 
	  * ?s ("string")  - strings (also DATE, FLOAT and DECIMAL)
	  * ?i ("integer") - the name says it all 
	  * ?n ("name")    - identifiers (table and field names) 
	  * ?a ("array")   - complex placeholder for IN() operator  (substituted with string of 'a','b','c' format, without parentesis)
	  * ?u ("update")  - complex placeholder for SET operator (substituted with string of `field`='value',`field`='value' format)
	  * and
	  * ?p ("parsed") - special type placeholder, for inserting already parsed statements without any processing, to avoid double parsing.
	  * 
	 * @param unknown_type $args
	 * @return unknown
	 */
	static public function p($args) {
		return self::prepareQuery(func_get_args());
	}
	
	
	static public function prepareQuery($args) {
		$query = '';
		$raw   = array_shift($args);
		$array = preg_split('~(\?[nsiuap])~u',$raw,null,PREG_SPLIT_DELIM_CAPTURE);
		$anum  = count($args);
		$pnum  = floor(count($array) / 2);
		if ( $pnum != $anum ) {
			die("Number of args ($anum) doesn't match number of placeholders ($pnum) in [$raw]");
		}

		foreach ($array as $i => $part)	{
			if ( ($i % 2) == 0 ) {
				$query .= $part;
				continue;
			}

			$value = array_shift($args);
			switch ($part) {
				case '?n':
					$part = self::escapeIdent($value);
					break;
				case '?s':
					$part = self::escapeString($value);
					break;
				case '?i':
					$part = self::escapeInt($value);
					break;
				case '?a':
					$part = self::createIN($value);
					break;
				case '?u':
					$part = self::createSET($value);
					break;
				case '?p':
					$part = $value;
					break;
			}
			$query .= $part;
		}
		return $query;
	}

	static public function escapeInt($value) {
		if ($value === NULL) return 'NULL';
		if (!is_numeric($value)) return FALSE;
		if (is_float($value)) return number_format($value, 0, '.', ''); // may lose precision on big numbers

		return $value;
	}

	static public function escapeString($value) {
		if ($value === NULL) return 'NULL';
		return	mysql_real_escape_string($value);
	}

	static public function escapeIdent($value) {
		if ($value) {
			return "`".str_replace("`","``",$value)."`";
		} else {
//			$this->error("Empty value for identifier (?n) placeholder");
		}
	}

	static public function createIN($data) {
		if (!is_array($data)) {
//			$this->error("Value for IN (?a) placeholder should be array");
			return;
		}
		
		if ($data === NULL) return 'NULL';
		
		$query = $comma = '';
		foreach ($data as $value) {
			$query .= $comma."'".self::escapeString($value)."'";
			$comma  = ",";
		}

		return $query;
	}

	static public function createSET($data) {
		if (!is_array($data)) {
//			$this->error("SET (?u) placeholder expects array, ".gettype($data)." given");
			return;
		}
		
		if (!$data) {
//			$this->error("Empty array for SET (?u) placeholder");
			return;
		}
		
		$query = $comma = '';
		foreach ($data as $key => $value) {
			$query .= $comma.self::escapeIdent($key).'='.self::escapeString($value);
			$comma  = ",";
		}
		
		return $query;
	}
}

?>