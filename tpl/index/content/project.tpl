<div class="wrapper">			
			
	<div class="text-block">
			<h1>{$aCat.title}</h1>
			{$aCat.text}
	</div>

	<div class="slideshow">
		<div class="carousel-container">
			<div id="icarousel">
			{foreach item=item from=$aCat.pics}
				<div class="slide">
					<a class="fancybox" rel="group" href="{$item.url}">
						<img src="{$item.url}" width="485" title="{$aCat.title}"/>
					</a>
				</div>
			{/foreach}						
			</div>
		</div>
	</div>
<!-----			
	{if $aProjects}
		<h3 class="to_l">{$aCat.title} &mdash; готовые решения</h3>
		<div class="works-v2">
		{foreach item=item from=$aProjects}
			<div class="works-v2-block">
                            <div class="works-v2-block-header">
                                <a>{$item.title}</a>
                            </div>
                            <img class="works-v2-block-img" src="{$item.picUrl}" width="318px" height="200px" />
                            <div class="works-v2-block-text">
                                <span>Площадь:{$item.square}м<sup>2</sup></span>
                                <span>{$item.price|number_format:0:",":" "} руб.</span>
                                <div class="works-v2-block-text-b"></div>
                            </div>
                            <div class="works-v2-block-img-h">
                                <a href="/prices/#project-{$item.id}" class="works-v2-block-img-icon __cart buy-project-btn" 
                                data-source="mainpage-projectblock" data-message="заказ типового решения: {$item.title}"></a>
                                <a href="/prices/#project-{$item.id}" class="works-v2-block-img-icon __eye open-custom-win"
                                data-winId="projectBy" data-id="{$item.id}"></a>
                                <div class="works-v2-block-img-b"></div>
                            </div>
                            <!--div class="works-v2-block-price"><span>{$item.price|number_format:0:",":" "} руб.</span></div-->
                        <!--/div>
		{/foreach}
		</div>
	{/if}
----------------------------------------------------------------->   
        {if $aProjects}
        <div class="works-v2 clearfix">
            {foreach item=item from=$aProjects}
                <div class="works-v2-block">
                    <img class="works-v2-block-img" src="{$item.picUrl}" width="318px" height="200px" style="z-index:1" title="{$item.title}"/>
                    <div class="works-v2-block-prices" style="z-index:2">{$item.price|number_format:0:",":" "} р</div>
                    <div class="works-v2-block-description">
                        <div class="works-v2-block-description-head">
                            {$item.title}
                        </div>
                        <div class="works-v2-block-description-body">
                            {$item.description|trim|strip_tags|truncate:75}<br>
                            Площадь: {$item.square} м<sup>2</sup>
                        </div>
                    </div>
                    <div class="works-v2-block-button buy-project-btn">
                        <span>Вызвать замерщика</span>
                        <!--div class="works-v2-block-text-b"></div-->
                    </div>
                    <!--div class="works-v2-block-img-h">
                        <a href="/prices/#project-{$item.id}" class="works-v2-block-img-icon __cart buy-project-btn" 
                        data-source="mainpage-projectblock" data-message="заказ типового решения: {$item.title}"></a>
                        <a href="/prices/#project-{$item.id}" class="works-v2-block-img-icon __eye open-custom-win"
                        data-winId="projectBy" data-id="{$item.id}"></a>
                        <div class="works-v2-block-img-b"></div>
                    </div-->
                    <!--div class="works-v2-block-price"><span>{$item.price|number_format:0:",":" "} руб.</span></div-->
                </div>
            {/foreach}          
        </div>
        {/if}
<!---------------------------------------------------------------------->    
	
	<div class="clearfix"></div>

	{*<div class="text-block">
		<h1>Заголовок для SEO</h1>
		<p>Если добавить к этому наличие огромного количества оттенков от насыщенных до спокойных пастельных, что помогает осуществить любые, даже самые смелые дизайнерские фантазии, то установка глянцевых натяжных потолков - лучшее решение для игры с цветом и пространством любого помещения. Что нередко используют при оформлении не только жилых домов и квартир, но и изысканных ресторанных залов и шикарных гостиных холлов.</p>
	</div>*}

	<form class="horizont-form send-request-form">
		<div class="horizont-form__heading">Оставьте заявку и получите замер и дизайн-проект бесплатно!</div>
			<input type="hidden" name="request-source" value="works-horizontal-form-center">
			<input type="hidden" name="request-message" value="Заказ на замер со страницы: {$aCat.title}">
			<input class="horizont-form__input" type="text" name="name" placeholder="Ваше имя">
			<input class="horizont-form__input mask_phone" type="text" name="phone" placeholder="Ваш телефон">
			<input type="checkbox" name="human" class="human">
			<input type="text" name="human_t" class="human">
			<button class="promo-contacts__button horizont-form__button __submit" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Отправить</button>
	</form>
			
	{*<div class="text-block">
		<h2 class="to_l">Ещё какой-то заголовок для SEO</h2>
		<p>Если добавить к этому наличие огромного количества оттенков от насыщенных до спокойных пастельных, что помогает осуществить любые, даже самые смелые дизайнерские фантазии, то установка глянцевых натяжных потолков - лучшее решение для игры с цветом и пространством любого помещения. Что нередко используют при оформлении не только жилых домов и квартир, но и изысканных ресторанных залов и шикарных гостиных холлов.</p>
	</div>*}

	{if $aReviews}
	<div class="user-reviews">
		<h2>Отзывы наших клиентов</h2>
		<ul class="user-reviews__list bxslider">
		{foreach item=item from=$aReviews}
			<li class="user-reviews__item">
				<a href = "/reviews/#{$item.id}"><img src="/data/images/reviews/{$item.id}/trimmed/{$item.cover}_mini.jpg" height="100" width="100" alt="Пользователь"></a>
                                <a href = "/reviews/#{$item.id}"><strong>{$item.author}</strong></a>
                                <a href = "/reviews/#{$item.id}"><span>{$item.title}<br></span></a>
				<hr>
                                <a href = "/reviews/#{$item.id}"><p>{$item.text|truncate:150}</p></a>
			</li>
		{/foreach}					
		</ul>
	</div>
	{/if}

	{*<div class="text-block">
		<h2 class="to_l">Может быть ещё какой-то заголовок подлинее</h2>
		<p>Если добавить к этому наличие огромного количества оттенков от насыщенных до спокойных пастельных, что помогает осуществить любые, даже самые смелые дизайнерские фантазии, то установка глянцевых натяжных потолков - лучшее решение для игры с цветом и пространством любого помещения. Что нередко используют при оформлении не только жилых домов и квартир, но и изысканных ресторанных залов и шикарных гостиных холлов.</p>
	</div>*}

	<div class="user-reviews other-cat">
		<h2>Другие категории натяжных потолков</h2>
		<ul class=" bxslider">
		{foreach item=item from=$aCats}
		<li class="cat-v2-item">
			<a href="/works/{$item.url}" class="cat-v2-item-head">{$item.title}</a>
			<div class="cat-v2-item-left">
				<img src="{$item.picUrl}" title="{$item.title}"/>
				<a href="/works/{$item.url}" class="cat-v2-item-btn">смотреть подробнее...</a>
			</div>
			<span>{$item.text|strip_tags|truncate:"200"}</span>
		</li>
		{/foreach}				
		</ul>
	</div>
</div>