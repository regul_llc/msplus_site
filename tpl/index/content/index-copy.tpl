<div class="wrapper">
    <!--div class="text-block">
        <h1>Натяжные потолки в Ростове-на-Дону</h1>
        <h4>Индивидуальный подход и лучшие цены на натяжные потолки.</h4>
        <p>Гибкий подход в ценовой политике и система скидок с индивидуальным подходом к каждому позволит воплотить любые Ваши желания в жизнь. Многочисленные положительные отзывы о натяжных потолках установленных нашими специалистами тому подтверждение.</p>
        <p>Аскетический стиль, плавные линии романизма или многоуровневые своды в стиле дворцов VII века - это вопрос Вашего стиля жизни.&nbsp;Технические моменты - это наша забота.</p>
        <p>Оставьте свой вопрос или заказ на выезд специалиста по номеру <strong>+7 (863) 229-55-06</strong> и Вы сами убедитесь, что ремонт - не такая уж большая проблема.</p>
    </div-->
    <div class="text-block">
        <h1>Натяжные потолки в Ростове-на-Дону от 300 рублей за м<sup>2</sup>.</h1>
        <br />

        <p>
            Компания «MSPlus» устанавливает <strong>натяжные потолки в Ростове-на-Дону</strong> по выгодной стоимости от 300 рублей за квадратный метр. В нашем активе – реализация типовых проектов небольшой площади, <a href="/works/glyancevie_natyajnie_potolki" title="Глянцевые натяжные потолки" class="dashed-link">глянцевых</a> и <a href="/works/matovie_natyajnie_potolki" title="Матовые натяжные потолки" class="dashed-link">матовых</a> модификаций с <a href="/works/podsvetka_natyajnih_potolkov" title="Натяжные потолки с подсветкой" class="dashed-link">подсветкой</a> и без нее, <a href="/works/mnogourovnevie_natyajnie_potolki" title="Двухуровневые натяжные потолки" class="dashed-link">двухуровневых</a> конструкций, а также – эксклюзивных вариантов «<a href="/works/photoprint_natyajnie_potolki" title="Натяжные потолки звездное небо" class="dashed-link">звездного неба</a>». 
            Компания разрабатывает проекты в формате полного цикла – от создания дизайн-макета до сервисного обслуживания установленных потолков. В <a href="/prices" title="Цены на натяжные потолки Ростов-на-Дону" class="dashed-link">стоимость</a> заказа входят цены на монтажный профиль, изготовление полотна и сборку изделия. Мастера нашей компании используют взрывобезопасные полимерно-композитные газовые баллоны швейцарского производства, 
            что обеспечивает безопасность монтажа натяжных конструкций на объектах любого типа. Потолки от «MSPlus» могут использоваться как в жилых, так и в коммерческих помещениях со сложными условиями эксплуатации (перепады атмосферы, повышение влажности и т.д.)

        </p>

        <!--h3>Качественная установка, бесплатный выезд и постоянные скидки<br />
            для жителей Ростова и Ростовской области.</h3>
        <br />
        <p>
            Помимо сведений о гарантиях, о стоимости  и характеристиках материалов, на сайте можно найти  дизайнерские решения  для точной визуализации Ваших желаний. Необходимо лишь открыть раздел "Работы".  Минимальный срок  установки натяжного потолка - один день, стоимость варьируется от 1500 рублей, без учета действующих акций.
        </p>

        <p>
            Помните, что визит сотрудника для конкретизации расчетов ни к чему не обязывает и ничего не стоит. Вас не будут осаждать орды вышколенных менеджеров неурочными звонками, ежедневными смс или письмами в соцсетях. Оставив заявку, Вы просто получите достоверную информацию по срокам выполнения заказа и рекомендации по выбору текстуры, типа, цветовой гаммы потолочного покрытия с учетом специфики помещения и установленного бюджетного  лимита.
        </p-->
        
        <h3>Как мы работаем:</h3>
        
        <ul>
            <li>Самостоятельно готовим дизайн-проект, выполняем все необходимые замеры, изготавливаем элементы  и в минимальные сроки монтируем их на объекте. <strong>Установка натяжных потолков в Ростове-на-Дону и Батайске</strong> производится в течение 1 дня.</li>
            <li>Используем материалы немецкого, бельгийского, итальянского и французского производства. Все полотна и комплектующие имеют сертификацию и соответствуют европейским стандартам качества.</li>
            <li>Предоставляем год бесплатного обслуживания и гарантию 10 лет на механическую прочность, устойчивость форм и красок потолков.</li>
        </ul>
    </div>
    

    <!--div class="our-advantages">
        <h2>Заказывая установку натяжных потолков, вы получаете:</h2>	
        <div class="advantages">
            <div class="advantages-block">
                <div class="ad-img pen"></div>
                <span>Бесплатный замер</span>
                <div class="advantages-block-sub">
                    Выезд замерщика к вам домой или в офис бесплатно
                </div>
            </div>
            <div class="advantages-block">
                <div class="ad-img gift"></div>
                <span>Дизайн<br> в подарок</span>
                <div class="advantages-block-sub">
                    Эксклюзивные дизайн-проекты только для Вас
                </div>
            </div>
            <div class="advantages-block">
                <div class="ad-img key"></div>
                <span>Бесплатный сервис</span>
                <div class="advantages-block-sub">
                    1 год бесплатного обслуживания Ваших потолков
                </div>
            </div>
            <div class="advantages-block">
                <div class="ad-img gar"></div>
                <span>Гарантия<br> 10 лет</span>
                <div class="advantages-block-sub">
                    Гарантия 10 лет на потолки установленные нами
                </div>
            </div>
            <div class="advantages-block">
                <div class="ad-img pig"></div>
                <span>Доступная цена</span>
                <div class="advantages-block-sub">
                    Вызовите замерщика и убедитесь сами!
                </div>
            </div>
            <div class="advantages-block">
                <div class="ad-img like"></div>
                <span>Высокое качество</span>
                <div class="advantages-block-sub">
                    Только проверенные материалы и самые опытные монтажники.
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div-->
    <br>
    <div class="advntgs">
        <h2>Наши преимущества</h2>
        <div>
            <table style="margin:auto;margin-top:15px;width:100%;">
                <tbody>
                    <tr>
                        <td><div class="circle __orange" style="background-image:url('/public/web/images/icons/1.png');margin:auto;"></div></td>
                        <td><div class="circle __orange" style="background-image:url('/public/web/images/icons/3.png');margin:auto;"></div></td>
                        <td><div class="circle __orange" style="background-image:url('/public/web/images/icons/5.png');margin:auto;"></div></td>
                    </tr>
                    <tr>
                        <td class="table-text" style="padding:10px 10px 0px 10px;"><strong>Бесплатный вызов замерщика</strong><br>Сотрудник компании выезжает на дом к клиентам, консультирует по вопросам выбора материалов и производит необходимые замеры на объекте.</td>
                        <td class="table-text" style="padding:10px 10px 0px 10px;"><strong>Бесплатная сервисная поддержка <br>в течение 1 года.</strong><br>Все работы по демонтажу, ремонту и обслуживанию потолков, включая случаи затопления соседями, производятся за счет компании «MSPlus».</td>
                        <td class="table-text" style="padding:10px 10px 0px 10px;"><strong>Широкий ассортимент акций <br>и уникальных предложений.</strong><br>Бесплатная установка осветительных приборов, скидки для именинников и военнослужащих, дополнительные бонусы при оформлении заказа через интернет.</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    

    
		
    

    <!--div class="video-v2">
        <div class="video-block-v2">
                <a rel="nofollow" class="fancy-video-js mr10" href="http://www.youtube.com/v/J-OKoleIKzk&amp;fs=1&amp;autoplay=1"><img width="276" height="206" src="http://img.youtube.com/vi/J-OKoleIKzk/0.jpg">Взрыв газовых баллонов.<br> Безопасные газовые баллоны</a>
        </div>

        <div class="video-block-v2">
                <a rel="nofollow" class="fancy-video-js mr10" href="http://www.youtube.com/v/eCneHmV1reg&amp;fs=1&amp;autoplay=1"><img width="276" height="206" src="http://img.youtube.com/vi/eCneHmV1reg/0.jpg">Безопасный полимерный<br> газовый баллон в огне</a>
        </div>

        <div class="video-block-v2">
            <a rel="nofollow" class="fancy-video-js" href="http://www.youtube.com/v/xocxYUHIywk&amp;fs=1&amp;autoplay=1"><img width="276" height="206" src="http://img.youtube.com/vi/xocxYUHIywk/0.jpg">Путин поручил изменить ГОСТ<br> на газовые баллоны</a>
        </div>
        <div class="clearfix"></div>
    </div-->

    <div class="cat-v2">
        <h3>Виды натяжных потолков</h3>
        {foreach item=item from=$aCats}
            <div class="cat-v2-item">
                <a href="/works/{$item.url}" class="cat-v2-item-head">{$item.title}</a>
                <div class="cat-v2-item-left">
                    <a href="/works/{$item.url}"><img src="{$item.picUrl}" width="212px" height="149px" alt="{$item.title}"/></a>
                        <a href="/works/{$item.url}" class="cat-v2-item-btn">смотреть подробнее...</a>
                </div>
                        <span>{$item.text|strip_tags|truncate:"220"}</span>
            </div>
        {/foreach}
        <div class="clearfix"></div>
    </div>

   
    
    <br/>

    <!--form class="horizont-form send-request-form">
        <input type="hidden" name="request-source" value="middle-horizontal-form-main">
        <div class="horizont-form__heading">Заказать натяжной потолок сейчас!</div>
        <input class="horizont-form__input" type="text" name="name" placeholder="Ваше имя">
        <input class="horizont-form__input mask_phone" type="text" name="phone" placeholder="Ваш телефон">
        <input type="checkbox" name="human" class="human">
        <input type="text" name="human_t" class="human">
        <button class="promo-contacts__button horizont-form__button __submit send-data-button" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Отправить</button>
    </form-->

    <div class="horizont-form-ex">
        <div class="horizont-form-ex-left" style="font-size:34px;">
            Для того чтобы <strong>оформить заказ на натяжные потолки</strong> (Ростов-на-Дону и Ростовская область), а также задать вопросы специалистам компании «MSPlus», заполните форму обратной связи.
        </div>
        <div class="horizont-form-ex-middle"></div>
        <div class="horizont-form-ex-right">
            <form class="form-order send-request-form">
                <div class="form-order__heading">Бесплатный замер и расчет стоимости натяжных потолков</div>
                <input type="hidden" name="request-source" value="bottom-form-main">
                <!-- Row 1 -->
                <div class="form-order__row">
                    <input class="form-order__input" name="name" type="text" placeholder="Ваше имя">
                </div>
                <!-- Row 2 -->
                <div class="form-order__row">
                    <input class="form-order__input mask_phone" name="phone" type="text" placeholder="Ваш телефон">
                </div>
                <!-- Row submit -->
                <input type="checkbox" name="human" class="human">
                <input type="text" name="human_t" class="human">
                <div class="form-order__row __submit">
                    <button class="form-order__button __submit send-data-button" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Заказать</button>
                </div>
            </form>
        </div>
    </div>
    
    <br> 
    <p></p>
    <p></p>
    <p></p>
    
    <h3>Почему клиенты выбирают нас для монтажа натяжных потолков?</h3>
    <div class="text-block">
        <h5>Гарантия 10 лет</h5>
        <p class="mb5">Компания «MSPlus» -одна из немногих компаний, которая уверенна на 100% в качестве предлагаемых услуг.<br/> 
                И на это есть причины:						
        </p>					
        <ul class="mb20">
                <li>
                        мы сотрудничаем не только с проверенными производителями России, но и Европы;
                </li>
                <li>
                        в нашей компании работают только высококвалифицированные сотрудники, имеющие не один год стажа в монтаже натяжных потолков;
                </li>
                <li>
                        материалы, используемые нами при выполнении заказа, соответствуют всем нормам сертификации и стандартизации;
                </li>
        </ul>
        <p>Именно поэтому мы даем своим клиентам настоящую гарантию 10 лет на механическую прочность, устойчивость форм и красок потолков, установленных нашей компанией, при соблюдении правил и норм эксплуатации.<br>
        Более того, при правильной эксплуатации, сроки сохранения первоначального вида наших материалов достигают 50 лет.</p>

        <h5>Год обслуживания натяжных потолков бесплатно</h5>
        <p>В «MSPlus» ценят своих клиентов и всегда поддерживают в любой ситуации.<br>
        Поэтому все работы по демонтажу, ремонту и обслуживанию в течение одного года для Вас абсолютно бесплатны, при условии соблюдения норм эксплуатации.<br>
        А внезапные неприятные сюрпризы (например, затопили соседи сверху) - это наша забота.</p>

        <h5>Безопасность при монтаже</h5>
        <p>Качественные материалы - залог Вашей безопасности. Мы используем исключительно сертифицированную продукцию. Главными показателями качества полотен, которые используют профессионалы нашей компании, являются экологичность ( в составе нет аллергенов, вредных веществ и нет неприятных запахов), влагоустойчивость ( препятствуют появлению конденсата, плесени, не подвержены коррозии), наличие бактерицидной пленки, и соответствие норм противопожарной безопасности.<br>
        Каждая монтажная бригада оснащена новейшим профессиональным инструментом и взрывобезопасными полимерно-композитными газовыми баллонами Швейцарских производителей, поэтому установка натяжных потолков производится быстро и качественно.</p>
    </div>


           
    <div class="our-works-v2 clearfix">
        <h2>Натяжные потолки в Ростове-на-Дону &mdash; готовые решения</h2> 
        
        
        <div class="works-v2 clearfix">
        {foreach item=item from=$aItems}
            <div class="works-v2-block">
                <img class="works-v2-block-img" src="{$item.picUrl}" width="318px" height="200px" style="z-index:1"/>
                <div class="works-v2-block-prices" style="z-index:2">{$item.price|number_format:0:",":" "} р</div>
                <div class="works-v2-block-description">
                    <div class="works-v2-block-description-head">
                        {$item.title}
                    </div>
                    <div class="works-v2-block-description-body">
                        {$item.title}<br>
                        Площадь: {$item.square} м<sup>2</sup>
                    </div>
                </div>
                <div class="works-v2-block-button buy-project-btn">
                    <span>Вызвать замерщика</span>
                    <!--div class="works-v2-block-text-b"></div-->
                </div>
                <!--div class="works-v2-block-img-h">
                    <a href="/prices/#project-{$item.id}" class="works-v2-block-img-icon __cart buy-project-btn" 
                    data-source="mainpage-projectblock" data-message="заказ типового решения: {$item.title}"></a>
                    <a href="/prices/#project-{$item.id}" class="works-v2-block-img-icon __eye open-custom-win"
                    data-winId="projectBy" data-id="{$item.id}"></a>
                    <div class="works-v2-block-img-b"></div>
                </div-->
                <!--div class="works-v2-block-price"><span>{$item.price|number_format:0:",":" "} руб.</span></div-->
            </div>
            {/foreach}          
        </div>
  
    </div>  
    

    <div class="opinion">

    </div>

    <!-- Отзывы -->
    <div class="user-reviews">
        <h2>Отзывы наших клиентов</h2>
        <ul class="user-reviews__list bxslider">
        {foreach item=item from=$aReviews}
            <li class="user-reviews__item">
                <a href = "reviews/#{$item.id}"><img src="/data/images/reviews/{$item.id}/trimmed/{$item.cover}_mini.jpg" height="100" width="100" alt="Пользователь"></a>
                <a href = "reviews/#{$item.id}"><strong>{$item.author}</strong></a>
                <a href = "reviews/#{$item.id}"><span style = "font-size:24px;">{$item.title}<br></span></a>
                <hr>
                <a href = "reviews/#{$item.id}"><p>{$item.text|truncate:150}</p></a>
            </li>
        {/foreach}
        </ul>
    </div>			
</div>
