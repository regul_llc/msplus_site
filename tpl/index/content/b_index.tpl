<div class="wrapper">
    <div class="text-block">
        <h1>Натяжные потолки в Ростове-на-Дону</h1>
        <h4>Индивидуальный подход и лучшие цены на натяжные потолки.</h4>
        <p>Гибкий подход в ценовой политике и система скидок с индивидуальным подходом к каждому позволит воплотить любые Ваши желания в жизнь. Многочисленные положительные отзывы о натяжных потолках установленных нашими специалистами тому подтверждение.</p>
        <p>Аскетический стиль, плавные линии романизма или многоуровневые своды в стиле дворцов VII века - это вопрос Вашего стиля жизни.&nbsp;Технические моменты - это наша забота.</p>
        <p>Оставьте свой вопрос или заказ на выезд специалиста по номеру <strong>+7 (863) 226-74-48</strong> и Вы сами убедитесь, что ремонт - не такая уж большая проблема.</p>
    </div>

    <div class="our-advantages">
        <h2>Наши преимущества</h2>	
        <div class="advantages">
            <div class="advantages-block">
                <div class="ad-img pen"></div>
                <span>Бесплатный замер</span>
                <div class="advantages-block-sub">
                    Выезд замерщика к вам домой или в офис бесплатно
                </div>
            </div>
            <div class="advantages-block">
                <div class="ad-img gift"></div>
                <span>Дизайн<br> в подарок</span>
                <div class="advantages-block-sub">
                    Эксклюзивные дизайн-проекты только для Вас
                </div>
            </div>
            <div class="advantages-block">
                <div class="ad-img key"></div>
                <span>Бесплатный сервис</span>
                <div class="advantages-block-sub">
                    1 год бесплатного обслуживания Ваших потолков
                </div>
            </div>
            <div class="advantages-block">
                <div class="ad-img gar"></div>
                <span>Гарантия<br> 10 лет</span>
                <div class="advantages-block-sub">
                    Гарантия 10 лет на потолки установленные нами
                </div>
            </div>
            <div class="advantages-block">
                <div class="ad-img pig"></div>
                <span>Доступная цена</span>
                <div class="advantages-block-sub">
                    Вызовите замерщика и убедитесь сами!
                </div>
            </div>
            <div class="advantages-block">
                <div class="ad-img like"></div>
                <span>Высокое качество</span>
                <div class="advantages-block-sub">
                    Только проверенные материалы и самые опытные монтажники.
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="text-block">
        <h1>О компании</h1>
        <p>Компания «MSPlus» уже более 7 лет на российском рынке натяжных потолков в Ростове-на-Дону, и c этим именем ассоциируются качество и гарантированное выполнение своих обязательств у всех, кто с нами сотрудничает.<br><br>

        Приоритетами для руководителей «MSPlus» являются прежде всего выполнение своих обязательств перед клиентами, поддержание марки компании и постоянное профессиональное развитие всех сотрудников.<br><br>

        Если вы делаете ремонт и решили установить себе натяжные потолки, - обязательно посмотрите примеры наших работ. Мы уверены, что вы найдете в них много интересных решений для себя. Либо же просто позвоните нашим специалистам, они вас подробно проконсультируют и подскажут оптимальный для вас вариант натяжного потолка!<br><br> 

        Все, кто уже работал с нами, убедились на собственном опыте, что натяжные потолки в Ростове-на-Дону - это действительно красиво, качественно, просто и выгодно!</p>
    </div>

    <div class="our-works-v2 clearfix">
        <h2>Натяжные потолки в Ростове-на-Дону &mdash; готовые решения</h2>	
        
        <div class="works-v2 clearfix">
        {foreach item=item from=$aItems}
            <div class="works-v2-block">
                <div class="works-v2-block-header">
                    <a href="/prices/#project-{$item.id}">{$item.title}</a>
                </div>
                <img class="works-v2-block-img" src="{$item.picUrl}" width="318px" height="200px" />
                <div class="works-v2-block-text">
                    <span>Площадь:{$item.square}м<sup>2</sup></span>
                    <span>{$item.price|number_format:0:",":" "} руб.</span>
                    <div class="works-v2-block-text-b"></div>
                </div>
                <div class="works-v2-block-img-h">
                    <a href="/prices/#project-{$item.id}" class="works-v2-block-img-icon __cart buy-project-btn" 
                    data-source="mainpage-projectblock" data-message="заказ типового решения: {$item.title}"></a>
                    <a href="/prices/#project-{$item.id}" class="works-v2-block-img-icon __eye open-custom-win"
					data-winId="projectBy" data-id="{$item.id}"></a>
                    <div class="works-v2-block-img-b"></div>
                </div>
                <!--div class="works-v2-block-price"><span>{$item.price|number_format:0:",":" "} руб.</span></div-->
            </div>
        {/foreach}			
        </div>
    </div>	
    
    <br/>
		
    <h3>Почему клиенты выбирают нас для монтажа натяжных потолков?</h3>
    <div class="text-block">
        <h5>Гарантия 10 лет</h5>
        <p class="mb5">Компания «MSPlus» -одна из немногих компаний, которая уверенна на 100% в качестве предлагаемых услуг.<br/> 
                И на это есть причины:						
        </p>					
        <ul class="mb20">
                <li>
                        мы сотрудничаем не только с проверенными производителями России, но и Европы;
                </li>
                <li>
                        в нашей компании работают только высококвалифицированные сотрудники, имеющие не один год стажа в монтаже натяжных потолков;
                </li>
                <li>
                        материалы, используемые нами при выполнении заказа, соответствуют всем нормам сертификации и стандартизации;
                </li>
        </ul>
        <p>Именно поэтому мы даем своим клиентам настоящую гарантию 10 лет на механическую прочность, устойчивость форм и красок потолков, установленных нашей компанией, при соблюдении правил и норм эксплуатации.<br>
        Более того, при правильной эксплуатации, сроки сохранения первоначального вида наших материалов достигают 50 лет.</p>

        <h5>Год обслуживания натяжных потолков бесплатно</h5>
        <p>В «MSPlus» ценят своих клиентов и всегда поддерживают в любой ситуации.<br>
        Поэтому все работы по демонтажу, ремонту и обслуживанию в течение одного года для Вас абсолютно бесплатны, при условии соблюдения норм эксплуатации.<br>
        А внезапные неприятные сюрпризы (например, затопили соседи сверху) - это наша забота.</p>

        <h5>Безопасность при монтаже</h5>
        <p>Качественные материалы - залог Вашей безопасности. Мы используем исключительно сертифицированную продукцию. Главными показателями качества полотен, которые используют профессионалы нашей компании, являются экологичность ( в составе нет аллергенов, вредных веществ и нет неприятных запахов), влагоустойчивость ( препятствуют появлению конденсата, плесени, не подвержены коррозии), наличие бактерицидной пленки, и соответствие норм противопожарной безопасности.<br>
        Каждая монтажная бригада оснащена новейшим профессиональным инструментом и взрывобезопасными полимерно-композитными газовыми баллонами Швейцарских производителей.</p>
    </div>

    <div class="video-v2">
        <div class="video-block-v2">
                <a rel="nofollow" class="fancy-video-js mr10" href="http://www.youtube.com/v/J-OKoleIKzk&amp;fs=1&amp;autoplay=1"><img width="276" height="206" src="http://img.youtube.com/vi/J-OKoleIKzk/0.jpg">Взрыв газовых баллонов.<br> Безопасные газовые баллоны</a>
        </div>

        <div class="video-block-v2">
                <a rel="nofollow" class="fancy-video-js mr10" href="http://www.youtube.com/v/eCneHmV1reg&amp;fs=1&amp;autoplay=1"><img width="276" height="206" src="http://img.youtube.com/vi/eCneHmV1reg/0.jpg">Безопасный полимерный<br> газовый баллон в огне</a>
        </div>

        <div class="video-block-v2">
            <a rel="nofollow" class="fancy-video-js" href="http://www.youtube.com/v/xocxYUHIywk&amp;fs=1&amp;autoplay=1"><img width="276" height="206" src="http://img.youtube.com/vi/xocxYUHIywk/0.jpg">Путин поручил изменить ГОСТ<br> на газовые баллоны</a>
        </div>
        <div class="clearfix"></div>
    </div>

    <form class="horizont-form send-request-form">
        <input type="hidden" name="request-source" value="middle-horizontal-form-main">
        <div class="horizont-form__heading">Заказать натяжной потолок сейчас!</div>
        <input class="horizont-form__input" type="text" name="name" placeholder="Ваше имя">
        <input class="horizont-form__input mask_phone" type="text" name="phone" placeholder="Ваш телефон">
        <input type="checkbox" name="human" class="human">
        <input type="text" name="human_t" class="human">
        <button class="promo-contacts__button horizont-form__button __submit send-data-button" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Отправить</button>
    </form>

    <div class="cat-v2">
        <h3>Виды натяжных потолков</h3>
        {foreach item=item from=$aCats}
            <div class="cat-v2-item">
                <a href="/works/{$item.url}" class="cat-v2-item-head">{$item.title}</a>
                <div class="cat-v2-item-left">
                    <a href="/works/{$item.url}"><img src="{$item.picUrl}" width="212px" height="149px" /></a>
                        <a href="/works/{$item.url}" class="cat-v2-item-btn">смотреть подробнее...</a>
                </div>
                        <a href="/works/{$item.url}"><span>{$item.text|strip_tags|truncate:"220"}</span></a>
            </div>
        {/foreach}
        <div class="clearfix"></div>
    </div>

    <div class="horizont-form-ex">
        <div class="horizont-form-ex-left">
                        Если Вы хотите заказать бесплатный замер или точную стоимость натяжных потолков, заполните форму, и наш менеджер свяжется с Вами в ближайшее время
        </div>
        <div class="horizont-form-ex-middle"></div>
        <div class="horizont-form-ex-right">
            <form class="form-order send-request-form">
                <div class="form-order__heading">Бесплатный замер и расчет стоимости натяжных потолков</div>
                <input type="hidden" name="request-source" value="bottom-form-main">
                <!-- Row 1 -->
                <div class="form-order__row">
                    <input class="form-order__input" name="name" type="text" placeholder="Ваше имя">
                </div>
                <!-- Row 2 -->
                <div class="form-order__row">
                    <input class="form-order__input mask_phone" name="phone" type="text" placeholder="Ваш телефон">
                </div>
                <!-- Row submit -->
                <input type="checkbox" name="human" class="human">
                <input type="text" name="human_t" class="human">
                <div class="form-order__row __submit">
                    <button class="form-order__button __submit send-data-button" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Заказать</button>
                </div>
            </form>
        </div>
    </div>

    <div class="opinion">

    </div>

    <!-- Отзывы -->
    <div class="user-reviews">
        <h2>Отзывы наших клиентов</h2>
        <ul class="user-reviews__list bxslider">
        {foreach item=item from=$aReviews}
            <li class="user-reviews__item">
                <img src="/data/images/reviews/{$item.id}/trimmed/{$item.cover}_mini.jpg" height="100" width="100" alt="Пользователь">
                <strong>{$item.author}, {$item.age} лет</strong>
                <hr>
                <span>{$item.title}<br></span>
                <p>{$item.text|truncate:150}</p>
            </li>
        {/foreach}
        </ul>
    </div>			
</div>
