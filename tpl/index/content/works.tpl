<div class="wrapper">		
			
	<div class="text-block">
            <h1>Виды натяжных потолков</h1><br>
		<p>Натяжные потолочные конструкции изготавливаются из прочных эластичных поливинилхлоридовых пленок, которые под воздействием равномерных нагрузок образуют идеально ровные поверхности. В зависимости от типов полотен и особенностей геометрии различают следующие виды этого элемента интерьера.</p>
	</div>

	<div class="work-section-wrapper">
		<div class="work-section">
			{foreach item=item from=$aCats}
				<div class="work-section__row l_clearfix">
					<div class="work-section__column work-section__column-1">
						<div class="work-section__image-block">
							<a href="/works/{$item.url}"><img src="{$item.picUrl}" width="320px" height="210px" alt="{$item.title}" title="{$item.title}"/></a>
						</div>
					</div>
					<div class="work-section__column work-section__column-2">
                                            <a href="/works/{$item.url}" title='{$item.title}'><h4 class="to_l title_link">{$item.title}</h4></a>
                                            <div class="work-section__text-block">
						<p>{$item.previewText}</p>
                                            </div>
                                            <noindex><a href="/works/{$item.url}" class="promo-contacts__button" rel="nofollow">Подробнее</a></noindex>
					</div>
				</div>
			{/foreach}
		</div>
        </div>
        <div class="text-block text-for-works">
            <strong><a class="link buy-project-btn">Заказать натяжные потолки в Ростове-на-Дону</a> от 300р. м2 можно по телефонам +7 (863) 226-74-94 с 9 до 21:00. В стоимость уже включены монтаж и материалы. Выезд замерщика по Ростову-на-Дону производится в день обращения!</strong>
        </div>
		
</div>
