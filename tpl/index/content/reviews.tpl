<div class="wrapper">			
	<!-- Отзывы -->
	<div class="user-reviews __width-full">
		<h1>Отзывы</h1>	
		<ul class="user-reviews__list">
		<!-- Item 1 -->
		{foreach item=item from=$aReviews}
                <a name ={$item.id}></a>
		<li class="user-reviews__item">
			<div class="user-reviews__item__column __1">
				<img src="/data/images/reviews/{$item.id}/trimmed/{$item.cover}_middle.jpg" height="200" width="200" alt="Пользователь">
			</div>
			<div class="user-reviews__item__column __2">
				<strong>{$item.author}, {$item.age} лет</strong>
				<p>{$item.text}</p>
			</div>
		</li>
		{/foreach}
		</ul>
	</div>

	<div class="text-block">
		{if !empty($message)}
			<h3 id="scrollToMe" style='margin-bottom:150px;'>
				{$message}
			</h3>
		{else}	
		<h3 class="c_margin-left__xxs">Оставьте Ваш отзыв о нашей работе.</h3>
		<form action="" method="POST" enctype="multipart/form-data" class="send-review-form __style-2">
			<input type="hidden" name="action" value="add">
			<p class="send-review-form__row __1">
				<label for="tfName">Ваше Имя:</label>
				<input type="text" name="data[author]" required="required" id="tfName">

				<label for="tfAge">Ваш Возраст:</label>
				<input type="text" name="data[age]" required="required" id="tfAge">

				{*<label for="tfEmail">Ваш E-Mail:</label>
				<input type="text" name="data[email]" required="required" id="tfEmail">*}
			</p>
			<p class="send-review-form__row __2">
				<label for="tfHeading">Заголовок:</label>
				<input type="text" name="data[title]" required="required" id="tfHeading">

				<label for="tfType">Тип потолка:</label>
				<select name="data[type]">
				<option value="0"></option>
				{foreach item=item from=$aCats}
					<option value="{$item.id}">{$item.title}</option>
				{/foreach}
				</select>
			</p>
			<p class="send-review-form__row __3">
				<label for="tfText">Ваш Текст:</label>
				<textarea id="tfText" name="data[text]"  required="required" cols="30" rows="10"></textarea>
				<input type="checkbox" name="human" class="human">
				<input type="text" name="human_t" class="human">
			</p>
			<p class="send-review-form__row __submit">
				<input class="promo-contacts__button {*send-review-button*}" type="submit" value="отправить">
				<input id="load_img" type="file" name="cover" onchange="$('#fname').html($(this).val())" value="отправить" style="display: none;">
				<input class="promo-contacts__button" type="button" value="загрузить фото" onclick="$('#load_img').click(); ">
				<span id="fname" class="photo-path"></span>
			</p>
		</form>
		{/if}
	</div>
</div>