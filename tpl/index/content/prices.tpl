{*<style>
    .banner {
        padding-top:20px;
        padding-left:20px;
        background-image:url("\public\web\img\bg.jpg");
    }
    
    .banner-text {
        color: white;
    }
    
</style>*}


<div class="banner" style="background-image:url('/public/web/img/novogodnije_podarki.jpg'); background-size: 100%;"><!-- height: 648px"-->
    

        <div class="banner-header">Предновогодняя акция</div>
        
        <div class="banner-form">
            <form class="form-order send-request-form" style="float:left;">
                <div class="form-order__heading">Оставьте заявку на вызов специалиста по замерам!</div>
                <input type="hidden" name="request-source" value="slider-form">
                <!-- Row 1 -->
                <div class="form-order__row">
                        <input class="form-order__input" type="text" name="name" placeholder="Ваше имя">
                </div>
                <!-- Row 2 -->
                <div class="form-order__row">
                        <input class="form-order__input mask_phone" type="text" name="phone" placeholder="Ваш телефон">
                        <input type="checkbox" name="human" class="human">
                        <input type="text" name="human_t" class="human">
                </div>
                <!-- Row submit -->
                <div class="form-order__row __submit">
                        <button class="form-order__button __submit send-data-button" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Получить скидку</button>
                </div>
            </form>
            <div class="banner-textblock"><span style="font-size: 40px;">До Нового года успей<br>Купить потолок за 99 рублей!</span><br>При заключении договора Вы получаете не только качественный потолок по низкой цене, но и гарантированный предновогодний подарок!
            </div>
        </div>
        
        
        
            
        <!--div class="banner-money" style="background-image:url('/public/web/img/money.png');"></div-->
        
    
    
    
</div>



<div class="wrapper">		
    <div class="text-block">
	<h1>Цены на натяжные потолки в Ростове-на-Дону</h1>
        <p>Компания MSPlus занимается монтажем натяжных потолков уже более девяти лет. 
            За это время мы собрали уникальную команду профессионалов способных установить 
            для вас абсолютно любой натяжной потолок с наиболее выгодной для Вас 
            стоимостью работ. Так же у нас налажены связи со всеми основными 
            производителями натяжных потолков, что дает нам возможность предлагать 
            своим клиентам самые лучшие материалы по самым выгодным ценам.</p>
	<p>Лучший способ узнать точную стоимость Ваших потолков - вызвать 
           замерщика нашей компании. Он все измерит, посчитает, даст 
           консультацию, ответит на Ваши вопросы и посоветует оптимальные для 
           Вас варианты. И все это бесплатно!
        </p>
    </div>

    <!--h3 class="to_l">Фото и цены на натяжные потолки</h3-->   
    
    <div class="works-v2 clearfix">
        {foreach item=item from=$aItems}
            <div class="works-v2-block">
                <img class="works-v2-block-img" src="{$item.picUrl}" width="318px" height="200px" style="z-index:1" title="{$item.title}" alt="{$item.title}"/>
                <div class="works-v2-block-prices" style="z-index:2">{$item.price|number_format:0:",":" "} р</div>
                <div class="works-v2-block-description">
                    <div class="works-v2-block-description-head">
                        {$item.title}
                    </div>
                    <div class="works-v2-block-description-body">
                        {$item.description|trim|strip_tags|truncate:75}<br>
                        Площадь: {$item.square} м<sup>2</sup>
                    </div>
                </div>
                <div class="works-v2-block-button buy-project-btn">
                    <span>Вызвать замерщика</span>
                    <!--div class="works-v2-block-text-b"></div-->
                </div>
                <!--div class="works-v2-block-img-h">
                    <a href="/prices/#project-{$item.id}" class="works-v2-block-img-icon __cart buy-project-btn" 
                    data-source="mainpage-projectblock" data-message="заказ типового решения: {$item.title}"></a>
                    <a href="/prices/#project-{$item.id}" class="works-v2-block-img-icon __eye open-custom-win"
                    data-winId="projectBy" data-id="{$item.id}"></a>
                    <div class="works-v2-block-img-b"></div>
                </div-->
                <!--div class="works-v2-block-price"><span>{$item.price|number_format:0:",":" "} руб.</span></div-->
            </div>
        {/foreach}          
    </div>
        <br>
    
     <p></p>
     <p></p>
    

</div>     
     
<div class="request-block">
    <div class="request-block-content">
        <div class="request-block-content-form">
            <div class="request-block-content-form-header">Оставьте заявку и получите замер и дизайн-проект бесплатно!</div>
            <div class="request-block-content-form-body">
                <form class="form-order send-request-form noBackground noPadding">
                <table width="100%" cellpadding="10px">
                    <tr>
                        <input type="hidden" name="request-source" value="request-form_prices">
                        <!-- Row 1 -->
                        <td class="form-order__row-conteiner-1" align="right">
                            <div class="form-order__row width200">
                                    <input class="form-order__input" type="text" name="name" placeholder="Ваше имя">
                            </div>
                        </td>
                        <!-- Row 2 -->
                        <td class="form-order__row-conteiner-2" align="center">
                            <div class="form-order__row width200">
                                    <input class="form-order__input mask_phone" type="text" name="phone" placeholder="Ваш телефон">
                                    <input type="checkbox" name="human" class="human">
                                    <input type="text" name="human_t" class="human">
                            </div>
                        </td>
                        <!-- Row submit -->
                        <td class="form-order__row-conteiner-3" align="left">
                            <div class="form-order__row __submit width200">
                                    <button class="form-order__button __submit send-data-button" style="background:#f8b334;" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Отправить</button>
                            </div>
                        </td>
                    </tr>
                </table>
                </form>
            </div>
        </div>
        <div class="request-block-content-callback">
            <div class="request-block-content-callback-text">Остались вопросы? Позвоните нам!</div>
            <div class="request-block-content-callback-phone">+7 (863) 226-74-48</div>
        </div>
    </div>
</div>
    
<div class="wrapper">
    
    <br>
    
     <p></p>
     <p></p>
     
    <h4>Компания MSPlus устанавливает потолки только по официальному договору.</h4>
    <h2 style="padding-bottom:15px;color:#f8b334;">Работаем в Ростове-на-Дону и области с 2009 года.</h2>
    <h4 style="font-size:21px;">За это время мы сделали уютными многие дома. Наша цель — забота о каждом клиенте.<br>Мы бережем Ваше время, силы, деньги и делаем самые уютные потолки для Вашего дома.</h4>

    <!--br>
    
     <p></p>
     <p></p>
    
    <h3 class="to_l">Прайс лист натяжных потолков ПВХ*</h3>
    <span><i class="attn"></i>*Стоимость уже включает изготовление, монтаж и ПВХ профиль</span>  
    <table class="price-v2">
        <tr class="head-row">
            <td>Производитель</td>
            <td>Страна производства</td>
            <td>Полотна</td>
            <td>Ширина, см</td>
            <td>Цена, руб/м2</td>
        </tr>

        <tr class="item-row">
            <td>Polyplast, Bassell</td>
            <td>Бельгия</td>
            <td>Матовые/ Лак/ Сатин
                <button class="promo-contacts__button buy-project-btn" data-source="price-table" data-message="Заказ Polyplast, Bassell">Заказать</button>
            </td>
            <td>320</td>
            <td>
                <strike>от 400</strike><br>
                <b style="color:red;">от 300</b>
            </td>
        </tr>

        <tr class="item-row">
            <td>Pongs</td>
            <td>Германия</td>
            <td>Матовые/ Лак/ Сатин
                <button class="promo-contacts__button buy-project-btn" data-source="price-table" data-message="Заказ Pongs">Заказать</button></td>
            <td>до 270</td>
            <td>
                <strike>от 420</strike><br>
                <b style="color:red;">от 380</b>
            </td>
        </tr>

        <tr class="item-row">
            <td>Renolit</td>
            <td>Германия</td>
            <td>Матовые/ Лак/ Сатин
                <button class="promo-contacts__button buy-project-btn" data-source="price-table" data-message="Заказ Renolit">Заказать</button>
            </td>
            <td>200</td>
            <td>420</td>
        </tr>

        <tr class="item-row">
            <td>Malpensa</td>
            <td>Италия</td>
            <td>Матовые/ Лак/ Сатин
                <button class="promo-contacts__button buy-project-btn" data-source="price-table" data-message="Заказ Malpensa">Заказать</button>
            </td>
            <td>200</td>
            <td>550</td>
        </tr>

        <tr class="item-row">
            <td>CTN</td>
            <td>Франция</td>
            <td>Матовые/ Лак/ Сатин
                <button class="promo-contacts__button buy-project-btn" data-source="price-table" data-message="Заказ CTN">Заказать</button>
            </td>
            <td>200</td>
            <td>550</td>
        </tr>

        <tr class="item-row">
            <td>Gain Dragon (качество Pongs)</td>
            <td>КНР</td>
            <td>Матовые/ Лак/ Сатин (белые)
                <button class="promo-contacts__button buy-project-btn" data-source="price-table" data-message="Заказ Gain Dragon">Заказать</button>
            </td>
            <td>500</td>
            <td>420</td>
        </tr>
    </table-->
    

    <div class="howToOrder">
        <div class="howToOrder-header"><h2>Как заказать потолок</h2></div>
        <div class="howToOrder-row1">
            <table>
                <tbody>
                    <tr>
                        <td><div class="circle __orange" style="background-image:url('/public/web/images/icons/1.png');"></div></td>
                        <td><div class="arrow" style="background-image:url('/public/web/images/icons/arrow.png');"></div></td>
                        <td><div class="circle __orange" style="background-image:url('/public/web/images/icons/2.png');"></div></td>
                        <td><div class="arrow" style="background-image:url('/public/web/images/icons/arrow.png');"></div></td>
                        <td><div class="circle __orange" style="background-image:url('/public/web/images/icons/3.png');"></div></td>
                        <td><div class="arrow" style="background-image:url('/public/web/images/icons/arrow.png');"></div></td>
                        <td><div class="circle __orange" style="background-image:url('/public/web/images/icons/4.png');"></div></td>
                    </tr>
                    <tr>
                        <td class="table-text">Замер помещения <br>и расчет стоимости</td>
                        <td></td>
                        <td class="table-text">Подписание договора</td>
                        <td></td>
                        <td class="table-text">Монтаж потолка</td>
                        <td></td>
                        <td class="table-text">Вы наслаждаетесь новым потолком!</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="howToOrder-row2">
            <h4 style="font-size:21px;"><span style="color:#f8b334">Замерщик будет у Вас в этот же день, если заявка будет сделана до 17:00 часов!</span>
                <br>Наш специалист быстро рассчитает стоимость и подготовит договор
                <br>(Вам не обязательно выезжать из дома для подписания).
            </h4>
        </div>
        <div class="howToOrder-row3">
            <h4 style="font-size:21px;"><span style="color:#f8b334">Монтаж возможен уже на следующий день после подписания договора.</span>
                <br>Натяжные потолки у нас — быстро и удобно!
            </h4>
        </div>
    </div>

    <!--div class="someArticle">
        <div class="someArticle-pic"><img src="/public/web/img/pic.png" width="100%"></div>
        <div class="someArticle-text">Значимость этих проблем настолько очевидна, что укрепление и развитие структуры 
            представляет собой интересный эксперимент проверки позиций, занимаемых участниками в отношении поставленных 
            задач. Повседневная практика показывает, что консультация с широким активом в значительной степени обуславливает 
            создание системы обучения кадров, соответствует насущным потребностям. Задача организации, в особенности же новая 
            модель организационной деятельности требуют определения и уточнения модели развития. Не следует, однако забывать, 
            что консультация с широким активом требуют от нас анализа модели развития. Повседневная практика показывает, что 
            консультация с широким активом играет важную роль в формировании дальнейших направлений развития.</div>
    </div-->
    
    <!--div class="wsi ">
        <h2>Почему стоит купить натяжной потолок у нас?</h2>
        <div class="row clearfix">
            <span><i class="trash"></i>Бесплатный вывоз мусора</span>
            <span><i class="gar"></i>Гарантия 10 лет</span>
            <span><i class="key"></i>Бесплатный сервис</span>
        </div>
        <h2>В стоимость включено</h2>
        <div class="row clearfix">
            <span><i class="cov"></i>Изготовление полотна</span>
            <span><i class="drel"></i>Монтажные работы</span>
            <span><i class="prof"></i>Профиль для монтажа</span>
        </div>
    </div-->
    <!--form class="horizont-form send-request-form">
        <input type="hidden" name="request-source" value="price-horizontal-form-top">
        <div class="horizont-form__heading">Оставьте заявку и получите консультацию с замером бесплатно!</div>
        <input class="horizont-form__input" type="text" name="name" placeholder="Ваше имя">
        <input class="horizont-form__input mask_phone" type="text" name="phone" placeholder="Ваш телефон">
        <input type="checkbox" name="human" class="human">
        <input type="text" name="human_t" class="human">				
        <button class="promo-contacts__button horizont-form__button __submit" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Отправить</button>
    </form-->
    			

    <div class="user-reviews">
        <h2>Отзывы наших клиентов</h2>
        <ul class="user-reviews__list bxslider">
        {foreach item=item from=$aReviews}
            <li class="user-reviews__item">
                <a href = "reviews/#{$item.id}"><img src="/data/images/reviews/{$item.id}/trimmed/{$item.cover}_mini.jpg" height="100" width="100" alt="Пользователь"></a>
                <a href = "reviews/#{$item.id}"><strong>{$item.author}</strong></a>
                <a href = "reviews/#{$item.id}"><span style = "font-size:24px;">{$item.type}<br></span></a>
                <hr>
                <a href = "reviews/#{$item.id}"><p>{$item.text|truncate:150}</p></a>
            </li>
        {/foreach}
        </ul>
    </div>
    
    <!--div class="price-act">
        <a href="/actions">Скидки и акции на установку натяжных потолков  >>><div class="price-act-pic"></div></a>
    </div-->

    <!--form class="horizont-form send-request-form">		
        <div class="horizont-form__heading">Оставьте заявку и получите консультацию с замером бесплатно!</div>
        <input type="hidden" name="request-source" value="price-horizontal-form-bottom">
        <input class="horizont-form__input" type="text" name="name" placeholder="Ваше имя">
        <input class="horizont-form__input mask_phone" type="text" name="phone" placeholder="Ваш телефон">
        <input type="checkbox" name="human" class="human">
        <input type="text" name="human_t" class="human">
        <button class="promo-contacts__button horizont-form__button __submit" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Отправить</button>
    </form-->
</div>
   
<div class="banner-prices-bottom" style="background-image:url('/public/web/img/dvuhurovnevyj_natjazhnoj_potolok.jpg'); background-size: 100%;">
    <div class='banner-prices-bottom-content'>
        <table>
            <tbody>
                <tr>
                    <td>
                        <div class='banner-prices-bottom-text'>
                            Если Вы хотите заказать бесплатный замер или узнать точную стоимость натяжных потолков, заполните форму, и наш менеджер свяжется с Вами в ближайшее время
                        </div>
                    </td>
                    <td>
                        <div class='banner-prices-bottom-form'>
                            <form class="form-order send-request-form biggerSize">
                                <div class="form-order__heading"><h4>Бесплатный замер и расчет стоимости натяжных потолков</h4></div>
                                <input type="hidden" name="request-source" value="slider-form">
                                <!-- Row 1 -->
                                <div class="form-order__row">
                                        <input class="form-order__input" type="text" name="name" placeholder="Ваше имя">
                                </div>
                                <!-- Row 2 -->
                                <div class="form-order__row">
                                        <input class="form-order__input mask_phone" type="text" name="phone" placeholder="Ваш телефон">
                                        <input type="checkbox" name="human" class="human">
                                        <input type="text" name="human_t" class="human">
                                </div>
                                <!-- Row submit -->
                                <div class="form-order__row __submit">
                                        <button class="form-order__button __submit send-data-button" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Заказать</button>
                                </div>
                            </form>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
        
        
    <script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=hfe2ovCPmh5WCP9DYc8k0L97qcnv9cobF1drVWMTVKeSk8i91zilG2HWKFkLfjafnY8Dz0oqAQADfURGGuRnyu7taBWRjY2bXuyZZEsorOpavG5ULYVhqQ8k2ITqPcj1Vp/56hM5mcq50aonivHvf9rUze0Deb0gy8COw0r**vY-';</script>
    <script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=xRw2Sy3XhGBrr28BAh70hJgrD6PNmCn//*iBZ3oEqNzskhrPGfJtGwIugcfAZcHbFItPNBg/yJQjcRZQtHq1xHzrcq8n7WlTIq26NwLG7On9E*FpX2Er7YEa3g5Blq3Wu*EeLyZNfkssellFPvDkrzSzk7X7oodAyMg0xXuaLx4-';</script>
