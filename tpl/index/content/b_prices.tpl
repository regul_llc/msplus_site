<div class="wrapper">		
    <div class="text-block">
	<h1>Цены на натяжные потолки в Ростове-на-Дону</h1>
        <p>Компания MSPlus занимается монтажем натяжных потолков уже более семи лет. 
            За это время мы собрали уникальную команду профессилов способных установить 
            для вас абсолютно любой натяжной потолок с наиболее выгодной для Вас 
            стоимостью работ. Так же у нас налажены связи со всеми основными 
            производителями натяжных потолков, что дает нам возможность предлагать 
            своим клиентам самые лучшеи материалы по самым выгодным ценам.</p>
	<p>Лучший способ узнать точную стоимость Ваших потолков - вызвать 
           замерщика нашей компании. Он все измерит, посчитает, даст 
           консультацию, ответит на Ваши вопросы и посоветует оптимальные для 
           Вас варианты. И все это бесплатно!
        </p>
    </div>

    <h3 class="to_l">Примеры готовых решений</h3>	
    
    <div class="works-v2 clearfix">
        {foreach item=item from=$aItems}
            <div class="works-v2-block">
                <div class="works-v2-block-header">
                    <a href="/prices/#project-{$item.id}">{$item.title}</a>
                </div>
                <img class="works-v2-block-img" src="{$item.picUrl}" width="318px" height="200px" />
                <div class="works-v2-block-text">
                    <span>Площадь:{$item.square}м<sup>2</sup></span>
                    <span>{$item.price|number_format:0:",":" "} руб.</span>
                    <div class="works-v2-block-text-b"></div>
                </div>
                <div class="works-v2-block-img-h">
                    <a href="/prices/#project-{$item.id}" class="works-v2-block-img-icon __cart buy-project-btn" 
                    data-source="mainpage-projectblock" data-message="заказ типового решения: {$item.title}"></a>
                    <a href="/prices/#project-{$item.id}" class="works-v2-block-img-icon __eye open-custom-win"
					data-winId="projectBy" data-id="{$item.id}"></a>
                    <div class="works-v2-block-img-b"></div>
                </div>
                <!--div class="works-v2-block-price"><span>{$item.price|number_format:0:",":" "} руб.</span></div-->
            </div>
        {/foreach}			
    </div>
    
    <div class="wsi ">
        <h2>Почему стоит заказать натяжной потолок у нас?</h2>
        <div class="row clearfix">
            <span><i class="trash"></i>Бесплатный вывоз мусора</span>
            <span><i class="gar"></i>Гарантия 10 лет</span>
            <span><i class="key"></i>Бесплатный сервис</span>
        </div>
        <h2>В цену включено</h2>
        <div class="row clearfix">
            <span><i class="cov"></i>Изготовление полотна</span>
            <span><i class="drel"></i>Монтажные работы</span>
            <span><i class="prof"></i>Профиль для монтажа</span>
        </div>
    </div>
    <!--form class="horizont-form send-request-form">
        <input type="hidden" name="request-source" value="price-horizontal-form-top">
        <div class="horizont-form__heading">Оставьте заявку и получите консультацию с замером бесплатно!</div>
        <input class="horizont-form__input" type="text" name="name" placeholder="Ваше имя">
        <input class="horizont-form__input mask_phone" type="text" name="phone" placeholder="Ваш телефон">
        <input type="checkbox" name="human" class="human">
        <input type="text" name="human_t" class="human">				
        <button class="promo-contacts__button horizont-form__button __submit" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Отправить</button>
    </form-->
		

    <h3 class="to_l">Прайс лист натяжных потолков ПВХ*</h3>
    <span><i class="attn"></i>*Цена уже включает изготовление, монтаж и ПВХ профиль</span>	
    <table class="price-v2">
        <tr class="head-row">
            <td>Производитель</td>
            <td>Страна производства</td>
            <td>Полотна</td>
            <td>Ширина, см</td>
            <td>Цена, руб.</td>
        </tr>

        <tr class="item-row">
            <td>Polyplast, Bassell</td>
            <td>Бельгия</td>
            <td>Матовые/ Лак/ Сатин
                <button class="promo-contacts__button buy-project-btn" data-source="price-table" data-message="Заказ Polyplast, Bassell">Заказать</button>
            </td>
            <td>320</td>
            <td>
                <strike>от 400</strike><br>
                <b style="color:red;">от 330</b>
            </td>
        </tr>

        <tr class="item-row">
            <td>Pongs</td>
            <td>Германия</td>
            <td>Матовые/ Лак/ Сатин
                <button class="promo-contacts__button buy-project-btn" data-source="price-table" data-message="Заказ Pongs">Заказать</button></td>
            <td>до 270</td>
            <td>
                <strike>от 420</strike><br>
                <b style="color:red;">от 380</b>
            </td>
        </tr>

        <tr class="item-row">
            <td>Renolit</td>
            <td>Германия</td>
            <td>Матовые/ Лак/ Сатин
                <button class="promo-contacts__button buy-project-btn" data-source="price-table" data-message="Заказ Renolit">Заказать</button>
            </td>
            <td>200</td>
            <td>420</td>
        </tr>

        <tr class="item-row">
            <td>Malpensa</td>
            <td>Италия</td>
            <td>Матовые/ Лак/ Сатин
                <button class="promo-contacts__button buy-project-btn" data-source="price-table" data-message="Заказ Malpensa">Заказать</button>
            </td>
            <td>200</td>
            <td>550</td>
        </tr>

        <tr class="item-row">
            <td>CTN</td>
            <td>Франция</td>
            <td>Матовые/ Лак/ Сатин
                <button class="promo-contacts__button buy-project-btn" data-source="price-table" data-message="Заказ CTN">Заказать</button>
            </td>
            <td>200</td>
            <td>550</td>
        </tr>

        <tr class="item-row">
            <td>Gain Dragon (качество Pongs)</td>
            <td>КНР</td>
            <td>Матовые/ Лак/ Сатин (белые)
                <button class="promo-contacts__button buy-project-btn" data-source="price-table" data-message="Заказ Gain Dragon">Заказать</button>
            </td>
            <td>500</td>
            <td>420</td>
        </tr>
    </table>
    
    <div class="price-act">
        <a href="/actions">Скидки и акции на установку натяжных потолков  >>><div class="price-act-pic"></div></a>
    </div>
    
    <!--form class="horizont-form send-request-form">		
        <div class="horizont-form__heading">Оставьте заявку и получите консультацию с замером бесплатно!</div>
        <input type="hidden" name="request-source" value="price-horizontal-form-bottom">
        <input class="horizont-form__input" type="text" name="name" placeholder="Ваше имя">
        <input class="horizont-form__input mask_phone" type="text" name="phone" placeholder="Ваш телефон">
        <input type="checkbox" name="human" class="human">
        <input type="text" name="human_t" class="human">
        <button class="promo-contacts__button horizont-form__button __submit" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Отправить</button>
    </form-->
</div>