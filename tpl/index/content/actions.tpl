<div class="wrapper">           
    <div class="arct-main">
        <h1>Скидки и акции на натяжные потолки в Ростове-на-Дону</h1>                
        <p class="mb5">В ближайшее время компания MSPlus планирует запуск нескольких очень выгодных для Вас акций.</p>
        <p>Оставьте свои контактные данные, чтобы быть в курсе и мы проинформируем Вас о новых акциях и уникальных предложениях!</p>
        <div class="act-item">
            <p>Предновогодняя акция!<br>
                <span>До Нового года успей<br>купить потолок за 99 рублей!</span> <br>
                
            </p>
            <button class="promo-contacts__button  __submit buy-project-btn" type="submit" data-source="actionpage-action-item" data-message="заказ по акции: Потолок за 99 рублей">Заказать</button>
            <div class="im-h"></div>
            <img src="/public/web/images/actions/podarki.jpg" alt="Акция натяжной потолок за 99 рублей Ростов-на-Дону" title="Акция натяжной потолок за 99 рублей Ростов-на-Дону"/>
        </div>
        
        <div class="act-item" style="">
            <p style="margin-bottom:7px;">Акция "Светильники в подарок!"<br>
                <span>При заказе потолков эконом-класса Вы получаете каждый третий светильник в подарок. При заказе пленки "люкс" — каждый второй.</span> </p>
            <!--p style="font-size:18px;">*В стоимость уже включены монтаж, полотно и расходные материалы.</p-->
            <button class="promo-contacts__button  __submit buy-project-btn" type="submit" data-source="actionpage-action-item" data-message="заказ по акции: Светильники в подарок">Заказать</button>
            <div class="im-h"></div>
            <img src="/public/web/images/actions/akcia-svetilniki.jpg" alt="Акция Светильники в подарок натяжные потолки Ростов-на-Дону" title="Акция Светильники в подарок натяжные потолки Ростов-на-Дону"/>
        </div>
              
        <div class="act-item">
            <p>Установка люстры бесплатно.<br>
                <span>При заказе монтажа натяжных потолков в нашей компании - специалисты установят люстру бесплатно.</span> </p>
            <button class="promo-contacts__button  __submit buy-project-btn" type="submit" data-source="actionpage-action-item" data-message="заказ по акции: Люстра">Заказать</button>
            <div class="im-h"></div>
            <img src="/public/web/images/actions/act-lst.png" alt="Акция на заказ натяжных потолков Ростов-на-Дону" title="Акция на заказ натяжных потолков Ростов-на-Дону"/>
        </div>
        
        

        <div class="act-item">
            <p>Акция WEB-заказ<br>
                <span>Заполните форму на вызов специалиста и получите <b>дополнительную скидку 5%!</b></span> </p>
            <button class="promo-contacts__button  __submit buy-project-btn" type="submit" data-source="actionpage-action-item" data-message="заказ по акции: Вебзаказ">Заказать</button>
            <div class="im-h"></div>
            <img src="/public/web/images/actions/act-web1.jpg" alt="Акция web-заказ натяжные потолки" title="Акция web-заказ натяжные потолки"/>
        </div>
        
        <div class="act-item">
            <p>Вы заслуживаете больше, поэтому у нас платите меньше!<br>
            <span>Военнослужащим 7% скидки на все виды работ.</span> </p>
            <button class="promo-contacts__button  __submit buy-project-btn" type="submit" data-source="actionpage-action-item" data-message="заказ по акции: Скидка для военнослужащих">Заказать</button>
            <div class="im-h"></div>
            <img src="/public/web/images/actions/act-voin.jpg" alt="Скидки военнослужащим при заказе натяжных потолков в Ростове-на-Дону" title="Скидки военнослужащим при заказе натяжных потолков в Ростове-на-Дону"/>
        </div>

        <div class="act-item">
            <p>Как на Ваши именины Вам натянем потолок!<br>
            <span>Всем именинникам скидка 5% в подарок.</span> </p>
            <p class="psub">* вы можете получить скидку при вызове замерщика 7 дней до и после дня рождения при предъявлении паспорта.</p>
            <button class="promo-contacts__button  __submit buy-project-btn" type="submit" data-source="actionpage-action-item" data-message="заказ по акции: Для именниников">Заказать</button>
            <div class="im-h"></div>
            <img src="/public/web/images/actions/act-dr.jpg" alt="Подарим скидку при заказе натяжных потолков в Ростове-на-Дону" title="Подарим скидку при заказе натяжных потолков в Ростове-на-Дону"/>
        </div>

        <form class="horizont-form send-request-form">
            <input type="hidden" name="request-source" value="actions-horizontal-form-top">
            <input type="hidden" name="request-message" value="Подписка на информирование об акциях">
            <div class="horizont-form__heading">Подписаться на информирование о новых акциях и скидках на натяжные потолки!</div>
            <input class="horizont-form__input" type="text" name="name" placeholder="Ваше имя">
            <input class="horizont-form__input" type="text" required="required" name="phone" placeholder="Ваш телефон или email">
            <input type="checkbox" name="human" class="human">
            <input type="text" name="human_t" class="human">
            <button class="promo-contacts__button horizont-form__button __submit" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Отправить</button>						
        </form>
    </div>
</div>