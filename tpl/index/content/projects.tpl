<!-- Houses -->
 <div class="houses">
	 <div class="container">
		 <div class="commback">&lArr; <a href="/">На главную</a></div>
		 <!-- Houses top -->
		 <div class="houses__top">
			 <!-- Houses items -->
			 <div class="houses-items clearfix">
				{foreach from=$aCats item=item}
					<!-- Houses item 1 -->
					<div class="houses-item {if $item.id eq $nCatId}__is-active{/if}">
						<a href="/projects/{$item.id}">
							<div class="houses-item__header">
								<!-- Label -->
								<div class="projects-item-label">
									<span class="projects-item-label__text">{$item.title|lower}</span>
									<div class="projects-item-label__small-arrow"></div>
								</div>
							</div>
							<div class="houses-item__body">
								<img class="project_img" src="/data/images/projects/{$item.cover_pid}/trimmed/{$item.cover}_mini.jpg" height="127" width="196" alt="Дом">

								<div class="projects-overlay hidden">
									<div class="projects-overlay__content">
										<div class="projects-overlay__content-item">
											<i class="icon-eye"></i>
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>
				{/foreach}
				
			 </div>

			 <!-- Houses info -->
			 <div id="ProjectsBlocks" class="houses-info">
				{foreach from=$aProjects item=item}
					<div class="houses-info__item">
						<div class="houses-info__item__heading">№ {$item.id} {$item.title}</div>

						<div class="houses-info__item__row clearfix">
							<!-- Column 1 -->
							<div class="projimages houses-info__item__column __1" data-pid="{$item.id}">
								<!-- Images. Ajax append -->						
							</div>
							<!-- Column 2 -->
							<div class="houses-info__item__column __2">
								<!-- Details -->
								<div class="houses-info__details">
									<div class="houses-info__details__heading">Описание проекта:</div>
									{$item.description}
								</div>
								<!-- Price -->
								<div class="houses__price">
									<div class="houses__price__heading">Стоимость проекта</div>
									<div class="houses__price__number">{$item.price|number_format:0:".":" "} р.</div>
									<div class="houses__price__button-wrapper">
										<button data-pid="{$item.id}" data-price="{$item.price}" class="button-2 houses__price__button buy-project-btn">Купить проект</button>
									</div>
								</div>
							</div>
						</div>
					</div> 
				{/foreach}
			 </div>
		 </div>
		 <!-- Houses bottom -->
		 <div class="houses__bottom"></div>
	 </div>
 </div>
