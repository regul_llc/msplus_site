<div class="column1">
	<div class="links">
		<a class="order js-no-action" href="">
			<img src="/public/img/link-order.jpg" alt="" width="67" height="69">
			<p>On-line<br><span>заявка</span></p>
		</a>
		<a class="calculator js-no-action" href="">
			<img src="/public/img/link-calc.jpg" alt="" width="67" height="69">						
			<p>On-line<br><span>калькулятор</span></p>
		</a>
	</div>
	<div class="portfolio">
		<h2>Примеры работ</h2>
		<div class="pagination-up disabled">
			<a class="js-no-action" href="#"><img src="/public/img/pagination-up.jpg" alt="" width="42" height="12"></a>
		</div>
		<div class="work-container">
			<div class="work-moving-container">
				{foreach from=$aLastWorks item=item}
					<div class="work">
                                                <a class="left-side-link" href="/works/{if ($item.url_name) }{$item.url_name}{else}{$item.mainwork}{/if}">{$item.title}</a>
						<img src="/public/img/upload/thumbnail/{$item.cover}" alt="" width="219" height="146">
						<p class="text">{$item.text|strip_tags:false|truncate:150:'..'}</p>
						<p class="link"><a href="/works/{if ($item.url_name) }{$item.url_name}{else}{$item.mainwork}{/if}" rel="nofollow">далее...</a></p>
					</div>
				{/foreach}
			</div>
		</div>
		<div class="pagination-down">
			<a class="js-no-action" href="#"><img src="/public/img/pagination-down.png" alt="" width="42" height="12"></a>
		</div>
	</div>
</div>