<div class="row1">
			<div class="wrapper">				
				<nav class="menu">
					<ul>
						<!-- Item 0 - logo -->
						<li class="menu__item">
							<div class="logo">
								<a href="/" title="Натяжные потолки в Ростове-на-Дону"><img src="/public/web/img/mslogo.jpg" height="79" width="248"></a>
							</div>
						</li>
						<!-- Item 1 -->
						<li class="{if $aPageParams.pageType=="articles"}active {/if}menu__item">{if $aPageParams.pageType=="articles"}<span>О потолках</span>{else}<a href="/articles" title="Статьи о натяжных потолках">О потолках</a>{/if}</li>
						<!-- Item 2 -->
						<li class="{if $aPageParams.pageType=="prices"}active {/if}menu__item">{if $aPageParams.pageType=="prices"}<span>Цены</span>{else}<a href="/prices"  title="Цены на натяжные потолки в Ростове-на-Дону">Цены</a>{/if}</li>
						<!-- Item 3 -->
						<li class="{if $aPageParams.pageType=="works"}active {/if}menu__item">{if $aPageParams.pageType=="works"}<span>Виды потолков</span>{else}<a href="/works"  title="Виды натяжных потолков">Виды потолков</a>{/if}
                                                    {*<ul class="sub-menu">
								<li><a href="/works/glyancevie_natyajnie_potolki">Глянцевые потолки</a></li>
								<li><a href="/works/matovie_natyajnie_potolki">Матовые потолки</a></li>
								<li><a href="/works/photoprint_natyajnie_potolki">Фотопечать</a></li>
                                                                <li><a href="/works/satinovie_natyajnie_potolki">Cатиновые потолки</a></li>
								<li><a href="/works/mnogourovnevie_natyajnie_potolki">Многоуровневые потолки</a></li>
								<li><a href="/works/podsvetka_natyajnih_potolkov">Декоративная подсветка</a></li>
                                                    </ul>*}
                                                </li>
						<!-- Item 4 -->
						<li class="{if $aPageParams.pageType=="reviews"}active {/if}menu__item">{if $aPageParams.pageType=="reviews"}<span>Отзывы</span>{else}<a href="/reviews"  title="Отзывы">Отзывы</a>{/if}</li>
						<!-- Item 5 -->
						<li class="{if $aPageParams.pageType=="actions"}active {/if}menu__item">{if $aPageParams.pageType=="actions"}<span>Акции</span>{else}<a href="/actions" title="Скидки и акции на натяжные потолки в Ростове-на-Дону">Акции</a>{/if}</li>
						<!-- Item 6 -->
						<li class="{if $aPageParams.pageType=="contacts"}active {/if}menu__item">{if $aPageParams.pageType=="contacts"}<span>Контакты</span>{else}<a href="/contacts" title="Контакты">Контакты</a>{/if}</li>
					</ul>
				</nav>
			</div>
		</div>

{if empty($aPageParams.NoBigHeader) || !$aPageParams.NoBigHeader}
		{include file="default/top_main.tpl"}
	{/if}
<div class="row2">
			<!-- Promo -->
			<div class="prom o clearfix">
				<div class="wrapper">
					<div class="promo__right">
						<div class="promo-social">
							<!-- Link 1 -->
							{*<a class="promo-social__link" href="#">
								<i class="promo-social__icon __f"></i>
							</a>
							<!-- Link 2 -->
							<a class="promo-social__link" href="#">
								<i class="promo-social__icon __ok"></i>
							</a>*}
							<!-- Link 3 -->
							<a class="promo-social__link" href="https://twitter.com/msplus61" target="_blank">
								<i class="promo-social__icon __tw"></i>
							</a>
							<!-- Link 4 -->
							<a class="promo-social__link" href="https://vk.com/natjazhnye_potolki_rostov" target="_blank">
								<i class="promo-social__icon __vk"></i>
							</a>
						</div>
					</div>
					<div class="promo__left">
						<!-- Promo contacts -->
						<div class="promo-contacts">
							<div class="promo-contacts__right">
								<button class="promo-contacts__button buy-project-btn" data-addition="top-button">Вызвать замерщика</button>
							</div>
							<div class="promo-contacts__left">
								<div class="promo-contacts__item __phone">+7 (863) 226-74-48</div>
								<div class="promo-contacts__item">Ежедневно с 09:00 - 21:00</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
