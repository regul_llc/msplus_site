<div class="row3">
    <!-- Slider -->
    <div class="promo-slider">
        <div class="tp-banner-container">
                <div class="tp-banner" >
                        <ul>
                            <li class="tp-banner__slide __2" data-transition="turnoff-vertical" data-slotamount="7" data-masterspeed="700"  data-saveperformance="on"  data-title="Intro Slide">
                                        <!-- MAIN IMAGE -->
                                        <img src="/public/web/img/dummy.png"  alt="slidebg2" data-lazyload="/public/web/photos/slider-revolution/potolok-so-svetilnikami.jpg" title="Акции на натяжные потолоки" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" width="1920px" height="600px">

                                        <!-- LAYER NR. 1 -->
                                        <div class="tp-caption lfr skewtoleftshort customout rs-parallaxlevel-0"
                                                data-x="140"
                                                data-y="159" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-speed="700"
                                                data-start="850"
                                                data-easing="Power3.easeInOut"
                                                data-elementdelay="0.1"
                                                data-endelementdelay="0.1"
                                                style="z-index: 2;">
                                                        <div class="tp-banner__layer-1">Акция "Светильники в подарок!"</div>
                                        </div>

                                        <!-- LAYER NR. 2 -->
                                        <div class="tp-caption lfb customout rs-parallaxlevel-0"
                                                data-x="651"
                                                data-y="282" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-speed="700"
                                                data-start="1600"
                                                data-easing="Power3.easeInOut"
                                                data-elementdelay="0.1"
                                                data-endelementdelay="0.1"
                                                style="z-index: 3;">
                                                        <!-- Form -->
                                                        <form class="form-order send-request-form">
                                                                <div class="form-order__heading">Оставьте заявку на вызов специалиста по замерам!</div>
                                                                <input type="hidden" name="request-source" value="slider-form">
                                                                <!-- Row 1 -->
                                                                <div class="form-order__row">
                                                                        <input class="form-order__input" type="text" name="name" placeholder="Ваше имя">
                                                                </div>
                                                                <!-- Row 2 -->
                                                                <div class="form-order__row">
                                                                        <input class="form-order__input mask_phone" type="text" name="phone" placeholder="Ваш телефон">
                                                                        <input type="checkbox" name="human" class="human">
                                                                        <input type="text" name="human_t" class="human">
                                                                </div>
                                                                <!-- Row submit -->
                                                                <div class="form-order__row __submit">
                                                                        <button class="form-order__button __submit send-data-button" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Получить скидку</button>
                                                                </div>
                                                        </form>
                                        </div>

                                        <!-- LAYER NR. 3 -->
                                        <div class="tp-caption lft customout rs-parallaxlevel-0" 
                                                data-x="140"
                                                data-y="282" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-speed="700"
                                                data-start="2200"
                                                data-easing="Power3.easeInOut"
                                                data-elementdelay="0.1"
                                                data-endelementdelay="0.1"
                                                style="z-index: 4;">

                                                        <div class="tp-banner__layer-3">
                                                                <p style="font-size:32px; text-align: left;">Дарим нашим клиентам дополнительные бонусы! При заказе потолков эконом-класса Вы получаете каждый третий светильник в подарок. При заказе пленки "люкс" — каждый второй.
                                                                </p>
                                                                <!--p style="color: #3a3a38; font-size: 50px; text-align: left;"> <b>300р</b>  <span style="margin-top:20px; font-size:30px; text-align: left;"> за кв.м.</span></p>
                                                                <p style="font-size:18px; text-align: left;">*В стоимость уже включены монтаж, полотно и расходные материалы.</p-->
                                                        </div>
                                        </div>

                                        <!-- LAYER NR. 4 -->
                                        <div class="tp-caption lfb customout rs-parallaxlevel-0"
                                                data-x="-60"
                                                data-y="190"
                                                
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.1;scaleY:0.1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-speed="700"
                                                data-start="1800"
                                                data-easing="Power3.easeInOut"
                                                data-elementdelay="0.1"
                                                data-endelementdelay="0.1"
                                                style="z-index: 5;">

                                                        <!--img src="/public/web/photos/slider-revolution/spring.png" style="height:392px;width:337px;" alt="Акции на натяжные потолоки"-->
                                        </div><!--data-y="267" -->
                            </li>
                            
                                <li class="tp-banner__slide __1" data-transition="turnoff-vertical" data-slotamount="7" data-masterspeed="500"   data-saveperformance="on"  data-title="Intro Slide">
                                        <!-- MAIN IMAGE -->
                                        <img src="/public/web/img/dummy.png"  alt="Бесплатный замер при заказе натяжных потолков" data-lazyload="/public/web/photos/slider-revolution/natjazhnoj_potolok_s_podsvetkoj.jpg" title="Бесплатный замер при заказе натяжных потолков" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" width="100%" height="600">

                                        <!-- LAYER NR. 1 -->
                                        <div class="tp-caption lfl customout rs-parallaxlevel-0"
                                                data-x="-20"
                                                data-y="54" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-speed="400"
                                                data-start="850"
                                                data-easing="Power3.easeInOut"
                                                data-elementdelay="0.1"
                                                data-endelementdelay="0.1"
                                                style="z-index: 2;">
                                                        <div class="tp-banner__layer-1">Бесплатный замер и консультация с выездом</div>
                                        </div>

                                        <!-- LAYER NR. 2 -->
                                        <div class="tp-caption lfl customout rs-parallaxlevel-0"
                                                data-x="-20"
                                                data-y="205" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-speed="700"
                                                data-start="1500"
                                                data-easing="Power3.easeInOut"
                                                data-elementdelay="0.1"
                                                data-endelementdelay="0.1"
                                                style="z-index: 3;">
                                                        <!-- Form -->
                                                        <form class="form-order send-request-form">
                                                                <div class="form-order__heading">Оставьте заявку на бесплатный выезд  замерщика и консультацию</div>
                                                                <input type="hidden" name="request-source" value="slider-form">
                                                                <!-- Row 1 -->
                                                                <div class="form-order__row">
                                                                        <input class="form-order__input" type="text" name="name" placeholder="Ваше имя">
                                                                </div>
                                                                <!-- Row 2 -->
                                                                <div class="form-order__row">
                                                                        <input class="form-order__input mask_phone" type="text" name="phone" placeholder="Ваш телефон">
                                                                        <input type="checkbox" name="human" class="human">
                                                                        <input type="text" name="human_t" class="human">
                                                                </div>
                                                                <!-- Row submit -->
                                                                <div class="form-order__row __submit">
                                                                        <button class="form-order__button __submit send-data-button" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Вызвать замерщика</button>
                                                                </div>
                                                        </form>
                                        </div>

                                        <!-- LAYER NR. 3 -->
                                        <div class="tp-caption  light_heavy_70_shadowed lfr randomrotateout tp-resizeme "
                                                data-x="368"
                                                data-y="205" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-easing="Power4.easeOut"
                                                data-speed="600"
                                                data-start="1800"
                                                data-elementdelay="0.05"
                                                data-endelementdelay="0.05"
                                                data-endspeed="300"
                                                data-endeasing="Power1.easeOut"
                                                style="z-index: 4;">

                                                        <div class="tp-banner__layer-3">
                                                                <p>Если Вы хотите получить бесплатный замер и узнать точную стоимость натяжных потолков для Вас, заполните форму, и наш менеджер свяжется с Вами в ближайшее время</p>
                                                        </div>
                                        </div>

                                        <!-- LAYER NR. 4 -->
                                        <div class="tp-caption sfr customout rs-parallaxlevel-0"
                                                data-x="688"
                                                data-y="0" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-speed="500"
                                                data-start="1200"
                                                data-easing="Power1.easeInOut"
                                                data-elementdelay="0.2"
                                                data-endelementdelay="0.1"
                                                style="z-index: 5;">

                                                        <img src="/public/web/photos/slider-revolution/man.png"  alt="Бесплатный замер при заказе натяжных потолков">
                                        </div>
                                </li>

                                

                                <li class="tp-banner__slide __3" data-transition="turnoff-vertical" data-slotamount="7" data-masterspeed="700"   data-saveperformance="on"  data-title="Intro Slide">
                                        <!-- MAIN IMAGE -->
                                        <img src="/public/web/img/dummy.png"  alt="slidebg3" data-lazyload="/public/web/photos/slider-revolution/novogodnije_podarki.jpg" title="Предновогодняя акция" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" width="1920%" height="600">

                                        <!-- LAYER NR. 1 -->
                                        <div class="tp-caption lfr skewtoleftshort customout rs-parallaxlevel-0"
                                                data-x="-20"
                                                data-y="159" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-speed="700"
                                                data-start="850"
                                                data-easing="Power3.easeInOut"
                                                data-elementdelay="0.1"
                                                data-endelementdelay="0.1"
                                                style="z-index: 2;">
                                                        <div class="tp-banner__layer-1">Предновогодняя акция!</div>
                                        </div>

                                        <!-- LAYER NR. 3 -->
                                        <div class="tp-caption lft customout rs-parallaxlevel-0"
                                                data-x="-20"
                                                data-y="299" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-speed="700"
                                                data-start="1500"
                                                data-easing="Power3.easeInOut"
                                                data-elementdelay="0.1"
                                                data-endelementdelay="0.1"
                                                style="z-index: 4;">

                                                        <div class="tp-banner__layer-3">
                                                                <!--p>
                                                                        В ассортименте натяжных потолочных конструкций достойное место занимают замшевые натяжные потолки, которые изготавливаются из пленки ПВХ замшевой фактуры. Рельефная поверхность базового покрытия напоминает натуральный материал – замш. 
                                                                        Обычно замшевые потолки применяется в сочетании с другими фактурами и в сложных многоуровневых конструкциях для создания эффекта глубины.
                                                                </p-->
                                                                <p>
                                                                    <span style="font-size: 34px">До Нового года успей<br>Закажи потолок за 99 рублей!<br></span>
                                                                    При заключении договора Вы получаете не только качественный потолок по низкой цене, но и гарантированный предновогодний подарок!
                                                                </p>
                                                        </div>
                                        </div>
                                        
                                          <!-- LAYER NR. 4 -->
                                        <div class="tp-caption lfl customout rs-parallaxlevel-0"
                                                data-x="-20"
                                                data-y="485" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-speed="700"
                                                data-start="2400"
                                                data-easing="Power3.easeInOut"
                                                data-elementdelay="0.1"
                                                data-endelementdelay="0.1"
                                                style="z-index: 99;">
                                                        <!-- Form -->

                                                        <form class="horizont-form send-request-form">
                                                        <input type="hidden" name="request-source" value="new-year-promo">
                                                        <!-- Row 1 -->
                                                                <input class="horizont-form__input" type="text" name="name" placeholder="Ваше имя">
                                                        <!-- Row 2 -->
                                                                <input class="horizont-form__input" type="text" name="phone" placeholder="Ваш телефон">
                                                                <input type="checkbox" name="human" class="human">
                                                                <input type="text" name="human_t" class="human">
                                                        <!-- Row submit -->
                                                                <button class="promo-contacts__button horizont-form__button __submit" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Получить подарок</button>
                                                        </form>
                                        </div>

                                </li>

                                <li class="tp-banner__slide __4" data-transition="turnoff-vertical" data-slotamount="7" data-masterspeed="500"   data-saveperformance="on"  data-title="Intro Slide">
                                        <!-- MAIN IMAGE -->
                                        <img src="/public/web/img/dummy.png"  alt="slidebg1" data-lazyload="/public/web/photos/slider-revolution/mnogourovnevyj_natjazhnoj_potolok.jpg" title="Многоуровневые натяжные потолки" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" width="100%" height="600">

                                        <!-- LAYER NR. 1 -->
                                        <div class="tp-caption lfl customout rs-parallaxlevel-0"
                                                data-x="350"
                                                data-y="224" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-speed="400"
                                                data-start="850"
                                                data-easing="Power3.easeInOut"
                                                data-elementdelay="0.1"
                                                data-endelementdelay="0.1"
                                                style="z-index: 2;">
                                                        <div class="tp-banner__layer-1">Бесплатный сервис 12 месяцев</div>
                                        </div>

                                        <!-- LAYER NR. 2 -->
                                        <div class="tp-caption lfl customout rs-parallaxlevel-0"
                                                data-x="-20"
                                                data-y="485" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-speed="700"
                                                data-start="1500"
                                                data-easing="Power3.easeInOut"
                                                data-elementdelay="0.1"
                                                data-endelementdelay="0.1"
                                                style="z-index: 99;">
                                                        <!-- Form -->

                                                        <form class="horizont-form send-request-form">
                                                        <input type="hidden" name="request-source" value="slider-service-form">
                                                        <!-- Row 1 -->
                                                                <input class="horizont-form__input" type="text" name="name" placeholder="Ваше имя">
                                                        <!-- Row 2 -->
                                                                <input class="horizont-form__input" type="text" name="phone" placeholder="Ваш телефон">
                                                                <input type="checkbox" name="human" class="human">
                                                                <input type="text" name="human_t" class="human">
                                                        <!-- Row submit -->
                                                                <button class="promo-contacts__button horizont-form__button __submit" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Получить консультацию</button>
                                                        </form>
                                        </div>

                                        <!-- LAYER NR. 3 -->
                                        <div class="tp-caption  light_heavy_70_shadowed lfr randomrotateout tp-resizeme "
                                                data-x="348"
                                                data-y="305" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-easing="Power4.easeOut"
                                                data-speed="600"
                                                data-start="1800"
                                                data-elementdelay="0.05"
                                                data-endelementdelay="0.05"
                                                data-endspeed="300"
                                                data-endeasing="Power1.easeOut"
                                                style="z-index: 4;">

                                                        <div class="tp-banner__layer-3">
                                                                <p>После установки потолка Вы получаете бесплатное сервисное обслуживание на 1 год по ремонтным работам, монтажу, демонтажу и даже в том случае, если Вас затопили</p>
                                                        </div>
                                        </div>

                                        <!-- LAYER NR. 4 -->
                                        <div class="tp-caption sfr customout rs-parallaxlevel-0"
                                                data-x="0"
                                                data-y="0" 
                                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                data-speed="500"
                                                data-start="1200"
                                                data-easing="Power1.easeInOut"
                                                data-elementdelay="0.2"
                                                data-endelementdelay="0.1"
                                                style="z-index: 5;">

                                                        <img src="/public/web/photos/slider-revolution/manserv.png"  alt="">
                                        </div>
                                </li>



                                <!-- <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-thumb="homeslider_thumb1.jpg"  data-saveperformance="on"  data-title="Intro Slide">								
                                        <img src="img/dummy.png"  alt="slidebg1" data-lazyload="images/slidebg1.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" width="100%" height="200">
                                </li>
                                <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-thumb="homeslider_thumb1.jpg"  data-saveperformance="on"  data-title="Intro Slide">
                                        <img src="img/dummy.png"  alt="slidebg2" data-lazyload="images/slidebg2.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" width="100%" height="200">
                                </li> -->
                        </ul>
                </div>
        </div>
    </div>
</div>
		