<div class="wrapper">
    <div class="footer-left">
        <noindex>
            <a href="/articles" class="footer-link" rel=”nofollow”>О потолках</a>
            <a href="/prices" class="footer-link" rel=”nofollow”>Цены</a>
            <a href="/works" class="footer-link" rel=”nofollow”>Работы</a>
            <a href="/actions" class="footer-link" rel=”nofollow”>Акции</a>
            <a href="/contacts" class="footer-link" rel=”nofollow”>Контакты</a>
        </noindex>
    </div>

    <div class="footer-midlle">

            <form class="form-order send-request-form">

                    <div class="form-order__heading">Заказать бесплатный замер</div>
                    <input type="hidden" name="request-source" value="footer-form">
            <!-- Row 1 -->
                    <div class="form-order__row">
                                    <input class="form-order__input" name="name" type="text" placeholder="Ваше имя">
                    </div>
            <!-- Row 2 -->
                    <div class="form-order__row">
                            <input class="form-order__input mask_phone" name="phone" type="text" placeholder="Ваш телефон">
                            <input type="checkbox" name="human" class="human">
                            <input type="text" name="human_t" class="human">
                    </div>
            <!-- Row submit -->
                    <div class="form-order__row __submit">
                            <button class="promo-contacts__button send-data-button" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" type="submit">Вызвать замерщика</button>
                    </div>
                    <span>Специалист приедет к Вам домой или в офис, произведет бесплатный замер и проконсультирует по всем интересующим Вас вопросам, поможет подобрать материал для потолка</span>
            </form>

    </div>

    <div class="footer-right">
        <div class="contacts">
                <p>+7(863) 226 74 48</p>
                <p>Ежедневно с 09:00 - 21:00</p>
        </div>

        <div class="promo-social">
                        <!-- Link 1 -->
                        {*<a class="promo-social__link" href="#">
                                <i class="promo-social__icon __f"></i>
                        </a>
                        <!-- Link 2 -->
                        <a class="promo-social__link" href="#">
                                <i class="promo-social__icon __ok"></i>
                        </a>*}
                        <!-- Link 3 -->
                        <a class="promo-social__link" href="https://twitter.com/msplus61" target="_blank">
                                <i class="promo-social__icon __tw"></i>
                        </a>
                        <!-- Link 4 -->
                        <a class="promo-social__link" href="https://vk.com/natjazhnye_potolki_rostov" target="_blank">
                                <i class="promo-social__icon __vk"></i>
                        </a>
        </div>

        <div class="copyright">
            <p>Copyright © 2012-2016 MSPLUS - <a href="/" class="main-link" title="Натяжные потолки в Ростове-на-Дону">Натяжные потолки Ростов-на-Дону</a></p>
            <p>создание сайта - Веб-студия Регул</p>
        </div>
    </div>
</div>
