<!DOCTYPE html >
<!--[if IE 7 ]>    <html lang="ru" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="ru" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="ru" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="ru" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<script type="text/javascript">
        var scale = (1 / window.devicePixelRatio).toFixed(2);
        scale = (scale >= 0.75) ? scale : 0.75;
        document.write('<meta name="viewport" id="viewport" content="width=1100, user-scalable=yes">'); //width=device-width,initial-scale='+ scale +'
	</script>
	<meta name="description" content="{$strDescription|default:'Натяжные потолки Ростове-на-Дону. Компания MSPlus производит монтаж и установку натяжных потолков в Ростове-на-Дону и Роствоской области. Натяжные потолки от MSPlus - это всегда лучшие цены, 10-ти летняя гарнтия, индивидуальный подход и качественный сервис.'}">
	<meta name="author" content="">

	<title>{$strPageTitle|default:'MSPlus - Натяжные потолки в Ростове-на-Дону. Гарантия на 10 лет! Бесплатная консультация и замер!'}</title>
	<link rel="stylesheet" href="/public/web/css/css_all.css?v=16.12.2017">
	
	<meta name="geo.placename" content="Ростов-на-Дону, Ростовская обл., Россия" />
        <meta name="geo.position" content="47.2357140;39.7015050" />
        <meta name="geo.region" content="RU-Ростовская область" />
        <meta name="ICBM" content="47.2357140, 39.7015050" />
    
	
    

	
</head>