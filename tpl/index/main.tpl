{include file="default/header.tpl"}
<body>
    
        {literal}
        <script>
           (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
           (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
           m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
           })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

           ga('create', 'UA-82327226-1', 'auto');
           ga('send', 'pageview');

        </script>
        {/literal}
    
	{include file="default/top.tpl"}
	
	<div id="content">
		{include file="content/$ContentTemplate.tpl"}
	</div>
	<footer>
		{include file="default/footer.tpl"}
	</footer>
	{include file="popups/all.tpl"}
	
	{include file="default/footer_system.tpl"}	

</body>
</html>