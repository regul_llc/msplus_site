<div class="row">
	<div class="col-md-8 col-md-offset-1">
		<section class="widget">
			<div class="body">
				<form id="fileupload" class='fileupload' action="/ajax/uploadPhotos?id={$nId}&type={$strType}"  data-geturl="/ajax/getPhotos?id={$nId}&type={$strType}" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="id" value="{$nId}">
					<input type="hidden" name="type" value="{$strType}">
					<div class="row">
						<div class="col-md-12">
							<div id="dropzone"  class="dropzone">
								Сюда можно перетащить файлы для загрузки
								<i class="fa fa-download-alt pull-right"></i>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 fileupload-progress fade">
							<!-- The global progress bar -->
							<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
								<div class="bar" style="width:0%;"></div>
							</div>
							<!-- The extended global progress information -->
							<div class="progress-extended">&nbsp;</div>
						</div>
					</div>
					<div class="form-actions fileupload-buttonbar no-margin">
						<span class="btn btn-sm btn-default fileinput-button">
								<i class="fa fa-plus"></i>
								<span>Добавить файлы...</span>
								<input type="file" name="files[]" multiple>
							</span>
						<button type="submit" class="btn btn-primary btn-sm start">
							<i class="fa fa-upload"></i>
							<span>Начать загрузку</span>
						</button>
						<button type="reset" class="btn btn-inverse btn-sm cancel">
							<i class="fa fa-ban"></i>
							<span>Отменить загрузку</span>
						</button>
					</div>
					<div class="fileupload-loading"><i class="fa fa-spin fa-spinner"></i></div>
					<!-- The table listing the files available for upload/download -->
					<table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
				</form>
			</div>
		</section>
	</div>
</div>

{literal}
<script id="template-upload" type="text/template">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        {% if (file.error) { %}
        <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
        <td>
            <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                <div class="bar" style="width:0%;"></div>
            </div>
        </td>
        <td>{% if (!o.options.autoUpload) { %}
            <button class="btn btn-primary btn-sm start">
                <i class="fa fa-upload"></i>
                <span>Загрузить</span>
            </button>
            {% } %}</td>
        {% } else { %}
        <td colspan="2"></td>
        {% } %}
        <td>{% if (!i) { %}
            <button class="btn btn-warning btn-sm cancel">
                <i class="fa fa-ban"></i>
                <span>Отменить</span>
            </button>
            {% } %}</td>
    </tr>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/template">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade" data-id="{%=file.id%}">
        {% if (file.error) { %}
        <td></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else { %}
        <td class="preview">{% if (file.thumbnail_url) { %}
            <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
            {% } %}</td>
        <td class="name">
            <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
        </td>        
		<td class="size">
			<div class="form-group hidden"><label><input type="radio" name="is_main" class="minimal setmainimg" {% if (file.is_main){ %} checked {% } %}/> Главная</label></div>
		</td>
        <td colspan="2"></td>
        {% } %}
        <td>
            <button class="btn btn-danger btn-sm delete" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
            <i class="fa fa-trash"></i>
            <span>Удалить</span>
            </button>
        </td>
    </tr>
    {% } %}
</script>
{/literal}

<script src="/public/admin/plugins/jQueryUI/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="/public/admin/fileupload/http_blueimp.github.io_JavaScript-Templates_js_tmpl.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="/public/admin/fileupload/http_blueimp.github.io_JavaScript-Load-Image_js_load-image.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="/public/admin/fileupload/http_blueimp.github.io_JavaScript-Canvas-to-Blob_js_canvas-to-blob.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/public/admin/fileupload/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/public/admin/fileupload/jquery.fileupload.js"></script>
<!-- The File Upload file processing plugin -->
<script src="/public/admin/fileupload/jquery.fileupload-fp.js"></script>
<!-- The File Upload user interface plugin -->
<script src="/public/admin/fileupload/jquery.fileupload-ui.js"></script>
<!-- files upload specific -->
<script src="/public/admin/fileupload/fileupload.js"></script>