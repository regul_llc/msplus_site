{strip}
    <h1>{$aData.title}</h1>
    <div class="pop-work-sl">
        <ul class="bxslider">
            {foreach item=item from=$aImages}
                <li>
                    <img src="/data/images/projects/{$item.item_id}/trimmed/{$item.id}_big.jpg" height="346" width="445" alt="Пример работы">
                </li>
            {/foreach}
        </ul>
    </div>
    <script type="text/javascript">
     $(function(){
      $('.bxslider').bxSlider();
     });
    </script>
	
    <div class="works-pop-cont">
        <p><span><b>Площадь</b></span><span><b>{$aData.square}м<sup>2</sup></b></span></p>
        <p class="pop-tex">{$aData.description}</p>
        <!--p><span>-Углы 6 шт. </span><span>0 р.</span></p>
        <p><span>-Профиль стен. 8 м.п</span><span>0 р.</span></p>
        <p><span>-Лента маск. 8 м.п.</span><span>0 р.</span></p>
        <p><span>-Отверстие под свет. 3 шт.</span><span>0 р.</span></p>
        <p><span>-Закладная под свет. 3 шт</span><span>0 р.</span></p>
        <p><span>-Монтажные работы</span><span>0 р.</span></p><br-->
        <p><span>Итого</span><span>{$aData.price} р.</span></p>
        
    </div>
    <form class="send-request-form">
        <input type="hidden" name="request-source" value="popup-form">
        <input type="hidden" name="request-type" value="">
        <input type="hidden" name="request-message" value="{$aData.description} {$aData.price} р.">
        <p><input required id="name_str" name="name" type="text" value="" placeholder="Ваше имя"></p>
        <p><input required class="mask_phone" name="phone" type="text" value="" placeholder="Ваш телефон"></p>
        <input type="checkbox" name="human" class="human">
        <input type="text" name="human_t" class="human">
        <p class="submit">
            <input class="promo-contacts__button send-data-button" type="submit" onclick="yaCounter24606239.reachGoal('ORDER'); return true;" value="Перезвонить мне">
        </p>
    </form>
{/strip}