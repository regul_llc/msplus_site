<div class="login-box">
  <div class="login-logo">
	<a href="{$MainUrl}/"><b>Admin</b>Big Plans</a>
  </div><!-- /.login-logo -->
  <div class="login-box-body">
	<p class="login-box-msg">{$strMess|default:"Авторизуйтесь для начала работ"}</p>
	<form action="" method="post">
	  <div class="form-group has-feedback">
		<input type="text" class="form-control" name="login" placeholder="Логин"/>
		<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	  </div>
	  <div class="form-group has-feedback">
		<input type="password" class="form-control" name="password" placeholder="Пароль"/>
		<span class="glyphicon glyphicon-lock form-control-feedback"></span>
	  </div>
	  <div class="row">
		<div class="col-xs-8">    
		  <div class="checkbox icheck">
			{*<label>
			  <input type="checkbox"> Запомнить меня
			</label>*}
		  </div>                        
		</div><!-- /.col -->
		<div class="col-xs-4">
			<button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
		</div><!-- /.col -->
	  </div>
	</form>
  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->