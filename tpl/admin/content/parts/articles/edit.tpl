<form role="form" class="ajaxform" method="post" data-listurl="{$MainUrl}/articles">
    <input type="hidden" name="project_id" value="{$aRow.id}"/>
	<input type="hidden" name="data_type" value="articles"/>
	<div class="box-body">
		<div class="form-group">
		  <label for="InputTitle">Заголовок статьи</label>
		  <input type="text" name="title" value="{$aRow.title}" required="required" class="form-control" id="InputTitle" placeholder="Введите заголовок статьи">
		</div>	 
		<div class="form-group">
			<label>
				<input type="checkbox" name="status" value="1" {if $aRow.status eq 1}checked{/if}> Опубливан (показывается на сайте)
			</label>
		</div>
		<div class="form-group">
		  <label for="InputTitle">Текст статьи <small></small></label>
		  <textarea class="textarea default-wisiwyg" name="text" placeholder="Введите текст отзыва">{$aRow.text}</textarea>
		</div>
	</div><!-- /.box-body -->
	<div class="box-footer">
		<button type="submit" class="btn btn-primary">Сохранить</button>
	</div>
</form>
					
{include file="components/fileUpload.tpl" nId=$aRow.id strType='articles'} 