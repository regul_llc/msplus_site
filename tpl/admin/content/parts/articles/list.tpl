<div class="box-body">
<table id="example1" class="table table-bordered table-hover table-striped">
  <thead>
	<tr>
		<th>Номер</th>
		<th>Заголовок</th>
		<th>Текст</th>
                <th>Статус</th>
                <th></th>
	</tr>
  </thead>
  <tbody>
  {foreach from=$aList item=item}
	<tr>
	  <td>{$item.id}</td>
	  <td><a href="/admin/articles/{$item.id}">{$item.title}</a></a></td>
	  <td>{$item.text|strip_tags|truncate:"100"}</td>
          <td>{$item.status}</td>
          <td><button class="btn btn-block btn-danger btn-delete-articles" data-id="{$item.id}">Удалить</button></td>
	</tr>                     
  {/foreach}
  </tbody>
  
</table>
</div><!-- /.box-body -->