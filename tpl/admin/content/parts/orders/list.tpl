<div class="box-body">
<table id="example1" class="table table-bordered table-hover table-striped">
  <thead>
	<tr>
		<th>Номер заявки</th>
		<th>Номер проекта</th>
		<th>Имя клиента</th>
		<th>Телефон клиента</th>	
		<th>Email клиента</th>
		<th>Дата создания</th>
		<th>Статус</th>
	</tr>
  </thead>
  <tbody>
  {foreach from=$aList item=item}
	<tr>
	  <td>{$item.id}</td>
	  <td>{$item.project_id}</td>
	  <td>{$item.cname}</a></td>
	  <td>{$item.cphone}</td>
	  <td>{$item.cemail}</td>
	  <td>{$item.date_add|date_format:"%d.%m.%Y %H:%M"}</td>
	  <td>{$item.status}</td>
	</tr>                     
  {/foreach}
  </tbody>
  {if $aList|@count > 9}
  <tfoot>
	<tr>
		<th>Номер заявки</th>
		<th>Номер проекта</th>
		<th>Имя клиента</th>
		<th>Телефон клиента</th>	
		<th>Email клиента</th>
		<th>Дата создания</th>
		<th>Статус</th>
	</tr>
  </tfoot>
  {/if}
</table>
</div><!-- /.box-body -->