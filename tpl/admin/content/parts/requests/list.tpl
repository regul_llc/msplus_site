<div class="box-body">
<table id="example1" class="table table-bordered table-hover table-striped">
  <thead>
	<tr>
		<th>Номер</th>
		<th>Имя клиента</th>
		<th>Телефон клиента</th>						
		<th>Дата создания</th>
		<th>ip</th>
		<th>бот</th>
		<th>сообщение</th>
		<th>форма</th>
		<th>Статус</th>
		<th>Действия</th>
	</tr>
  </thead>
  <tbody>
  {foreach from=$aList item=item}
	<tr>
	  <td>{$item.id}</td>
	  <td>{$item.cname}</a></td>
	  <td>{$item.cphone}</td>
	  <td>{$item.date_add|date_format:"%d.%m.%Y %H:%M"}</td>
	  <td>{$item.ip}</td>
	  <td>{if $item.is_bot}Да{else}Нет{/if}</td>
	  <td>{$item.message}</td>
	  <td>{$item.source}</td>
	  <td>
		  {if $item.status eq 1}Обработано
		  {else}Новая{/if}
	  </td>
	  <td>
			{if $item.status eq 1}<button class="btn btn-small btn-info btn-update-request" data-id="{$item.id}" data="status=0">Не обработано</button>
			{else}<button class="btn btn-small btn-success btn-update-request" data-id="{$item.id}" data="status=1">Обработано</button>{/if}
			
			{if $item.is_bot}<button class="btn btn-small btn-info btn-update-request" data-id="{$item.id}" data="isbot=0">Это не бот</button>
			{else}<button class="btn btn-small btn-warning btn-update-request" data-id="{$item.id}" data="isbot=1">Это бот</button>{/if}
			
			<button class="btn btn-small btn-danger btn-delete-request" data-id="{$item.id}">Удалить</button> 
	  </td>
	</tr>                     
  {/foreach}
  </tbody>
  {if $aList|@count > 9}
  <tfoot>
	<tr>
		<th>Номер</th>
		<th>Имя клиента</th>
		<th>Телефон клиента</th>						
		<th>Дата создания</th>
		<th>ip</th>
		<th>бот</th>
		<th>сообщение</th>
		<th>форма</th>
		<th>Статус</th>
		<th>Действия</th>
	</tr>
  </tfoot>
  {/if}
</table>
</div><!-- /.box-body -->