<form role="form" class="ajaxform" method="post" data-listurl="{$MainUrl}/projects/{$aProject.cat_id}">
	<input type="hidden" name="project_id" value="{$aProject.id}"/>
	<div class="box-body">
		<div class="form-group">
		  <label for="InputTitle">Название проекта</label>
		  <input type="text" name="title" value="{$aProject.title}" required="required" class="form-control" id="InputTitle" placeholder="Введите название проекта">
		</div>	 
		<div class="input-group col-lg-4 form-group">
			<span class="input-group-addon">Цена</span>
			<input type="text" class="form-control" name="price" value="{$aProject.price}">
			<span class="input-group-addon"> руб.</span>
		</div>		
		<div class="form-group">
			<label>
				<input type="checkbox" name="status" value="1" {if $aProject.status eq 1}checked{/if}> Опубливан (показывается на сайте)
			</label>
		</div>
		<div class="form-group">
		  <label for="InputTitle">Описание проекта <small>выводится в списке проектов вместе с фотографиями</small></label>
		  <textarea class="textarea default-wisiwyg" name="description" placeholder="Введите крутое описание проекта">{$aProject.description}</textarea>
		</div>	 
	</div><!-- /.box-body -->
	<div class="box-footer">
		<button type="submit" class="btn btn-primary">Сохранить</button>
	</div>
</form>
			
<div class="row">
	<div class="col-md-8 col-md-offset-1">
		<section class="widget">
			<div class="body">
				<form id="fileupload" data-cat="" data-pid="{$aProject.id}" action="" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="cat_id" value="{$aProject.cat_id}">
					<input type="hidden" name="project_id" value="{$aProject.id}">
					<div class="row">
						<div class="col-md-12">
							<div id="dropzone"  class="dropzone">
								Сюда можно перетащить файлы для загрузки
								<i class="fa fa-download-alt pull-right"></i>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 fileupload-progress fade">
							<!-- The global progress bar -->
							<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
								<div class="bar" style="width:0%;"></div>
							</div>
							<!-- The extended global progress information -->
							<div class="progress-extended">&nbsp;</div>
						</div>
					</div>
					<div class="form-actions fileupload-buttonbar no-margin">
						<span class="btn btn-sm btn-default fileinput-button">
								<i class="fa fa-plus"></i>
								<span>Добавить файлы...</span>
								<input type="file" name="files[]" multiple>
							</span>
						<button type="submit" class="btn btn-primary btn-sm start">
							<i class="fa fa-upload"></i>
							<span>Начать загрузку</span>
						</button>
						<button type="reset" class="btn btn-inverse btn-sm cancel">
							<i class="fa fa-ban"></i>
							<span>Отменить загрузку</span>
						</button>
					</div>
					<div class="fileupload-loading"><i class="fa fa-spin fa-spinner"></i></div>
					<!-- The table listing the files available for upload/download -->
					<table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
				</form>
			</div>
		</section>
	</div>
</div>
		

{*	
	    <!-- jQuery 2.1.3 -->
    <script src="/public/admin/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- jQuery UI 1.11.2 -->
    <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
*}
{*
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
*}


{literal}
<script id="template-upload" type="text/template">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        {% if (file.error) { %}
        <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
        <td>
            <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                <div class="bar" style="width:0%;"></div>
            </div>
        </td>
        <td>{% if (!o.options.autoUpload) { %}
            <button class="btn btn-primary btn-sm start">
                <i class="fa fa-upload"></i>
                <span>Загрузить</span>
            </button>
            {% } %}</td>
        {% } else { %}
        <td colspan="2"></td>
        {% } %}
        <td>{% if (!i) { %}
            <button class="btn btn-warning btn-sm cancel">
                <i class="fa fa-ban"></i>
                <span>Отменить</span>
            </button>
            {% } %}</td>
    </tr>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/template">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade" data-id="{%=file.id%}">
        {% if (file.error) { %}
        <td></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else { %}
        <td class="preview">{% if (file.thumbnail_url) { %}
            <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
            {% } %}</td>
        <td class="name">
            <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
        </td>        
		<td class="size">
			<div class="form-group hidden"><label><input type="radio" name="is_main" class="minimal setmainimg" {% if (file.is_main){ %} checked {% } %}/> Главная</label></div>
		</td>
        <td colspan="2"></td>
        {% } %}
        <td>
            <button class="btn btn-danger btn-sm delete" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
            <i class="fa fa-trash"></i>
            <span>Удалить</span>
            </button>
        </td>
    </tr>
    {% } %}
</script>
{/literal}
{*//<td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>*}