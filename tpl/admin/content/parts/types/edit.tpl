<form role="form" class="ajaxform" method="post" data-listurl="{$MainUrl}/types/">
	<input type="hidden" name="project_id" value="{$typeData.id}"/>
        <input type="hidden" name="data_type" value="type"/>
	<div class="box-body">
		<div class="form-group">
		  <label for="InputTitle">Название проекта</label>
		  <input type="text" name="title" value="{$typeData.title}" required="required" class="form-control" id="InputTitle" placeholder="Введите название проекта">
		</div>
		<div class="form-group">
			<label>
				<input type="checkbox" name="status" value="1" {if $typeData.status eq 1}checked{/if}> Опубливан (показывается на сайте)
			</label>
		</div>
		<div class="form-group">
		  <label for="InputTitle">Описание проекта <small>выводится в списке проектов вместе с фотографиями</small></label>
		  <textarea class="textarea default-wisiwyg" name="description" placeholder="Введите крутое описание проекта">{$typeData.text}</textarea>
		</div>	 
	</div><!-- /.box-body -->
	<div class="box-footer">
		<button type="submit" class="btn btn-primary">Сохранить</button>
	</div>
</form>
{include file="components/fileUpload.tpl" nId=$typeData.id strType='type'} 