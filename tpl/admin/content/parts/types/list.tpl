<div class="box-body">
<table id="example1" class="table table-bordered table-hover table-striped">
  <thead>
	<tr>
		<th>Номер позиции</th>
		<th>Название</th>
		<th>Описание</th>
		<th>Статус</th>
	</tr>
  </thead>
  <tbody>
  {foreach from=$aList item=item}
	<tr>
	  <td>{$item.id}</td>
	  <td><a href="/admin/types/{$item.id}">{$item.title}</a></td>
	  <td>{$item.text|strip_tags|truncate:"100"}</td>
      <td>{$item.status}</td>
	</tr>                     
  {/foreach}
  </tbody>
  {if $aList|@count > 9}
  <tfoot>
	<tr>
		<th>Номер позиции</th>
		<th>Название</th>
		<th>Описание</th>
		<th>Статус</th>
	</tr>
  </tfoot>
  {/if}
</table>
</div><!-- /.box-body -->