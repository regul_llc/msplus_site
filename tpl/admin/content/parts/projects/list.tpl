<div class="box-body">
<table id="example1" class="table table-bordered table-hover table-striped">
  <thead>
	<tr>
	  <th>№</th>
	  <th>Название проекта</th>	  
	  <th>Категория</th>
	   <th>Статус</th>
	  <th>&nbsp;</th>
	</tr>
  </thead>
  <tbody>
  {foreach from=$aList item=item}
	<tr>
	  <td>{$item.id}</td>
	  <td><a href="{$MainUrl}/projects/{$item.id}">{$item.title}</a></td>	  	  
	  <td>{$item.cat_id}</td>
	  <td>{$item.status}</td>
	  <td><button class="btn btn-block btn-danger btn-delete-project" data-id="{$item.id}">Удалить</button></td>
	</tr>                     
  {/foreach}
  </tbody>
  {if $aList|@count > 9}
  <tfoot>
	<tr>
	  <th>№</th>
	  <th>Название проекта</th>	  
	  <th>Категория</th>
	  <th>Статус</th>
	  <th>&nbsp;</th>
	</tr>
  </tfoot>
  {/if}
</table>
</div><!-- /.box-body -->