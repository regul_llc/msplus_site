<form role="form" class="ajaxform" method="post" data-listurl="{$MainUrl}/projects/">
	<input type="hidden" name="project_id" value="{$aProject.id}"/>
        <input type="hidden" name="data_type" value="project"/>
	<div class="box-body">
		<div class="form-group">
		  <label for="InputTitle">Название проекта</label>
		  <input type="text" name="title" value="{$aProject.title}" required="required" class="form-control" id="InputTitle" placeholder="Введите название проекта">
		</div>	 
		<div class="form-group col-lg-4">
			<label for="InputTitle">Категория проекта <small></small></label>
			<select class="types" name="cat_id">
				{foreach item=item from=$aProject.cats}
					<option value="{$item.id}" {if $item.id == $aProject.cat_id}selected{/if}>{$item.title}</option>
				{/foreach}
			</select>
		</div>
		<div class="input-group col-lg-4 form-group">
			<span class="input-group-addon">Цена</span>
			<input type="text" class="form-control" name="price" value="{$aProject.price}">
			<span class="input-group-addon"> руб.</span>
		</div>
                
                <div class="input-group col-lg-4 form-group">
			<span class="input-group-addon">Площадь</span>
			<input type="text" class="form-control" name="square" value="{$aProject.square}">
			<span class="input-group-addon"> руб.</span>
		</div>
                
		<div class="form-group">
			<label>
				<input type="checkbox" name="status" value="1" {if $aProject.status eq 1}checked{/if}> Опубливан (показывается на сайте)
			</label>
		</div>
		<div class="form-group">
		  <label for="InputTitle">Описание проекта <small>выводится в списке проектов вместе с фотографиями</small></label>
		  <textarea class="textarea default-wisiwyg" name="description" placeholder="Введите крутое описание проекта">{$aProject.description}</textarea>
		</div>                
	</div><!-- /.box-body -->
	<div class="box-footer">
		<button type="submit" class="btn btn-primary">Сохранить</button>
	</div>
</form>
		  
{include file="components/fileUpload.tpl" nId=$aProject.id strType='project'}