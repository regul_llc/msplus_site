<form role="form" class="ajaxform" method="post" data-listurl="{$MainUrl}/reviews">
	<input type="hidden" name="project_id" value="{$aRow.id}"/>
        <input type="hidden" name="data_type" value="review"/>
	<div class="box-body">
		<div class="form-group">
		  <label for="InputTitle">Заголовок отзыва</label>
		  <input type="text" name="title" value="{$aRow.title}" required="required" class="form-control" id="InputTitle" placeholder="Введите заголовок отзыва">
		</div>	 
		<div class="input-group col-lg-4 form-group">
			<label for="tfType">Тип потолка:</label>
			<select name="cat">
				<option value="0">Без категории</option>
			{foreach item=item from=$aCats}
				<option value="{$item.id}">{$item.title}</option>
			{/foreach}
			</select>
		</div>	
		<div class="input-group col-lg-4 form-group">
			<span class="input-group-addon">Возраст</span>
			<input type="text" class="form-control" name="age" value="{$aRow.age}">
			<span class="input-group-addon"> лет.</span>
		</div>				
		<div class="form-group">
			<label>
				<input type="checkbox" name="status" value="1" {if $aRow.status eq 1}checked{/if}> Опубливан (показывается на сайте)
			</label>
		</div>
		<div class="form-group">
		  <label for="InputTitle">Текст отзыва <small></small></label>
		  <textarea class="textarea default-wisiwyg" name="text" placeholder="Введите текст отзыва">{$aRow.text}</textarea>
		</div>
	</div><!-- /.box-body -->
	<div class="box-footer">
		<button type="submit" class="btn btn-primary">Сохранить</button>
	</div>
</form>
					
{include file="components/fileUpload.tpl" nId=$aRow.id strType='review'} 