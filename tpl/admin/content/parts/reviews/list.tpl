<div class="box-body">
<table id="example1" class="table table-bordered table-hover table-striped">
  <thead>
	<tr>
	  <th>№</th>
	  <th>ФИО отправителя</th>	  					
	  <th>Категория</th>
	  <th>Дата отзыва</th>
	  <th>Статус</th>	
	  <th>&nbsp;</th>
	</tr>
  </thead>
  <tbody>
  {foreach from=$aList item=item}
	<tr>
	  <td>{$item.id}</td>
	  <td><a href="{$MainUrl}/reviews/{$item.id}">{$item.author}</a></td>	  
	  <td>{$item.type}</td>
	  <td>{$item.date_add|date_format:"%d.%m.%Y %H:%M"}</td>
	  <td>{$item.status}</td>
	  <td><button class="btn btn-block btn-danger btn-delete-review" data-id="{$item.id}">Удалить</button></td>
	</tr>                     
  {/foreach}
  </tbody>
  {if $aList|@count > 9}
  <tfoot>
	<tr>
	  <th>Номер проекта</th>
	  <th>Название проета</th>
	  <th>Статус</th>						
	  <th>Количество фотографий</th>
	  <th>Количество просмотров</th>
	  <th>Количество заказов</th>
	  <th>&nbsp;</th>
	</tr>
  </tfoot>
  {/if}
</table>
</div><!-- /.box-body -->