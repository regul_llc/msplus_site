<section class="content">
	<div class="row">
	  <div class="col-xs-12">
		<div class="box">
		  <div class="box-header">
			<form method="post" class="input-group input-group-sm" style="max-width: 500px;">
				<input type="hidden" name="action" value="add"/>
				<input type="text" class="form-control" name="pname" placeholder="Заголовок отзыва" required="required">
				<span class="input-group-btn">
					<button class="btn btn-info btn-flat" name="submit" type="submit">Добавить новый отзыв</button>
				</span>
			</form>
			<br/>
			<h3 class="box-title">Список Отзывов</h3>
		  </div><!-- /.box-header -->
			{if isset($aList)}
				{include file="content/parts/reviews/list.tpl"}
			{else}
				{include file="content/parts/reviews/edit.tpl"}
			{/if}
		</div><!-- /.box -->
	  </div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->
