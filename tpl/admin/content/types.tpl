 <section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">
		{if isset($aList)}
			{include file="content/parts/types/list.tpl"}
		{else}
			{include file="content/parts/types/edit.tpl"}
		{/if}
	</div><!-- /.row -->
 </section>