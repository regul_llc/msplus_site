 <!-- Main content -->
 <section class="content">
	<!-- Small boxes (Stat box) -->
        <form method="post" class="input-group input-group-sm" style="max-width: 500px;">
            <input type="hidden" name="action" value="add"/>
            <input type="text" class="form-control" name="pname" placeholder="Заголовок статьи" required="required">
            <span class="input-group-btn">
                <button class="btn btn-info btn-flat" name="submit" type="submit">Добавить новую статью</button>
            </span>
        </form>
	<br/>
	<div class="row">
		{if isset($aList)}
			{include file="content/parts/articles/list.tpl"}
		{else}
			{include file="content/parts/articles/edit.tpl"}
		{/if}
	</div><!-- /.row -->
 </section><!-- /.content -->