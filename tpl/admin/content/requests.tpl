 <!-- Main content -->
 <section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">
		{if isset($aList)}
			{include file="content/parts/requests/list.tpl"}
		{else}
			{include file="content/parts/requests/edit.tpl"}
		{/if}
	</div><!-- /.row -->
 </section><!-- /.content -->