 <!-- Main content -->
 <section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">
		{if isset($aList)}
			{include file="content/parts/orders/list.tpl"}
		{else}
			{include file="content/parts/orders/edit.tpl"}
		{/if}
	</div><!-- /.row -->
 </section><!-- /.content -->