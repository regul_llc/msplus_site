<section class="content">
	<div class="row">
	  <div class="col-xs-12">
		<div class="box">
		  <div class="box-header">
			<form method="post" class="input-group input-group-sm" style="max-width: 500px;">
				<input type="hidden" name="action" value="add"/>
				<input type="text" class="form-control" name="pname" placeholder="Название проекта" required="required">
				<span class="input-group-btn">
					<button class="btn btn-info btn-flat" name="submit" type="submit">Добавить новый проект</button>
				</span>
			</form>
			<br/>
			<h3 class="box-title">Список проектов</h3>
		  </div><!-- /.box-header -->
			{if isset($aList)}
				{include file="content/parts/projects/list.tpl"}
			{else}
				{include file="content/parts/projects/edit.tpl"}
			{/if}
		</div><!-- /.box -->
	  </div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->
