<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
	<!-- Sidebar user panel -->
	<div class="user-panel">
	  <div class="pull-left image">
		<img src="/public/common/img/regul-logo.jpg" class="img-circle" alt="User Image" />
	  </div>
	  <div class="pull-left info">
		<p>{$aCurUser.name}</p>
		<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
	  </div>
	</div>

	<!-- sidebar menu: : style can be found in sidebar.less -->
	<ul class="sidebar-menu">
	  <li class="header">НАВИГАЦИЯ</li>	  
	  {foreach from=$aMenu key=key item=item}
		{if !empty($item.items)}
			<li class="treeview {if $aPath[0] eq $key}active{/if}">
				<a href="#">
				  <i class="fa fa-edit"></i> <span>{$item.name}</span>
				  <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					{foreach from=$item.items item=subitem}
						<li class="{if $aPath[1] eq $subitem.id}active{/if}"><a href="{$MainUrl}/{$key}/{$subitem.id}"><i class="fa fa-circle-o"></i> {$subitem.title}</a></li>
					{/foreach}
				</ul>
			</li>
		{else}
		<li class="{if $aPath[0] eq $key}active{/if}">
			<a href="{$MainUrl}/{$key}">
			  <i class="fa {$item.class}"></i> <span>{$item.name}</span> 
			  {*
			  <small class="label pull-right bg-green">new</small>
			  <small class="label pull-right bg-yellow">12</small>
			  <small class="label pull-right bg-red">3</small>
			  <span class="label label-primary pull-right">4</span>
			  *}
			</a>
		</li>
		{/if}
	  {/foreach}
	</ul>
  </section>
  <!-- /.sidebar -->
</aside>