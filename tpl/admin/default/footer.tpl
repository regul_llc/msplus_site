<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>Version</b> 0.3.1
	</div>
	<strong>Copyright &copy; 2014-2015 <a href="http://webregul.ru">Regul, LLC</a>.</strong> All rights reserved.
</footer>