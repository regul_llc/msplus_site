<!DOCTYPE html>
<html>
{include file="default/header.tpl"}
<body class="skin-blue fixed" data-mainurl="{$MainUrl}">
	<div class="wrapper">
		{include file="default/top.tpl"}
		{include file="default/left_column.tpl"}
		<div class="content-wrapper">
			{include file="default/content_header.tpl"}
			{include file="content/$ContentTemplate.tpl"}			
		</div>		
		{include file="default/footer.tpl"}
	</div>
	{include file="popups/all.tpl"}
	{include file="default/footer_system.tpl"}
</body>
</html>
