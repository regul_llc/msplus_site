<div id="SaveSuccess" class="modal fade">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <h4 class="modal-title">Данные успешно сохранены</h4>
		</div>
		<div class="modal-body">
		  <p>Можете просто закрыть это окно и продолжить работу на этой странице. Или перейти к списку... 
			  И закрыть окно и пойти пить чай... Или не закрывать окно... Возможно все!!! Это окно бескрайних возможностей.</p>
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
		  <button type="button" class="btn btn-primary btnhref back-to-list" rel="{$MainUrl}">Перейти к списку</button>
		</div>
	  </div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="ConfirmDelete" class="modal fade">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <h4 class="modal-title">Подтверждение удаления</h4>
		</div>
		<div class="modal-body">
		  <p>Вы действительно хотите удалить?</p>
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Отмена</button>
		  <button type="button" class="btn btn-primary btn-danger btn-delete close-window" rel="">Удалить</button>
		</div>
	  </div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->