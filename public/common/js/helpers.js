
function number_format(number, decimals, dec_point, thousands_sep){
	//  discuss at: http://phpjs.org/functions/number_format/
	// original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
	//   example 1: number_format(1234.56);
	//   returns 1: '1,235'
	//   example 2: number_format(1234.56, 2, ',', ' ');
	//   returns 2: '1 234,56'
	//   example 8: number_format(67000, 5, ',', '.');
	//   returns 8: '67.000,00000'
	//   example 9: number_format(0.9, 0);
	//   returns 9: '1'
	number = (number + '')
	  .replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
	  prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
	  sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
	  dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
	  s = '',
	  toFixedFix = function(n, prec) {
		var k = Math.pow(10, prec);
		return '' + (Math.round(n * k) / k)
		  .toFixed(prec);
	  };
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
	  .split('.');
	if (s[0].length > 3) {
	  s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '')
	  .length < prec) {
	  s[1] = s[1] || '';
	  s[1] += new Array(prec - s[1].length + 1)
		.join('0');
	}
	return s.join(dec);
}

