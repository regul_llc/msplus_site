$(document).ready(function(){
	var MainUrl = $('body').attr('data-mainurl');
	
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass: 'iradio_minimal-blue'
	});
	
	
	$('.ajaxform').on('submit',function(){
		var me = $(this);
		$.post("/ajax/saveData", 
			{'data': me.serializeObject()}, function(data){
				$('#SaveSuccess button.back-to-list').attr('rel', me.attr('data-listurl'));
				$('#SaveSuccess').modal();
			}, "json"
		);		
		return false;
	});
	
	$('.btnhref').on('click',function(){
		window.location.href = $(this).attr('rel');
	});
	
	$('.btn-delete-project').on('click',function(){
		me = $(this);		
		confirmDelete('Вы действительно хотите удалить проект?', function(){
			$.post("/ajax/deleteProject", 
				{'project_id': me.attr('data-id')}, function(data){
					me.parents('tr').slideUp(1000);
				}, "json"
			);	
		},'Удалить проект');
	});
});

function confirmDelete(strMessage, callback, strBtnText){
	strBtnText = strBtnText || 'Удалить';
	$('#ConfirmDelete div.modal-body p').text(strMessage);
	$('#ConfirmDelete button.btn-delete').text(strBtnText);
	$('#ConfirmDelete button.btn-delete').one('click',callback);
	$('#ConfirmDelete button.close-window').one('click',function(){ $('#ConfirmDelete').modal('hide'); });
	$('#ConfirmDelete').modal('show');
}