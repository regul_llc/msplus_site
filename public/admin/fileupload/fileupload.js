$(document).ready(function () {
    function pageLoad(){
        'use strict';
		
        // Initialize the jQuery File Upload widget:
        var $fileupload = $('#fileupload');
        $fileupload.fileupload({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            //url: '/ajax/uploadProjectPhotos?project_id='+$fileupload.attr('data-pid')+'&data-type='+$fileupload.attr('data-type'),
			url: $fileupload.attr('action'),
			getUrl: $fileupload.attr('data-geturl'),
			testParam: 'ddd',
            dropZone: $('#dropzone'),
			always: function(){
				console.log('done');
				
			},
			stop: function (e) {
				console.log('Uploads finished');
				setTimeout(setIChecks(),1000);
			}
        });		
		
        // Enable iframe cross-domain access via redirect option:
        $fileupload.fileupload(
            'option',
            'redirect',
            window.location.href.replace(
                /\/[^\/]*$/,
                '/cors/result.html?%s'
            )
        );
		
        // Load existing files:
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $fileupload.fileupload('option', 'getUrl'),
            dataType: 'json',
            context: $fileupload[0]
        }).done(function (result) {
			$(this).fileupload('option', 'done').call(this, null, {result: result});	
			setIChecks()
		});		
    }
	
	function setIChecks(){
		$('input.setmainimg').parent().parent().removeClass('hidden');
		$('input.setmainimg').iCheck({
			checkboxClass: 'icheckbox_minimal-blue',
			radioClass: 'iradio_minimal-blue'
		});			
		$('input.setmainimg').on('ifChecked', function(event){
			console.log('setmain');
			var nImgId = $(this).parents('tr.template-download').first().attr('data-id');
			var nProjectId = $(this).parents('form#fileupload').first().attr('data-pid');
                        var dataType = $(this).parents('form#fileupload').first().attr('data-type');
			$.post( "/ajax/setMainImg", {'img_id' : nImgId, 'project_id' : nProjectId, 'data-type' : dataType} );
		});
	}

    pageLoad();

    //PjaxApp.onPageLoad(pageLoad);

});

