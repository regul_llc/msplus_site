$(function() {
  $("#phone_str").mask("+7 (999) 999-99-99");

  $('[placeholder]').placeholder();

  if ($.fn.iCarousel) {
    $('#icarousel').iCarousel({
            slides: 7,
            slidesSpace: 160,
            nextLabel: "Вперед",
            previousLabel: "Назад",
            playLabel: "Начать",
            pauseLabel: "Пауза",
            timer: "360Bar",
            timerPadding: 3,
            timerOpacity: 0.4,
            timerColor: "#0F0",
            timerX: 10,
            timerY: 20
          });
  }

  if ($('.bannercontainer').length > 0) {
    $('.bannercontainer').kenburn(
              {
                thumbWidth:50,
                thumbHeight:50,
                
                thumbAmount:5,                                          
                thumbStyle:"both",
                thumbVideoIcon:"off",
                
                thumbVertical:"bottom",
                thumbHorizontal:"center",             
                thumbXOffset:0,
                thumbYOffset:40,
                bulletXOffset:0,
                bulletYOffset:-16,
                
                hideThumbs:"on",
                                                                                      
                touchenabled:'on',
                pauseOnRollOverThumbs:'off',
                pauseOnRollOverMain:'off',
                preloadedSlides:2,              
                
                timer:7,
                
                debug:"off"              
            });
  }
  
	//Галлерея изображений
  $('.gallery').carouFredSel({
    items: 1,
    scroll: {
      fx: "crossfade"
    },
    auto: false,
    pagination: {
      container: '.gallery-container .thumbnails',
      anchorBuilder: function(nr) {
        var src = $(this).attr('src');
        src = src.replace('/large/', '/small/');
        return '<img src="' + src + '">';
      }
    }
  });

  $('.js-no-action').on('click', function(event) {
    event.preventDefault();
  });

  $('.order').on('click', function() {
    $('.order-form-container').arcticmodal();
  });
  $('.calculator').on('click', function() {
    $('.calc-form-container').arcticmodal();
  });

  if ($.fn.fancybox) {
    $(".fancybox").fancybox();
  }



  //Slider gallery in content
  var thumbnailsWidth = 0;
  $('.thumbnails img').each(function() {
    var imgWidth = /*$(this).width()*/133 + parseInt($(this).css('margin-left')) + parseInt($(this).css('margin-right'));
    thumbnailsWidth += imgWidth;
  });
  $('.thumbnails').width(thumbnailsWidth);
  $('.thumbnails img:nth-child(4)').addClass('lastVisible');
  $('.thumbnails img:nth-child(1)').addClass('firstVisible');

  $('.pagination-right').on('click', function() {
    var lastVisibleImage = $('.thumbnails .lastVisible');
    if (!isLast(lastVisibleImage[0])) {
      slideToNext();
    }
    //Turn on arrows
    var linkLeft = $('.pagination-left');
    if (linkLeft.hasClass('disabled')) {
      linkLeft.removeClass('disabled');
    }
  });
  $('.pagination-left').on('click', function() {
    var firstVisibleImage = $('.thumbnails .firstVisible');
    if (!isFirst(firstVisibleImage[0])) {
      slideToPrevious();
    }
    //Turn on arrows
    var linkRight = $('.pagination-right');
    if (linkRight.hasClass('disabled')) {
      linkRight.removeClass('disabled');
    }
  });

  var moveOffset = 32 + 133;
  var images = $('.thumbnails img');  
  function isLast(image) {
    return image === $('.thumbnails img:last-child')[0];
  }
  function isFirst(image) {
    return image === $('.thumbnails img:first-child')[0];
  }
  function slideToNext() {
    $('.thumbnails').animate({left: '-=' + moveOffset}, 400);
    
    //Set firstVisible
    var indexOfFirstVisible = images.index($('.thumbnails .firstVisible'));
    images.siblings('.firstVisible').removeClass('firstVisible');
    images.eq(indexOfFirstVisible + 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = images.index($('.thumbnails .lastVisible'));
    images.siblings('.lastVisible').removeClass('lastVisible');
    images.eq(indexOfLastVisible + 1).addClass('lastVisible');

    var lastVisible = $('.thumbnails .lastVisible');
    if (isLast(lastVisible[0])) {
      $('.pagination-right').addClass('disabled');
    }
  }
  function slideToPrevious() {
    $('.thumbnails').animate({left: '+=' + moveOffset}, 400);
    
    //Set firstVisible
    var indexOfFirstVisible = images.index($('.thumbnails .firstVisible'));
    images.siblings('.firstVisible').removeClass('firstVisible');
    images.eq(indexOfFirstVisible - 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = images.index($('.thumbnails .lastVisible'));
    images.siblings('.lastVisible').removeClass('lastVisible');
    images.eq(indexOfLastVisible - 1).addClass('lastVisible');

    var firstVisible = $('.thumbnails .firstVisible');
    if (isFirst(firstVisible[0])) {
      $('.pagination-left').addClass('disabled');
    }
  }



  //Ховер фото в слайдере портфолио (слева который)
  //Ховер фото на странице РАБОТЫ
  $('.work__image-block img, .work-section__image-block__image').on('mouseenter', function () {
    $(this).animate({
      top: '+=15',
      left: '+=22',
      width: '-=44',
      height: '-=30'
    });
  }).on('mouseleave', function () {
    $(this).animate({
      top: '-=15',
      left: '-=22',
      width: '+=44',
      height: '+=30'
    });
  });


  //Прокрутка портфолио, раз в 7 секунд
  var goUp = false;

  setInterval(function () {

    var lastVisibleBlock = $('.work-container .lastVisible');
    var firstVisibleBlock = $('.work-container .firstVisible');

    if (!goUp) {
      if (!isLastBlock(lastVisibleBlock[0])) {
        slideToDown();
        //Turn on arrows
        var linkUp = $('.pagination-up');
        if (linkUp.hasClass('disabled')) {
          linkUp.removeClass('disabled');
        }
      } else {
        goUp = true;        
      }
    }

    if (goUp) {
      if (!isFirstBlock(firstVisibleBlock[0])) {
        slideToUp();
        //Turn on arrows
        var linkDown = $('.pagination-down');
        if (linkDown.hasClass('disabled')) {
          linkDown.removeClass('disabled');
        }
      } else {
        goUp = false;
        if (!isLastBlock(lastVisibleBlock[0])) {
          slideToDown();
          //Turn on arrows
          var linkUp = $('.pagination-up');
          if (linkUp.hasClass('disabled')) {
            linkUp.removeClass('disabled');
          }
        }
      }
    }
  }, 7000);

  //Slider gallery in portfolio
  var workContainerHeight = 0;
  $('.work-container .work').each(function(index) {
    if (index > 1) {
      return;
    };
    var workHeight = $(this).height() + parseInt($(this).css('margin-top')) + parseInt($(this).css('margin-bottom'))
                       + parseInt($(this).css('padding-top')) + parseInt($(this).css('padding-bottom'));
    workContainerHeight += workHeight;
  });
  $('.work-container').height(workContainerHeight);
  $('.work-container .work:nth-child(1)').addClass('firstVisible');
  // $('.work-container .work:nth-child(2)').addClass('lastVisible');
  $('.work-container .work:nth-child(1)').addClass('lastVisible');

  $('.pagination-down a').on('click', function() {
    var lastVisibleBlock = $('.work-container .lastVisible');
    if (!isLastBlock(lastVisibleBlock[0])) {
      slideToDown();
    }
    //Turn on arrows
    var linkUp = $('.pagination-up');
    if (linkUp.hasClass('disabled')) {
      linkUp.removeClass('disabled');
    }
  });
  $('.pagination-up a').on('click', function() {
    var firstVisibleBlock = $('.work-container .firstVisible');
    if (!isFirstBlock(firstVisibleBlock[0])) {
      slideToUp();
    }
    //Turn on arrows
    var linkDown = $('.pagination-down');
    if (linkDown.hasClass('disabled')) {
      linkDown.removeClass('disabled');
    }
  });

  var elements = $('.work-container .work');  
  function isLastBlock(block) {
    return block === $('.work-container .work:last-child')[0];
  }
  function isFirstBlock(block) {
    return block === $('.work-container .work:first-child')[0];
  }
  function slideToDown() {    
    //Set firstVisible
    var indexOfFirstVisible = elements.index($('.work-container .firstVisible'));
    elements.siblings('.firstVisible').removeClass('firstVisible');
    elements.eq(indexOfFirstVisible + 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = elements.index($('.work-container .lastVisible'));
    elements.siblings('.lastVisible').removeClass('lastVisible');
    elements.eq(indexOfLastVisible + 1).addClass('lastVisible');

    var lastVisibleElement = $('.work-container .lastVisible');
    var moveOffset = lastVisibleElement.height() + parseInt(lastVisibleElement.css('margin-top')) + parseInt(lastVisibleElement.css('margin-bottom'))
                       + parseInt(lastVisibleElement.css('padding-top')) + parseInt(lastVisibleElement.css('padding-bottom')) + 1;
    $('.work-moving-container').animate({top: '-=' + moveOffset}, 400);

    if (isLastBlock(lastVisibleElement[0])) {
      $('.pagination-down').addClass('disabled');
    }
  }
  function slideToUp() {    
    //Set firstVisible
    var indexOfFirstVisible = elements.index($('.work-container .firstVisible'));
    elements.siblings('.firstVisible').removeClass('firstVisible');
    elements.eq(indexOfFirstVisible - 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = elements.index($('.work-container .lastVisible'));
    elements.siblings('.lastVisible').removeClass('lastVisible');
    elements.eq(indexOfLastVisible - 1).addClass('lastVisible');

    var firstVisibleElement = $('.work-container .firstVisible');
    var moveOffset = firstVisibleElement.height() + parseInt(firstVisibleElement.css('margin-top')) + parseInt(firstVisibleElement.css('margin-bottom'))
                       + parseInt(firstVisibleElement.css('padding-top')) + parseInt(firstVisibleElement.css('padding-bottom')) + 1;
    $('.work-moving-container').animate({top: '+=' + moveOffset}, 400);

    if (isFirstBlock(firstVisibleElement[0])) {
      $('.pagination-up').addClass('disabled');
    }
  }
});

if ($('#map').length > 0) {
  ymaps.ready(init);
}
var myMap, 
    myPlacemark;

function init(){ 
    myMap = new ymaps.Map ("map", {
        center: [47.2520,39.7351],
        zoom: 12,
        behaviors: ['default', 'scrollZoom']
    });

    //Создаем и добавляем метку    
    myPlacemark = new ymaps.Placemark([47.2520,39.7351], {
        content: '',
        balloonContent: ''
    });    
    myMap.geoObjects.add(myPlacemark);

    // Создание экземпляра элемента управления
    myMap.controls.add(
       new ymaps.control.ZoomControl()
    );

    // Обращение к конструктору класса элемента управления по ключу - кнопки "Схема", "Спутник"
    myMap.controls.add('typeSelector');

    //Добавляем кнопки на карте
    myMap.controls.add('mapTools');
}