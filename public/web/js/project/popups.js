// Popups
$(function () {

	$('.popup-close').on('click', function (event) {
		event.stopPropagation();

		$(this).closest('.popup').hide();
	});

	$('[data-popup]').on('click', function (event) {
		event.stopPropagation();
		event.preventDefault();

		var $this = $(this);
		var $popupToOpen = $('#' + $this.data('popup'));
		
		var offsetX = ( $popupToOpen.outerWidth() - $this.outerWidth() ) / 2;
		var offsetY = 60;
		var positionLeft = $this.offset().left - offsetX;
		var positionTop = $this.offset().top + offsetY;

		if (positionTop + $popupToOpen.height() > $(document).height()) {
			positionTop = positionTop - $popupToOpen.height() - (offsetY * 2) - 15;
			$popupToOpen.addClass('__style-2');
		} else {
			$popupToOpen.removeClass('__style-2');
		}

		$popupToOpen.css({
			left: positionLeft,
			top: positionTop
		});

		$popupToOpen.show();
	});

	$(document).on('click', function (event) {
		$('.popup').hide();
	});

	$('.popup').on('click', function (event) {
		event.stopPropagation();
	});
});