$(function () {
	$('.mask_phone').mask("7 (999) 999-9999");
	$('.mask_phone').attr('pattern','^7 \\([\\d]{3}\\)\\s[\\d]{3}(\\s|-)[\\d]{4}$').attr('title', 'Телефон в формате 7 (999) 999-9999');
	
	jVForms.initialize({
        notice: 'Off',
		pattern: {
			all: '[\\S\\s]+',                   // любой символ из набора.
               
			numInt: '^\\d+$',                // число целое.
			numFloat: '^\\d+(\\.|,)\\d+$',   // число десятичное.
			notNum: '^\\D+$',                // кроме чисел.
			index: '^\\d{6}$',               // числовой индекс.

			wordUpper: '^[A-ZА-ЯЁ-]+$',      // слово на Ru/US в верхнем регистре и знак(-).
			wordLower: '^[a-zа-яё-]+$',      // слово на Ru/US в нижнем регистре и знак(-).
			wordRuUpper: '^[А-ЯЁ-]+$',       // слово на Ru в верхнем регистре и знак(-).
			wordRuLower: '^[а-яё-]+$',       // слово на Ru в нижнем регистре и знак(-).
			wordUSUpper: '^[A-Z-]+$',        // слово на US в верхнем регистре и знак(-).
			wordUSLower: '^[a-z-]+$',        // слово на US в нижнем регистре и знак(-).

			stringRu: '^[^a-zA-Z]+$',        // сторка любая не содержащая US букв.
			stringUS: '^[^а-яА-ЯёЁ]+$',      // сторка любая не содержащая Ru букв.

			timeHM: '^[012][\\d]:[012345][\\d]$',        // время в формате час:минуты.
			dateDMY: '^[0123][\\d]/[01][\\d]/[\\d]{4}$', // дата в формате день/месяц/год.
			dataDMYus: '[01][\\d]/[0123][\\d]/[\\d]{4}', // дата в форматк месяц/день/год.

			cc: '^[\\d]{4}\\s[\\d]{4}\\s[\\d]{4}\\s[\\d]{4}$', // кредитная карта в формате 9999 9999 9999 9999.

			phone: '^[\\d]{3}(\\s|-)?[\\d]{2}(\\s|-)?[\\d]{2}$',     // номер в формате 999 99 99|9999999|999-99-99|999-99 99.
			phoneDash: '^\\([\\d]{3}\\)\\s[\\d]{3}(\\s|-)[\\d]{4}$', // номер в формате (999) 999-9999|(999) 999 9999.
			phoneAlong: '^\\([\\d]{3}\\)\\s[\\d]{7}$',               // номер в формате (999) 9999999.
			phoneRF: '^\\+\7\s\([\\d]{3}\\)\\s[\\d]{3}(\\s|-)[\\d]{4}$' // номер в формате (999) 999-9999|(999) 999 9999.
			
		}
    });

	$('.send-request-form input[name="name"]').attr('required','required');
	$('.send-request-form input[name="phone"]').attr('required','required');

	$('.buy-project-btn').on('click', function (event) {
		event.stopPropagation();
		var addition = $(this).attr('data-addition');   
		var message = $(this).attr('data-message');
		var source = $(this).attr('data-source');		
		var $popup = $('#popup-buy');
		$popup.css({ top: 50, left: '50%', 'margin-left': -$popup.outerWidth()/2 });	
		$popup.find('input[name="request-type"]').val(addition);
		$popup.find('input[name="request-source"]').val(source);
		$popup.find('input[name="request-message"]').val(message);
		$('.popup-overlay').show();
		$('#popup-buy').show();
		return false;
	});
	
	$('.popup-2__close').on('click', function (event) {
		event.stopPropagation();
		$('.popup-overlay').hide();
		$(this).closest('#popup-buy').hide();
	});
	
//	$('.send-data-button').on('click', function(event){
//		event.stopPropagation();
//		var form = $(this).closest('.send-request-form');
//		var data = form.serializeObject();
//		console.log('startSend');
//		$.ajax({
//			url: '/ajax/setRequest',
//			type:"POST",
//			data: data,
//			contentType:"application/json; charset=utf-8",
//			dataType:"json",
//			success: function (response) {
//				alert('sended');
//			}
//		});
//		console.log(data);
//		return false;
//	});
	
	$('.send-request-form').on('submit', function(event){
		event.stopPropagation();
		var self = this;
		$.post("/ajax/setOrderRequest", { 'data': $(this).serializeObject() }, 
			function(data){
				$(self).closest('.popup-call').hide();
				fancyNotif('<h4>Ваша заявка принята!</h4><br/><p>В ближайшее время мы с Вами свяжемся.</p>');
			}, "json"
		);
		return false;
	});
	
	
	$('.send-review-button').on('click', function(event){
		event.stopPropagation();
		var form = $(this).closest('.send-review-form');

		var data = form.serializeObject();
		console.log('startSend');
		$.ajax({
			url: '/ajax/addReview',
			type:"POST",
			data: JSON.stringify(
				data
			),
			contentType:"application/json; charset=utf-8",
			dataType:"json",
			success: function (response) {
				alert('sended');
			}
		});
		console.log(data);
		return false;
	});
	
	$('.open-custom-win').on('click',function(event){
		event.stopPropagation();
		var self = this;
		var strWinId = $(this).attr('data-winId');
		var nItemId = $(this).attr('data-id');
		$.post("/ajax/getWinContent", { 'win': strWinId, 'id': nItemId}, 
			function(data){
				$('#popup-custom').show();
				console.log(data);
			}, "json"
		);
	});
	
	$('.send-order-form').on('submit',function(){
		self = this;
		$.post("/ajax/setOrder", 
			{'data':  $(this).serializeObject() }, function(data){
				$(self).parent('.popup-2').hide();
				$('.popup-overlay').hide();
				fancyNotif('<h5>Ваша заявка принята!</h5><br/><p>В ближайшее время мы с Вами свяжемся.</p>');
			}, "json"
		);		
		return false;
	});
	
	
	
	$('img.project_img').on('mouseenter', function(){
		$(this).parent().children('.projects-overlay').fadeIn(0);
	});
	$('.projects-overlay').on('mouseleave', function(){
		$(this).fadeOut(0);
	});
	
	
	projectsImagesLoad();
	
});

function projectsImagesLoad(){
	$('#ProjectsBlocks .projimages').each(function(i,e){
		var nProjectId = $(e).attr('data-pid');
		var imgTpl =	'<a class="houses-info__item__image-block fancybox" rel="pr{ProjectId}" href="/data/images/projects/{ProjectId}/trimmed/{ImgId}_big.jpg">'+
							'<img src="/data/images/projects/{ProjectId}/trimmed/{ImgId}_small.jpg" height="127" width="152" alt="Дом">'+
						'</a>';
		$.post("/ajax/getProjectImages", 
			{'project_id': nProjectId}, function(data){
				var aImgsId = data.images;		
				for (var x=0; x<aImgsId.length; x++){
					var curImg = imgTpl;
					curImg = curImg.replace(/{ProjectId}/g,nProjectId);
					curImg = curImg.replace(/{ImgId}/g,aImgsId[x]);
					$(e).append(curImg);
				}	
				fancyInit('.fancybox');
			}, "json"
		);
	});
}

function fancyInit(strElement){
	$(strElement).fancybox({
		openEffect	: 'none',
		closeEffect	: 'none',
		prevEffect	: 'fade',
		nextEffect	: 'fade',
		playSpeed	: 2000,
		helpers		: {
			title	: { type : 'inside' },
			buttons	: {
				'prev' : true,
				'next' : false	
			}
		}
	});
}

function fancyNotif(strText){
	var win = $.fancybox(
		strText,
		{
			'autoDimensions'	: false,
			'width'         	: 'auto',
			'height'        	: 'auto',
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'opacity'			: true,
			'overlayShow'		: false,
			'autoCenter'		: false,
			'type'				: 'inline',
			'topRatio'			: 0.2,
			 helpers:  { overlay : null },
			 afterLoad			: function(){
				 console.log($(this));
				 setTimeout(function() {$.fancybox.close(); },3000);
			 }
		}
	);
}