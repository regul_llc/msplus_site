$(function () {
	$('.mask_phone').mask("7 (999) 999-9999");
	$('.mask_phone').attr('pattern','^7 \\([\\d]{3}\\)\\s[\\d]{3}(\\s|-)[\\d]{4}$').attr('title', 'Телефон в формате 7 (999) 999-9999');
	
	jVForms.initialize({
        notice: 'Off',
		pattern: {
			all: '[\\S\\s]+',                   // любой символ из набора.
               
			numInt: '^\\d+$',                // число целое.
			numFloat: '^\\d+(\\.|,)\\d+$',   // число десятичное.
			notNum: '^\\D+$',                // кроме чисел.
			index: '^\\d{6}$',               // числовой индекс.

			wordUpper: '^[A-ZА-ЯЁ-]+$',      // слово на Ru/US в верхнем регистре и знак(-).
			wordLower: '^[a-zа-яё-]+$',      // слово на Ru/US в нижнем регистре и знак(-).
			wordRuUpper: '^[А-ЯЁ-]+$',       // слово на Ru в верхнем регистре и знак(-).
			wordRuLower: '^[а-яё-]+$',       // слово на Ru в нижнем регистре и знак(-).
			wordUSUpper: '^[A-Z-]+$',        // слово на US в верхнем регистре и знак(-).
			wordUSLower: '^[a-z-]+$',        // слово на US в нижнем регистре и знак(-).

			stringRu: '^[^a-zA-Z]+$',        // сторка любая не содержащая US букв.
			stringUS: '^[^а-яА-ЯёЁ]+$',      // сторка любая не содержащая Ru букв.

			timeHM: '^[012][\\d]:[012345][\\d]$',        // время в формате час:минуты.
			dateDMY: '^[0123][\\d]/[01][\\d]/[\\d]{4}$', // дата в формате день/месяц/год.
			dataDMYus: '[01][\\d]/[0123][\\d]/[\\d]{4}', // дата в форматк месяц/день/год.

			cc: '^[\\d]{4}\\s[\\d]{4}\\s[\\d]{4}\\s[\\d]{4}$', // кредитная карта в формате 9999 9999 9999 9999.

			phone: '^[\\d]{3}(\\s|-)?[\\d]{2}(\\s|-)?[\\d]{2}$',     // номер в формате 999 99 99|9999999|999-99-99|999-99 99.
			phoneDash: '^\\([\\d]{3}\\)\\s[\\d]{3}(\\s|-)[\\d]{4}$', // номер в формате (999) 999-9999|(999) 999 9999.
			phoneAlong: '^\\([\\d]{3}\\)\\s[\\d]{7}$',               // номер в формате (999) 9999999.
			phoneRF: '^\\+\7\s\([\\d]{3}\\)\\s[\\d]{3}(\\s|-)[\\d]{4}$' // номер в формате (999) 999-9999|(999) 999 9999.
			
		}
    });

	$('.send-request-form input[name="name"]').attr('required','required');
	$('.send-request-form input[name="phone"]').attr('required','required');

	$('.buy-project-btn').on('click', function (event) {
		event.stopPropagation();
		var addition = $(this).attr('data-addition');   
		var message = $(this).attr('data-message');
		var source = $(this).attr('data-source');		
		var $popup = $('#popup-buy');
		$popup.css({ top: 50, left: '50%', 'margin-left': -$popup.outerWidth()/2 });	
		$popup.find('input[name="request-type"]').val(addition);
		$popup.find('input[name="request-source"]').val(source);
		$popup.find('input[name="request-message"]').val(message);
		$('.popup-overlay').show();
		$('#popup-buy').show();
		return false;
	});
	
	$('.popup-2__close').on('click', function (event) {
		event.stopPropagation();
		$('.popup-overlay').hide();
		$(this).closest('#popup-buy').hide();
	});
	
//	$('.send-data-button').on('click', function(event){
//		event.stopPropagation();
//		var form = $(this).closest('.send-request-form');
//		var data = form.serializeObject();
//		console.log('startSend');
//		$.ajax({
//			url: '/ajax/setRequest',
//			type:"POST",
//			data: data,
//			contentType:"application/json; charset=utf-8",
//			dataType:"json",
//			success: function (response) {
//				alert('sended');
//			}
//		});
//		console.log(data);
//		return false;
//	});;
	
	$('.send-request-form').on('submit', function(event){
		event.preventDefault();alert(23)
		event.stopPropagation();
		var self = this;
		$.post("/ajax/setOrderRequest", { 'data': $(this).serializeObject() }, 
			function(data){
				$(self).closest('.popup-call').hide();
				fancyNotif('<h4>Ваша заявка принята!</h4><br/><p>В ближайшее время мы с Вами свяжемся.</p>');
			}, "json"
		);
		return false;
	});


    // Фото и цены на натяжные потолки (глаз)
    $('.order-form-container.works-pop').on('.send-request-form submit', function (event) {
        event.stopPropagation();
        var form = $(this).find('.send-request-form');
        $.post("/ajax/setOrderRequest", {'data': $(form).serializeObject()}, function (data) {
            $(form).closest('.popup-call').hide();
            fancyNotif('<h4>Ваша заявка принята!</h4><br/><p>В ближайшее время мы с Вами свяжемся.</p>');
        }, "json");
        return false;
    });
	
	
	$('.send-review-button').on('click', function(event){
		event.stopPropagation();
		var form = $(this).closest('.send-review-form');

		var data = form.serializeObject();
		console.log('startSend');
		$.ajax({
			url: '/ajax/addReview',
			type:"POST",
			data: JSON.stringify(
				data
			),
			contentType:"application/json; charset=utf-8",
			dataType:"json",
			success: function (response) {
				alert('sended');
			}
		});
		console.log(data);
		return false;
	});
 
	
	$('.send-order-form').on('submit',function(){
		self = this;
		$.post("/ajax/setOrder", 
			{'data':  $(this).serializeObject() }, function(data){
				$(self).parent('.popup-2').hide();
				$('.popup-overlay').hide();
				fancyNotif('<h5>Ваша заявка принята!</h5><br/><p>В ближайшее время мы с Вами свяжемся.</p>');
			}, "json"
		);		
		return false;
	});
	
	$('.open-custom-win').on('click',function(event){
		event.stopPropagation();
		var self = this;
		var strWinId = $(this).attr('data-winId');
		var nItemId = $(this).attr('data-id');
		$.post("/ajax/getWinContent", { 'win': strWinId, 'id': nItemId}, 
			function(data){
				$('#popup-custom').show();
				console.log(data);
			}, "json"
		);
		return false;
	});
	
	$('img.project_img').on('mouseenter', function(){
		$(this).parent().children('.projects-overlay').fadeIn(0);
	});
	$('.projects-overlay').on('mouseleave', function(){
		$(this).fadeOut(0);
	});
	
	
	projectsImagesLoad();
	
});

function projectsImagesLoad(){
	$('#ProjectsBlocks .projimages').each(function(i,e){
		var nProjectId = $(e).attr('data-pid');
		var imgTpl =	'<a class="houses-info__item__image-block fancybox" rel="pr{ProjectId}" href="/data/images/projects/{ProjectId}/trimmed/{ImgId}_big.jpg">'+
							'<img src="/data/images/projects/{ProjectId}/trimmed/{ImgId}_small.jpg" height="127" width="152" alt="Дом">'+
						'</a>';
		$.post("/ajax/getProjectImages", 
			{'project_id': nProjectId}, function(data){
				var aImgsId = data.images;		
				for (var x=0; x<aImgsId.length; x++){
					var curImg = imgTpl;
					curImg = curImg.replace(/{ProjectId}/g,nProjectId);
					curImg = curImg.replace(/{ImgId}/g,aImgsId[x]);
					$(e).append(curImg);
				}	
				fancyInit('.fancybox');
			}, "json"
		);
	});
}

function fancyInit(strElement){
	$(strElement).fancybox({
		openEffect	: 'none',
		closeEffect	: 'none',
		prevEffect	: 'fade',
		nextEffect	: 'fade',
		playSpeed	: 2000,
		helpers		: {
			title	: { type : 'inside' },
			buttons	: {
				'prev' : true,
				'next' : false	
			}
		}
	});
}

function fancyNotif(strText){
	var win = $.fancybox(
		strText,
		{
			'autoDimensions'	: false,
			'width'         	: 'auto',
			'height'        	: 'auto',
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'opacity'			: true,
			'overlayShow'		: false,
			'autoCenter'		: false,
			'type'				: 'inline',
			'topRatio'			: 0.2,
			 helpers:  { overlay : null },
			 afterLoad			: function(){
				 console.log($(this));
				 setTimeout(function() {$.fancybox.close(); },3000);
			 }
		}
	);
}

$(function() {
  $("#phone_str").mask("+7 (999) 999-99-99");

  $('[placeholder]').placeholder();

  if ($.fn.iCarousel) {
    $('#icarousel').iCarousel({
            slides: 7,
            slidesSpace: 160,
            nextLabel: "Вперед",
            previousLabel: "Назад",
            playLabel: "Начать",
            pauseLabel: "Пауза",
            timer: "360Bar",
            timerPadding: 3,
            timerOpacity: 0.4,
            timerColor: "#0F0",
            timerX: 10,
            timerY: 20
          });
  }

  if ($('.bannercontainer').length > 0) {
    $('.bannercontainer').kenburn(
              {
                thumbWidth:50,
                thumbHeight:50,
                
                thumbAmount:5,                                          
                thumbStyle:"both",
                thumbVideoIcon:"off",
                
                thumbVertical:"bottom",
                thumbHorizontal:"center",             
                thumbXOffset:0,
                thumbYOffset:40,
                bulletXOffset:0,
                bulletYOffset:-16,
                
                hideThumbs:"on",
                                                                                      
                touchenabled:'on',
                pauseOnRollOverThumbs:'off',
                pauseOnRollOverMain:'off',
                preloadedSlides:2,              
                
                timer:7,
                
                debug:"off"              
            });
  }
  
	//Галлерея изображений
  $('.gallery').carouFredSel({
    items: 1,
    scroll: {
      fx: "crossfade"
    },
    auto: false,
    pagination: {
      container: '.gallery-container .thumbnails',
      anchorBuilder: function(nr) {
        var src = $(this).attr('src');
        src = src.replace('/large/', '/small/');
        return '<img src="' + src + '">';
      }
    }
  });

  $('.js-no-action').on('click', function(event) {
    event.preventDefault();
  });

  $('.order').on('click', function() {
    $('.order-form-container').arcticmodal();
  });
  $('.calculator').on('click', function() {
    $('.calc-form-container').arcticmodal();
  });

  if ($.fn.fancybox) {
    $(".fancybox").fancybox();
  }



  //Slider gallery in content
  var thumbnailsWidth = 0;
  $('.thumbnails img').each(function() {
    var imgWidth = /*$(this).width()*/133 + parseInt($(this).css('margin-left')) + parseInt($(this).css('margin-right'));
    thumbnailsWidth += imgWidth;
  });
  $('.thumbnails').width(thumbnailsWidth);
  $('.thumbnails img:nth-child(4)').addClass('lastVisible');
  $('.thumbnails img:nth-child(1)').addClass('firstVisible');

  $('.pagination-right').on('click', function() {
    var lastVisibleImage = $('.thumbnails .lastVisible');
    if (!isLast(lastVisibleImage[0])) {
      slideToNext();
    }
    //Turn on arrows
    var linkLeft = $('.pagination-left');
    if (linkLeft.hasClass('disabled')) {
      linkLeft.removeClass('disabled');
    }
  });
  $('.pagination-left').on('click', function() {
    var firstVisibleImage = $('.thumbnails .firstVisible');
    if (!isFirst(firstVisibleImage[0])) {
      slideToPrevious();
    }
    //Turn on arrows
    var linkRight = $('.pagination-right');
    if (linkRight.hasClass('disabled')) {
      linkRight.removeClass('disabled');
    }
  });

  var moveOffset = 32 + 133;
  var images = $('.thumbnails img');  
  function isLast(image) {
    return image === $('.thumbnails img:last-child')[0];
  }
  function isFirst(image) {
    return image === $('.thumbnails img:first-child')[0];
  }
  function slideToNext() {
    $('.thumbnails').animate({left: '-=' + moveOffset}, 400);
    
    //Set firstVisible
    var indexOfFirstVisible = images.index($('.thumbnails .firstVisible'));
    images.siblings('.firstVisible').removeClass('firstVisible');
    images.eq(indexOfFirstVisible + 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = images.index($('.thumbnails .lastVisible'));
    images.siblings('.lastVisible').removeClass('lastVisible');
    images.eq(indexOfLastVisible + 1).addClass('lastVisible');

    var lastVisible = $('.thumbnails .lastVisible');
    if (isLast(lastVisible[0])) {
      $('.pagination-right').addClass('disabled');
    }
  }
  function slideToPrevious() {
    $('.thumbnails').animate({left: '+=' + moveOffset}, 400);
    
    //Set firstVisible
    var indexOfFirstVisible = images.index($('.thumbnails .firstVisible'));
    images.siblings('.firstVisible').removeClass('firstVisible');
    images.eq(indexOfFirstVisible - 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = images.index($('.thumbnails .lastVisible'));
    images.siblings('.lastVisible').removeClass('lastVisible');
    images.eq(indexOfLastVisible - 1).addClass('lastVisible');

    var firstVisible = $('.thumbnails .firstVisible');
    if (isFirst(firstVisible[0])) {
      $('.pagination-left').addClass('disabled');
    }
  }



  //Ховер фото в слайдере портфолио (слева который)
  //Ховер фото на странице РАБОТЫ
  $('.work__image-block img, .work-section__image-block__image').on('mouseenter', function () {
    $(this).animate({
      top: '+=15',
      left: '+=22',
      width: '-=44',
      height: '-=30'
    });
  }).on('mouseleave', function () {
    $(this).animate({
      top: '-=15',
      left: '-=22',
      width: '+=44',
      height: '+=30'
    });
  });


  //Прокрутка портфолио, раз в 7 секунд
  var goUp = false;

  setInterval(function () {

    var lastVisibleBlock = $('.work-container .lastVisible');
    var firstVisibleBlock = $('.work-container .firstVisible');

    if (!goUp) {
      if (!isLastBlock(lastVisibleBlock[0])) {
        slideToDown();
        //Turn on arrows
        var linkUp = $('.pagination-up');
        if (linkUp.hasClass('disabled')) {
          linkUp.removeClass('disabled');
        }
      } else {
        goUp = true;        
      }
    }

    if (goUp) {
      if (!isFirstBlock(firstVisibleBlock[0])) {
        slideToUp();
        //Turn on arrows
        var linkDown = $('.pagination-down');
        if (linkDown.hasClass('disabled')) {
          linkDown.removeClass('disabled');
        }
      } else {
        goUp = false;
        if (!isLastBlock(lastVisibleBlock[0])) {
          slideToDown();
          //Turn on arrows
          var linkUp = $('.pagination-up');
          if (linkUp.hasClass('disabled')) {
            linkUp.removeClass('disabled');
          }
        }
      }
    }
  }, 7000);

  //Slider gallery in portfolio
  var workContainerHeight = 0;
  $('.work-container .work').each(function(index) {
    if (index > 1) {
      return;
    };
    var workHeight = $(this).height() + parseInt($(this).css('margin-top')) + parseInt($(this).css('margin-bottom'))
                       + parseInt($(this).css('padding-top')) + parseInt($(this).css('padding-bottom'));
    workContainerHeight += workHeight;
  });
  $('.work-container').height(workContainerHeight);
  $('.work-container .work:nth-child(1)').addClass('firstVisible');
  // $('.work-container .work:nth-child(2)').addClass('lastVisible');
  $('.work-container .work:nth-child(1)').addClass('lastVisible');

  $('.pagination-down a').on('click', function() {
    var lastVisibleBlock = $('.work-container .lastVisible');
    if (!isLastBlock(lastVisibleBlock[0])) {
      slideToDown();
    }
    //Turn on arrows
    var linkUp = $('.pagination-up');
    if (linkUp.hasClass('disabled')) {
      linkUp.removeClass('disabled');
    }
  });
  $('.pagination-up a').on('click', function() {
    var firstVisibleBlock = $('.work-container .firstVisible');
    if (!isFirstBlock(firstVisibleBlock[0])) {
      slideToUp();
    }
    //Turn on arrows
    var linkDown = $('.pagination-down');
    if (linkDown.hasClass('disabled')) {
      linkDown.removeClass('disabled');
    }
  });

  var elements = $('.work-container .work');  
  function isLastBlock(block) {
    return block === $('.work-container .work:last-child')[0];
  }
  function isFirstBlock(block) {
    return block === $('.work-container .work:first-child')[0];
  }
  function slideToDown() {    
    //Set firstVisible
    var indexOfFirstVisible = elements.index($('.work-container .firstVisible'));
    elements.siblings('.firstVisible').removeClass('firstVisible');
    elements.eq(indexOfFirstVisible + 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = elements.index($('.work-container .lastVisible'));
    elements.siblings('.lastVisible').removeClass('lastVisible');
    elements.eq(indexOfLastVisible + 1).addClass('lastVisible');

    var lastVisibleElement = $('.work-container .lastVisible');
    var moveOffset = lastVisibleElement.height() + parseInt(lastVisibleElement.css('margin-top')) + parseInt(lastVisibleElement.css('margin-bottom'))
                       + parseInt(lastVisibleElement.css('padding-top')) + parseInt(lastVisibleElement.css('padding-bottom')) + 1;
    $('.work-moving-container').animate({top: '-=' + moveOffset}, 400);

    if (isLastBlock(lastVisibleElement[0])) {
      $('.pagination-down').addClass('disabled');
    }
  }
  function slideToUp() {    
    //Set firstVisible
    var indexOfFirstVisible = elements.index($('.work-container .firstVisible'));
    elements.siblings('.firstVisible').removeClass('firstVisible');
    elements.eq(indexOfFirstVisible - 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = elements.index($('.work-container .lastVisible'));
    elements.siblings('.lastVisible').removeClass('lastVisible');
    elements.eq(indexOfLastVisible - 1).addClass('lastVisible');

    var firstVisibleElement = $('.work-container .firstVisible');
    var moveOffset = firstVisibleElement.height() + parseInt(firstVisibleElement.css('margin-top')) + parseInt(firstVisibleElement.css('margin-bottom'))
                       + parseInt(firstVisibleElement.css('padding-top')) + parseInt(firstVisibleElement.css('padding-bottom')) + 1;
    $('.work-moving-container').animate({top: '+=' + moveOffset}, 400);

    if (isFirstBlock(firstVisibleElement[0])) {
      $('.pagination-up').addClass('disabled');
    }
  }
});

if ($('#map').length > 0) {
  ymaps.ready(init);
}
var myMap, 
    myPlacemark;

function init(){ 
    myMap = new ymaps.Map ("map", {
        center: [47.2520,39.7351],
        zoom: 12,
        behaviors: ['default', 'scrollZoom']
    });

    //Создаем и добавляем метку    
    myPlacemark = new ymaps.Placemark([47.2520,39.7351], {
        content: '',
        balloonContent: ''
    });    
    myMap.geoObjects.add(myPlacemark);

    // Создание экземпляра элемента управления
    myMap.controls.add(
       new ymaps.control.ZoomControl()
    );

    // Обращение к конструктору класса элемента управления по ключу - кнопки "Схема", "Спутник"
    myMap.controls.add('typeSelector');

    //Добавляем кнопки на карте
    myMap.controls.add('mapTools');
}

$(function () {
	$('.user-reviews .bxslider').bxSlider({
		slideMargin: 10,
		moveSlides: 1,
		infiniteLoop: true,
		minSlides: 2,
		maxSlides: 2,
		// slideWidth: 344
		slideWidth: 490
	});


	jQuery('.tp-banner').show().revolution(
		{
			delay:9000,
         startwidth:960,
         startheight:600,
         startWithSlide:0,
 
         fullScreenAlignForce:"off",
         autoHeight:"off",
         minHeight:"off",
 
         shuffle:"off",
 
         onHoverStop:"on",
 
         thumbWidth:100,
         thumbHeight:50,
         thumbAmount:3,
 
         hideThumbsOnMobile:"off",
         hideNavDelayOnMobile:1500,
         hideBulletsOnMobile:"off",
         hideArrowsOnMobile:"off",
         hideThumbsUnderResoluition:0,
 
         hideThumbs:0,
         hideTimerBar:"off",
 
         keyboardNavigation:"on",
 
         navigationType:"bullet",
         navigationArrows:"solo",
         navigationStyle:"round",
 
         navigationHAlign:"center",
         navigationVAlign:"bottom",
         navigationHOffset:30,
         navigationVOffset:30,
 
         soloArrowLeftHalign:"left",
         soloArrowLeftValign:"center",
         soloArrowLeftHOffset:20,
         soloArrowLeftVOffset:0,
 
         soloArrowRightHalign:"right",
         soloArrowRightValign:"center",
         soloArrowRightHOffset:20,
         soloArrowRightVOffset:0,
 
 
         touchenabled:"on",
         swipe_velocity:"0.7",
         swipe_max_touches:"1",
         swipe_min_touches:"1",
         drag_block_vertical:"false",
 
         parallax:"mouse",
         parallaxBgFreeze:"on",
         parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
         parallaxDisableOnMobile:"off",
 
         stopAtSlide:-1,
         stopAfterLoops:-1,
         hideCaptionAtLimit:0,
         hideAllCaptionAtLilmit:0,
         hideSliderAtLimit:0,
 
         dottedOverlay:"none",
 
         spinned:"spinner4",
 
         fullWidth:"off",
         forceFullWidth:"off",
         fullScreen:"off",
         fullScreenOffsetContainer:"#topheader-to-offset",
         fullScreenOffset:"0px",
 
         panZoomDisableOnMobile:"off",
 
         simplifyAll:"off",
 
         shadow:0	
		});

});