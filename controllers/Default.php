<?php

class Default_Controller
{
	protected $oSmarty = null;
	protected $strPath = '';
	
	protected static $oDB = null;
	protected static $aRequest = array();
	protected static $aPath = array();
	private static $aTmplVars = array();
	private static $aPageParams = array();


	public function __call($strMethod, $mParams) {		
		if (!method_exists($this, $strMethod)) {
			$this->error404();			
		} else {
			call_user_func_array(array(&$this, $strMethod));	
		}
	}
	
	
	public function __construct($aParams) {
		self::$oDB = new DBApi();
		Registry::set('DBApi', self::$oDB);
		$mainApi = new mainApi(self::$oDB);
		
		$this->setRequest();
		
		self::$aPath = Registry::get('address');
		
		$this->oSmarty = new Smarty;		
		$this->oSmarty->template_dir = $aParams['smarty_template_dir'];
		$this->oSmarty->compile_dir = $aParams['smarty_compile_dir'];
		$this->oSmarty->cache_dir = $aParams['smarty_cache_dir'];
		
		$this->strPath = $aParams['path'];
		$this->oSmarty->assign('strPath', $aParams['path']);
		
		if (isset($aParams['page'])) {
			$this->strMethodName = $aParams['page'];
			$this->oSmarty->assign('strPage', $this->strMethodName);
		}		
		
		define('isPRODUCTION',(H::getServerType() == 'production'));
		$this->oSmarty->assign('isPRODUCTION',isPRODUCTION);
		
		$this->init();
	}
	
	protected function render($strTemplate, $strLayaut='main'){
		$this->oSmarty->assign('aPageParams', self::$aPageParams);
		foreach (self::$aTmplVars as $key=>$var){
			$this->oSmarty->assign($key, $var);
		}
		$this->oSmarty->assign('ContentTemplate', $strTemplate);
		$this->oSmarty->display($strLayaut.'.tpl');
		exit();
	}
	protected function fetchTpl($strTpl){
		$this->oSmarty->assign('aPageParams', self::$aPageParams);
		foreach (self::$aTmplVars as $key=>$var){
			$this->oSmarty->assign($key, $var);
		}
		return $this->oSmarty->fetch($strTpl);
	}
	protected function init() {
		print 'aaa';
	}
	
	protected function getPath($nIndex=0){
		if (!$nIndex) return self::$aPath;
		if (isset(self::$aPath[$nIndex])){
			return self::$aPath[$nIndex];
		}else{
			return FALSE;
		}
	}
	
	protected function setTmplVar($key, $var){
		self::$aTmplVars[$key] = $var;
	}
	protected function setTmplVars($aVars){
		foreach ($aVars as $key=>$var){
			self::$aTmplVars[$key] = $var;
		}		
	}
	
	protected function setPageParam($key, $var){
		self::$aPageParams[$key] = $var;
	}
	
	public function error404() {	
        header("HTTP/1.1 404 Page Not Found");
		die('<h1>Страница не найдена!</h1>');
	}	
		
	protected function redirect($strUrlPage='') {
		header("HTTP/1.1 301 Moved Permanently");
		header(sprintf("Location: %s", '/'.$strUrlPage));
		exit;
	}
	
	private function setRequest(){
		$aRequest = $_REQUEST;
		foreach ($aRequest as $key=>$value){
			//вставить проверку типов и чистку полей в зависимости от типа
		}
		if (isset($aRequest['filter']) && $aRequest['filter']){
			$aTmp = (array)json_decode($aRequest['filter'],2);
			$aFilters = array();
			foreach ($aTmp as $key=>$value){	
				if (!empty($value['value'])) $aFilters[$value['property']] = $value['value'];
			}
			$aRequest['filters'] = $aFilters;
		}else{
			$aRequest['filters'] = array();
		}
		self::$aRequest = $aRequest;
	}
	
	protected function getRequest($strParam,$type='str'){
		$mixResult = (isset(self::$aRequest[$strParam])) ? self::$aRequest[$strParam] : FALSE;
		switch ($type) {
			case 'int':	
				return intval($mixResult);		break;
			case 'str': 
				return strval($mixResult);		break;
			case 'array': 
				if (is_array($mixResult)) return $mixResult;
				$oTmp = @json_decode($mixResult);
				return (is_object($oTmp)) ? (array)$oTmp : array();
				break;
			default:	
				return strval($mixResult);		break;
		}		
	}
	
	protected static function createResponsFromArray($aData,$strCallBack=''){
		if ($strCallBack){
			return sprintf('%s(%s)', $strCallBack,json_encode($aData,JSON_NUMERIC_CHECK));
		}else{
			return json_encode($aData,JSON_NUMERIC_CHECK);
		}
	}
	protected static function printRespons($strResponse,$bExitAfterResponse=TRUE){		
		print $strResponse;
		if ($bExitAfterResponse) exit();
	}
}
