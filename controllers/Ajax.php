<?php

class Ajax_Controller extends Default_Controller 
{
	private static $aResponse = array('success'=>true);


	public function saveProjectInfo() {
		$aData = $this->getRequest('data', 'array');
		$nProjectId = $aData['project_id'];
		unset($aData['project_id'],$aData['_wysihtml5_mode']);
		$aData['status'] = (!empty($aData['status'])) ? $aData['status'] : 0;
		projects::updateProject($nProjectId, $aData);
		self::printRespons(self::createResponsFromArray(self::$aResponse),FALSE);		
		projects::checkMainPhoto($nProjectId);
		exit();
	}
        
	public function saveData() {
		if (($_SERVER['REQUEST_METHOD'] == 'POST')){
			$aData = $this->getRequest('data', 'array');
			$strType = $aData['data_type'];
			$nId = $aData['project_id'];
			
			switch ($strType) {
				case 'project':
					projects::projectSave($nId, $aData);
					break;
				case 'type':
					projects::typeSave($nId, $aData);
					break;
				case 'review':
					reviews::update($aData, $nId);
					break;
				case 'articles':
						articles::update($aData, $nId);
				default:
					break;
			}			
			//$method = $dataType."Save";
			//projects::$method($nDataId, $aData);
			self::printRespons(self::createResponsFromArray(self::$aResponse),FALSE);
		}
	}
        
    public function uploadPhotos() {
        $strType = $this->getRequest('type', 'str');
		$nId = $this->getRequest('id', 'int');
		$aFile = H::reArrayFiles($_FILES['files']);
		if($nId && $aFile){
			$aImg = images::UploadImage($aFile[0], $nId, $strType);
			if ($aImg){ 
				self::$aResponse['files'][] = $aImg; 
			}else{ 
				self::$aResponse['success'] = false; 					
			}				
		}else{
			self::$aResponse['success'] = false;
		}		
        self::printRespons(self::createResponsFromArray(self::$aResponse));		
    }	
	public function getPhotos(){
		$strType = $this->getRequest('type', 'str');
		$nId = $this->getRequest('id', 'int');
		self::$aResponse['files'] = images::getImagesForAdmin($strType, $nId);
		self::printRespons(self::createResponsFromArray(self::$aResponse));	
	}
    public function deletePhoto() {
		$nImgId = $this->getRequest('id', 'int');
		$strType = $this->getRequest('type', 'str');
		switch ($strType) {
			case 'project':
				projects::deleteProjectImage($nImgId);
				
				break;
			case 'type':
				projects::deleteTypeImage($nImgId);
				break;
			case 'review':
				reviews::deleteImage($nImgId);
				break;
			case 'articles':
				articles::deleteImage($nImgId);
			default:
				break;
		}		
		self::printRespons(self::createResponsFromArray(self::$aResponse));
	}
	public function setMainImg() {
		$nImgId = $this->getRequest('img_id', 'int');
		images::setMainPhoto($nImgId);
		self::printRespons(self::createResponsFromArray(self::$aResponse));
	}
	
	public function getWinContent(){
		$nId = $this->getRequest('id', 'int');
		$strWinId = $this->getRequest('win', 'str');
		switch ($strWinId) {
			case 'projectBy':
				$this->setTmplVar('aData',projects::getProject($nId));			
				$this->setTmplVar('aImages',images::getImagesByType('project', $nId));			
				break;
			default:
				break;
		}
		$strTplPath = sprintf('ajax/popup-content/%s.tpl',$strWinId);
		self::$aResponse['content'] = $this->fetchTpl($strTplPath);//$this->oSmarty->fetch($strTplPath);
		//self::$aResponse['data'] = projects::getProject($nId);
		self::printRespons(self::createResponsFromArray(self::$aResponse));	
	}

	public function getProjectImages() {
		$nProjectId = $this->getRequest('project_id', 'int');
		self::$aResponse['images'] = projects::getProjectImages($nProjectId);
		self::printRespons(self::createResponsFromArray(self::$aResponse));		
	}	
	public function deleteProject(){
		$nProjectId = $this->getRequest('project_id', 'int');
		projects::deleteProject($nProjectId);
		self::printRespons(self::createResponsFromArray(self::$aResponse));		
	}
	public function deleteReview(){
		$nId = $this->getRequest('id', 'int');
		reviews::delete($nId);
		self::printRespons(self::createResponsFromArray(self::$aResponse));		
	}
	public function deleteRequest(){
		$nId = $this->getRequest('id', 'int');
		requests::delete($nId);
		self::printRespons(self::createResponsFromArray(self::$aResponse));		
	}
	public function updateRequest(){
		$nId = $this->getRequest('id', 'int');
		$strData = $this->getRequest('data', 'str');
		$aData = requests::getUpdateData($strData);
		requests::update($aData, $nId);
		self::printRespons(self::createResponsFromArray(self::$aResponse));		
	}
	
        
        public function addArticles() {
		$aData = $this->getRequest('data', 'array');
		$a=0;
	}
        public function deleteArticles(){
		$nId = $this->getRequest('id', 'int');
		articles::delete($nId);
		self::printRespons(self::createResponsFromArray(self::$aResponse));		
	}
        
        public function updateArticles(){
		$nId = $this->getRequest('id', 'int');
		$strData = $this->getRequest('data', 'str');
		$aData = articles::getUpdateData($strData);
		articles::update($aData, $nId);
		self::printRespons(self::createResponsFromArray(self::$aResponse));		
	}
        
        
	public function addReview() {
		$aData = $this->getRequest('data', 'array');
		$a=0;
	}
        
	public function setOrderRequest() {		
		$aData = $this->getRequest('data', 'array');
		$aData['fullrequest'] = json_encode($aData);
		if(!empty($aData['human']) || !empty($aData['human_t'])){
			$aData['isbot'] = 1;
		}
		$aData['ip'] = $_SERVER['REMOTE_ADDR'];
		self::$aResponse['request_id'] = requests::setRequest($aData);
		self::printRespons(self::createResponsFromArray(self::$aResponse));		
	}
	
	public function setOrder() {
		$aData = $this->getRequest('data', 'array');
		self::$aResponse['order_id'] = orders::setNew($aData);
		self::printRespons(self::createResponsFromArray(self::$aResponse));		
	}

	/****** Служебные методы  *******/
	
	protected $oSmarty = null;
	
	/** Массив возвращаемых данных в формате Json */
	protected $JSON = array();
	
	public function __call($strMethod, $mParams) {		
		if (!method_exists($this, $strMethod)) {
			$this->e('Экшн "'.$strMethod.'" не найден!');		
		} else {
			call_user_func_array(array(&$this, $strMethod));	
		}
	}
	
	
	protected function init() {
		$this->oSmarty = new Smarty;
		$this->oSmarty->template_dir = DR.'/tpl/';
		$this->oSmarty->compile_dir = DR.'/tpl/smarty_sys/ctpl/';
		$this->oSmarty->cache_dir = DR.'/tpl/smarty_sys/cache/';
	}
	
	
	public function __destruct() {
//		$this->end();
	}

	
	/** Возвращет ошибку с текстом
	 *
	 * @param str $strMessage - Текст сообщения */
	protected function e($strMessage) {
		echo json_encode(array(
			'message' => $strMessage,
			'error' => true
		));
		die;
	}
	
	
	public function end() {
		$this->JSON['error'] = false;
		
		echo json_encode($this->JSON);
		die;
	}
}
