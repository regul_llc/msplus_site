<?php

class Admin_controller extends Default_Controller
{
	const MainUrl = '/admin';

	protected $oSmarty = null;


	public function index() {
		$this->setTmplVars(array('strPageName'=>"Главная", 'strPageSubName'=>"Управление и статистика"));
		$this->render('index');
	}
        
	public function projects() {
		if ($this->getRequest('action') == 'add'){
			$this->setTmplVars(array('strPageName'=>"Готовые проекты", 'strPageSubName'=>"Добавление проекта"));
			$nProjectId = projects::addProject($this->getRequest('pname','str'));				
			$strRedirect = ($nProjectId) ? 'projects/'.$nProjectId : 'projects';
			$this->redirect($strRedirect);
		}    
		
		$nProjectId = $this->getPath(1);
		if (!$nProjectId) {
			$this->setTmplVars(array('strPageName'=>"Готовые проекты", 'strPageSubName'=>"Список проектов"));
			$data['cats'] = projects::getFullCats(false); //и? получили, а что дальше?
			$this->setTmplVar('aList', projects::getProjects(0, 10));
		} else {
			$this->setTmplVars(array('strPageName'=>"Готовые проекты", 'strPageSubName'=>"Редактирование проекта"));
			$fullProject = projects::getProject($nProjectId);		
			$fullProject['cats'] = projects::getFullCats(false);
			$this->setTmplVar('aProject', $fullProject);
		}
		
		$this->render('projects');
	}
        
	public function types() {
		$this->setTmplVars(array('strPageName'=>"Типы потолков", 'strPageSubName'=>"Управление типами заказов"));
		$catId = $this->getPath(1);
		if (!$catId) {
			$this->setTmplVar('aList', projects::getFullCats(false));
		} else {
			$fullCat = projects::getCatFullInfo($catId);
			$this->setTmplVar('typeData', $fullCat);
		}
		$this->render('types');
	}
	
        public function articles() {
            $this->setTmplVars(array('strPageName'=>"Статьи", 'strPageSubName'=>"Управление статьями"));
		$nId = $this->getPath(1);
		if ($this->getRequest('action') == 'add'){
			$this->setTmplVars(array('strPageName'=>"Статьи", 'strPageSubName'=>"Добавление cтатьи"));
			$nProjectId = articles::add(array('title'=>$this->getRequest('pname','str')),TRUE);				
			$strRedirect = ($nProjectId) ? 'articles/'.$nProjectId : 'articles';
			$this->redirect($strRedirect);
		}    
		if (!$nId) {
			$this->setTmplVar('aList', articles::getAll(FALSE,FALSE));
		} else {
			$aRow = articles::getOne($nId);
			$this->setTmplVar('aRow', $aRow);
		}
		$this->render('articles');
        }
        
	public function reviews() {
		$this->setTmplVars(array('strPageName'=>"Отзывы", 'strPageSubName'=>"Список отзывов"));
		$nId = $this->getPath(1);
		if ($this->getRequest('action') == 'add'){
			$this->setTmplVars(array('strPageName'=>"Отзывы", 'strPageSubName'=>"Добавление отзыва"));
			$nProjectId = reviews::add(array('title'=>$this->getRequest('pname','str')),TRUE);				
			$strRedirect = ($nProjectId) ? 'reviews/'.$nProjectId : 'reviews';
			$this->redirect($strRedirect);
		}    
		if (!$nId) {
			$this->setTmplVar('aList', reviews::getAll(FALSE,FALSE));
		} else {
			$aRow = reviews::getOne($nId);
			$this->setTmplVar('aCats', projects::getAllCats(TRUE));
			$this->setTmplVar('aRow', $aRow);
		}
		$this->render('reviews');
	}
		
	public function orders() {
		$this->setTmplVars(array('strPageName'=>"Заказы", 'strPageSubName'=>"Управление заказами"));
		$this->setTmplVar('aList', orders::getAll());
		$this->render('orders');
	}
        
        public function requests() {
            $this->setTmplVars(array('strPageName'=>"Заявки", 'strPageSubName'=>"Управление заявками"));
            $this->setTmplVar('aList', requests::getRequests());
            $this->render('requests');
	}
		
	public function contacts() {
		$nAction = 0;
		
		if (isset($_POST['options'])) {
			$nAction = (options::set($_POST['options'])) ? 1 : 2;
		}

		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', 'Контакты');
		$this->oSmarty->assign('aOptions', options::getAll('contacts'));
		$this->oSmarty->display('index.tpl');
	}
	

	/** Форма авторизации пользователя **/
	public function login() {
		$strMessage = '';

		if (isset($_POST['submit'])) {
			
			if (isset($_POST['login']) && $_POST['login']!='' && isset($_POST['password']) && $_POST['password']!='') {
				$oUser = new user();

				if ($oUser->login($_POST, $this->strPath)) {
					$this->redirect();
				} else {
					$strMessage = join('<br>', $oUser->getErrors());
				}
			} else {
				$strMessage = 'Введите Ваш логин и пароль';
			}
		}
		$this->setTmplVar('strMess', $strMessage);
		$this->render('logon','login');
	}
	
	
	/** Разлогинивание **/
	public function logout() {
		user::logout($this->strPath);
		$this->redirect();
	}
	
	
	/** Страница не найдена **/
	public function error404() {	
		$this->oSmarty->assign('strPageName', 'Страница не найдена');
		$this->oSmarty->assign('strMessage', 'Страница не найдена');
		$this->oSmarty->display('404.tpl');
		die;
	}
	
	
	/** отрабатывается перед вызванным методом **/
	public function init() {
		$this->setTmplVar('MainUrl', self::MainUrl);
		if (!user::isLogin($this->strPath)) {
			$this->login();
			die;
		}
		$aPath = $this->getPath();
		for ($i=0; $i<3; $i++) { $aPath[$i] = (isset($aPath[$i])) ? $aPath[$i] : ""; }
		$aCats = projects::getAllCats();		
		$aMenu = array(
				''		=> array('name'=>'Главная', 'class'=>'fa-dashboard'),
				'projects'	=> array('name'=>'Готовые проекты', 'class'=>'fa-files-o'),
				'types'         => array('name'=>'Типы потолков', 'class'=>'fa-files-o'),
				//'orders'	=> array('name'=>'Заказы', 'class'=>'fa-folder'),
                                'articles'      => array('name'=>'Статьи', 'class'=>'fa-files-o'),
				'requests'	=> array('name'=>'Заявки', 'class'=>'fa-envelope'),
				'reviews'	=> array('name'=>'Отзывы', 'class'=>'fa-envelope')
				//'others' => array('name'=>'Прочее', 'class'=>'fa-files-o'),
		);
		$this->setTmplVar('aPath', $aPath);
		$this->setTmplVar('aMenu',  $aMenu);		
		$this->setTmplVar('aCurUser', user::getCurrentUser());
	}
	
	/** редирект **/
	protected function redirect($strUrlPage='') {
		header(sprintf("Location: %s/%s", self::MainUrl,$strUrlPage));
		exit;
	}
	
	
	/** Перехват вызова метода **/
	public function __call($strMethod, $mParams) {		
		if (!method_exists($this, $strMethod)) {
			$this->error404();			
		} else {
			call_user_func_array(array(&$this, $strMethod));	
		}
	}
	
	protected function getPath($nIndex=0){
		$aPath = self::$aPath;
		array_splice($aPath, 0, 2);
		if (!$nIndex) return $aPath;
		if (isset($aPath[$nIndex])){
			return $aPath[$nIndex];
		}else{
			return FALSE;
		}
	}
}

