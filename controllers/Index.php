<?php

class Index_controller extends Default_Controller
{
	protected $oSmarty = null;
	
	/** картинка капчи **/
	public function captcha() {
            H::capthca();
	}
	
	public function index() {
		$this->setPageParam('pageType', 'index');
		$this->oSmarty->assign('strPageTitle', 'Натяжные потолки в Ростове-на-Дону');
		$this->oSmarty->assign('strDescription', 'Купить натяжные потолки в Ростове-на-Дону 
                                        и Батайске. Гарантия 10 лет. Бесплатный вызов замерщика.');

		$this->setTmplVar('aItems', projects::getProjects());
		$this->setTmplVar('aCats', projects::getCatsForWeb(9));
		$this->setTmplVar('aReviews', reviews::getAll(FALSE,TRUE,6));
		$this->render('index');
	}
	
	public function prices() {
		$this->setPageParam('pageType', 'prices');
		$this->oSmarty->assign('strPageTitle', 'Цена на натяжные потолки в Ростове-на-дону');
		$this->oSmarty->assign('strDescription', 'Цены на натяжные потолки в Ростове-на-Дону от 300 рублей за квадратный метр с монтажом - компания «MSPlus». Звоните по телефону: +7 (863) 226-74-48');
                
                $this->setPageParam('NoBigHeader', 1);
		$this->setTmplVar('aItems', projects::getProjects(0, 10, 'big'));
		$this->setTmplVar('aReviews', reviews::getAll(FALSE,TRUE,6));
		$this->render('prices');
                
	}
        
	public function articles() {
		$this->setPageParam('pageType', 'articles');
                $this->setPageParam('NoBigHeader', 1);
		$pArticle = $this->getPath(2);

		if (!$pArticle) {
            $this->setTmplVar('strPageTitle', 'Статьи о натяжных потолках ');
                $this->setTmplVar('strDescription', 'Статьи о видах натяжных потолков, их обслуживании, ремонте и монтаже.');
			$this->setTmplVar('aItems', articles::getArticles());
			$this->render('articles');
		} else {
                    if (is_numeric($pArticle)) { /**
            $aRecord = articles::getArticle($nArticle);
			$this->setTmplVar('article', $aRecord);
            $this->setTmplVar('strPageTitle', $aRecord['meta_title']);
			$this->render('article');**/
                        $pArticle = articles::getUrlById($pArticle);
                        $this->redirect('articles/'.$pArticle);
                    } else {
                    
            $aRecord = articles::getArticleByUrl($pArticle);
			$this->setTmplVar('article', $aRecord);
            $this->setTmplVar('strPageTitle', $aRecord['meta_title']);
			$this->render('article');
                    }
		}            
	}
        
	public function actions() {
		$this->setPageParam('pageType', 'actions');
		$this->oSmarty->assign('strPageTitle', 'Скидки и акции на натяжные потолки в Ростове-на-Дону');
		$this->oSmarty->assign('strDescription', 'Выгодные акции на изготовление и монтаж натяжных потолков в Ростове-на-Дону от компании «MSPlus». Звоните по телефону: +7 (863) 226-74-48');
                $this->setPageParam('NoBigHeader', 1);
		$this->render('actions');
	}
        
	public function projects() {
            $this->setPageParam('pageType', 'projects');
            $this->setPageParam('NoBigHeader', 1);
            $nCatId = $this->getPath(2);
            if (!$nCatId) $this->error404();
            $this->setTmplVar('nCatId', $nCatId);
            $this->setTmplVar('aCats', projects::getCatsForWeb());
            $this->setTmplVar('aProjects', projects::getProjects($nCatId));
            $this->setPageParam('NoBigHeader', 1);
            $this->render('projects');
	}
        
	public function works() {
		$this->setPageParam('pageType', 'works');
        $this->setPageParam('NoBigHeader', 1);
		$sCatName = $this->getPath(2);
		if (!$sCatName) {
			$this->setTmplVar('aCats', projects::getCatsForWeb(9));
            $this->setTmplVar('strPageTitle', 'Виды натяжных потолков - «MSPlus»');
                $this->setTmplVar('strDescription', 'Производим монтаж всех видов натяжных потолков: 
                                  матовые, глянцевые, сатиновые, многоуровневые.');
                        $this->render('works');
		} else {
			$aCat = projects::getCatByUrl($sCatName);
            $this->setTmplVar('strPageTitle', $aCat['meta_title']);
                $this->setTmplVar('strDescription', $aCat['meta_description']);
			$this->setTmplVar('aReviews', reviews::getAll($aCat['id'],TRUE));
			$this->setTmplVar('aProjects', projects::getProjects($aCat['id']));
			$this->setPageParam('NoBigHeader', true);
			$this->setTmplVar('aCat', $aCat);
			$this->setTmplVar('aCats', projects::getCatsForWeb(9, 'middle'));
			$this->render('project');
		}
	}

	public function contacts() {

		$this->setPageParam('pageType', 'contacts');
                $this->oSmarty->assign('strDescription', 'MSPlus61 - Контакты |
                         Ростов-на-Дону, Телефон: +7 (863) 226-74-48, e-mail: info@msplus61.ru');
		$this->oSmarty->assign('strPageTitle', 'MSPlus - натяжные потолки в Ростове-на-Дону. Контакты.');
		$this->setPageParam('NoBigHeader', true);
		$this->render('contacts');
	}
        
	public function reviews() {
		$this->setPageParam('pageType', 'reviews');
		$this->oSmarty->assign('strPageTitle', 'Отзывы о натяжных потолках - «MSPlus»');
                $this->oSmarty->assign('strDescription', 'Компания «MSPlus» в Ростове-на-Дону - отзывы наших клиентов о натяжных потолках и нашей работе. Звоните по телефону: +7 (863) 226-74-48');
                $this->setPageParam('NoBigHeader', 1);
		$this->setTmplVar('aReviews', reviews::getAll(0,TRUE));
		$this->setTmplVar('aCats', projects::getCatsForWeb(9));
		$strAction = $this->getRequest('action');
		if ($this->getRequest('action') == 'add'){
			$aData = $this->getRequest('data','array');
			if($aData){
				$nId = reviews::add($aData);
				if ($nId && !empty($_FILES['cover']) && !$_FILES['cover']['error']) images::UploadImage ($_FILES['cover'], $nId, 'review');
				$this->setTmplVar('message', 'Спасибо за Ваш отзыв! Он появится на сайте сразу после модерации.');
			}
		}
		$this->render('reviews');
	}
        

	/** Страница не найдена **/
	public function error404() {
		header('HTTP/1.1 404 Not Found');
		$this->setPageParam('pageType', '404');
		$this->oSmarty->assign('strPageName', 'Страница не найдена');
		$this->oSmarty->assign('strMessage', 'Страница не найдена');
		$this->render('404');
		die;
	}
	
	/** отрабатывается перед вызванным методом **/
	public function init() {
		$this->setTmplVar('strSiteName', 'MsPlus');
	}
	
	/** Перехват вызова метода **/
	public function __call($strMethod, $mParams) {		
		if (!method_exists($this, $strMethod)) {
			$this->error404();			
		} else {
			call_user_func_array(array(&$this, $strMethod));	
		}
	}
}

 
